package lr08;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 *
 * @author Matsokha Andrey
 */
public class StudentModel {
    private List<Student> studentList = new ArrayList<>();
    
    public List<Student> getStudentList() {
        return Collections.unmodifiableList(studentList);
    }

    public void addStudent(Student student) {
        studentList.add(student);
    }
    
    public void setAvgExaminationGrade(Student student, float grade) {
        int index = studentList.indexOf(student);
        Student studentFromList = studentList.get(index);
        studentFromList.setAvgGradeOfLastExamination(grade);
    }

    /**
     * Get student from list by name, lastName and group.
     * 
     * @param name
     * @param lastName
     * @param group
     * @return Student
     * @exception NoSuchElementException if no compatible student found
     */
    public Student getStudent(String name, String lastName, String group) {
        for (Student student : studentList) {
            if (Objects.equals(name, student.getName())
                    && Objects.equals(lastName, student.getLastName())
                    && Objects.equals(group, student.getGroup())) {
                return student;
            }
        }
        
        throw new NoSuchElementException("No such student in list");
    }

    void removeStudent(Student student) {
        studentList.remove(student);
    }
}
