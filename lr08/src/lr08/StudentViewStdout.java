package lr08;

/**
 *
 * @author Matsokha Andrey
 */
public class StudentViewStdout implements StudentView {

    @Override
    public void printStudentList(StudentModel studentModel) {
        System.out.println("List of all students:");
        for (Student student : studentModel.getStudentList()) {
            System.out.println(student);
        }
    }

    @Override
    public void printError(String msg) {
        System.out.println("Error: " + msg);
    }
    
}
