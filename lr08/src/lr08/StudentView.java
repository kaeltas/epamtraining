package lr08;

/**
 *
 * @author Matsokha Andrey
 */
public interface StudentView {
    
    void printStudentList(StudentModel studentModel);
    void printError(String msg);
    
}
