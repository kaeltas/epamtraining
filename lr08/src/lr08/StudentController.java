package lr08;

import java.util.NoSuchElementException;

/**
 *
 * @author Matsokha Andrey
 */
public class StudentController {
    private final StudentModel studentModel = new StudentModel();
    private final StudentView studentView;

    public StudentController(StudentView studentView) {
        this.studentView = studentView;
    }
    
    public void fillStudentsList() {
        studentModel.addStudent(new Student("Group 01", 4.4f, 
                "Kenny", "O'Donnell", "", new BirthDate(1980, 10, 15)));
        studentModel.addStudent(new Student("Group 01", 4.2f, 
                "Paddy", "Mac'Neela", "", new BirthDate(1982, 03, 10)));
        studentModel.addStudent(new Student("Group 01", 4.5f, 
                "Shoen", "O'Connell", "", new BirthDate(1985, 11, 25)));
        studentModel.addStudent(new Student("Group 01", 4.0f, 
                "Joe", "O'Brian", "", new BirthDate(1981, 07, 28)));
    }
    
    public void viewAllStudents() {
        studentView.printStudentList(studentModel);
    }
    
    public void setExaminationGrade(Student student, float grade) {
        studentModel.setAvgExaminationGrade(student, grade);
    }
    
    public void setExaminationGrade(String name, String lastName,
            String group, float grade) {
        try {
            Student student = studentModel.getStudent(name, lastName, group);
            studentModel.setAvgExaminationGrade(student, grade);
        } catch (NoSuchElementException e) {
            studentView.printError("Can't set grade. No such student [" + name + " " + lastName 
            + " " + group + "]");
        }
    }
    
    public void setAvgExaminationGrade(String name, String lastName,
            String group, int gradeExam1, int gradeExam2, 
            int gradeExam3, int gradeExam4) {
        
        float avgGrade = (float)(gradeExam1 + gradeExam2 
                    + gradeExam3 + gradeExam4) / 4;
        setExaminationGrade(name, lastName, group, avgGrade);
    }
    
    public void removeStudent(Student student) {
        studentModel.removeStudent(student);
    }
    
    public void removeStudent(String name, String lastName, String group) {
        try {
            Student student = studentModel.getStudent(name, lastName, group);
            studentModel.removeStudent(student);
        } catch (NoSuchElementException e) {
            studentView.printError("Can't remove. No such student [" + name + " " + lastName 
            + " " + group + "]");
        }
    }
    
    public void addStudent(String name, String lastName, String middleName,
            String group, int year, int month, int day) {
        Student student = new Student(group, 0, name, lastName, middleName, 
                new BirthDate(year, month, day));
        studentModel.addStudent(student);
    }
    
}
