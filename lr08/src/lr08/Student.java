package lr08;

import java.util.Objects;

/**
 *
 * @author Matsokha Andrey
 */
public class Student extends Person {
    private String group;
    private float avgGradeOfLastExamination;

    public Student(String group, float avgGradeOfLastExamination, 
            String name, String lastName, String middleName, 
            BirthDate birthDate) {
        super(name, lastName, middleName, birthDate);
        this.group = group;
        this.avgGradeOfLastExamination = avgGradeOfLastExamination;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public float getAvgGradeOfLastExamination() {
        return avgGradeOfLastExamination;
    }

    public void setAvgGradeOfLastExamination(float avgGradeOfLastExamination) {
        this.avgGradeOfLastExamination = avgGradeOfLastExamination;
    }

    @Override
    public String toString() {
        BirthDate bd = getBirthDate();
        return "Student{" 
                + getName() + " " + getLastName() + " " + getMiddleName() + " "
                + "birthdate=" + bd.getDay() + "." 
                + bd.getMonth() + "." + bd.getYear() + " "
                + "group=" + group + ", avgGradeOfLastExamination=" + avgGradeOfLastExamination + '}';
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 79 * hash + Objects.hashCode(this.group);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
                return false;
        }
        if (!super.equals(obj)) {
            return false;
        } else {
            final Student other = (Student) obj;
            if (!Objects.equals(this.group, other.group)) {
                return false;
            }
        }
        return true;
    }



    
    
    
}
