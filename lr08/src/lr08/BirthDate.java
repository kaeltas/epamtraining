package lr08;

/**
 * Holds a birthdate
 * 
 * @author Matsokha Andrey
 */
public class BirthDate {
    private final int year;
    private final int month;
    private final int day;

    public BirthDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.year;
        hash = 59 * hash + this.month;
        hash = 59 * hash + this.day;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BirthDate other = (BirthDate) obj;
        if (this.year != other.year) {
            return false;
        }
        if (this.month != other.month) {
            return false;
        }
        if (this.day != other.day) {
            return false;
        }
        return true;
    }
    
    
    
}
