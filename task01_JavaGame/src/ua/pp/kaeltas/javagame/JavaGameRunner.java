package ua.pp.kaeltas.javagame;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matsokha Andrey
 */
public class JavaGameRunner {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new JavaGameControllerView().play();
    }
    
}
