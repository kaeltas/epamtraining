package ua.pp.kaeltas.javagame;

import java.util.ArrayList;
import java.util.List;


/**
 * JavaGame - player should step by step guess the number in range [0-100]
 * 
 * @author Matsokha Andrey
 */
public class JavaGame {
    private static final int RANGE_MIN = 0;
    private static final int RANGE_MAX = 100;
    
    private int secretNumber;
    private int rangeMin;
    private int rangeMax;
    private List<GuessStep> playerGuessSteps;
    
    private RandomGenerator randomGenerator;

    public JavaGame() {
        this(RANGE_MIN, RANGE_MAX);
    }
    
    public JavaGame(int rangeMin, int rangeMax) {
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
        this.randomGenerator = new UniformlyRandomGenerator();
        chooseSecretNumber();
        playerGuessSteps = new ArrayList<GuessStep>();
    }
    
    /**
     * Return random number from {@code rangeMin} to {@code rangeMax} range,
     * both bounds exclusively.
     * 
     * @param rangeMin minimum range bound. Must be five less than {@code rangeMax}.
     * @param rangeMax maximum range bound. Must be five more than {@code rangeMin}.
     * @exception IllegalArgumentException if rangeMin not five less than rangeMax.
     * @return 
     */
    public void chooseSecretNumber() {
//        if (rangeMin > rangeMax || (rangeMax - rangeMin <= 5) ) {
//            throw new IllegalArgumentException("RangeMin must be five less than RangeMax");
//        }
        secretNumber = randomGenerator.rand(rangeMin+1, rangeMax-1);
    }
    
    /**
     * Checks if guessNumber equals to secretNumber and stores each guessNumber.
     * 
     * @param guessNumber player assumption about secretNumber value
     * @return boolean value: true, if guessNumber equals to secretNumber; 
     * false - otherwise.
     */
    boolean guess(final int guessNumber) {
        playerGuessSteps.add(new GuessStep(guessNumber, rangeMin, rangeMax));
        
        if (secretNumber > guessNumber) {
            rangeMin = guessNumber;
        } else {
            rangeMax = guessNumber;
        }
                
        return (guessNumber == secretNumber);
    }
    
    /**
     * Checks if guessNumber is in allowed range
     * 
     * @param guessNumber
     * @return true, if guessNumber is in allowed range; false - otherwise.
     */
    public boolean isNumberInAllowedRange(final int guessNumber) {
        return (guessNumber < rangeMax && guessNumber > rangeMin);
    }
    
    /**
     * Checks if secretNumber is bigger than guessNumber
     * 
     * @param guessNumber
     * @return true, if secretNumber is bigger than guessNumber; false - otherwise.
     */
    public boolean isBigger(final int guessNumber) {
        return secretNumber > guessNumber;
    }
    
    /**
     * Returns current count of player guesses.
     * 
     * @return 
     */
    public int countSteps() {
        return playerGuessSteps.size();
    }
    
    /**
     * String representation of all steps made by player.
     * 
     * @return 
     */
    public String playerStepsToString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Secret number: " + secretNumber + "\n");
        
        int counter = 1;
        for (GuessStep guessStep : playerGuessSteps) {
            sb.append(">>Step #" + counter++ + "\n");
            sb.append("\tRange " + guessStep.rangeMin + " - " + guessStep.rangeMax + "\n");
            sb.append("\tPlayer guess: " + guessStep.guessNumber + "\n");
        }
        
        return sb.toString();
    }
    
    public void setRandomGenerator(RandomGenerator randomGenerator) {
        this.randomGenerator = randomGenerator;
    }

    public int getRangeMin() {
        return rangeMin;
    }

    public int getRangeMax() {
        return rangeMax;
    }

    public void setRangeMin(int rangeMin) {
        this.rangeMin = rangeMin;
    }

    public void setRangeMax(int rangeMax) {
        this.rangeMax = rangeMax;
    }

    public int getSecretNumber() {
        return secretNumber;
    }
    
}
