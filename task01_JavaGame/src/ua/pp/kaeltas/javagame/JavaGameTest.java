package ua.pp.kaeltas.javagame;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Some tests
 * 
 * @author Matsokha Andrey
 */
public class JavaGameTest {

    
    public static void main(String[] args) {
        
        JavaGameTest javaGameTest = new JavaGameTest();
        
        int guessNumber = 10;
        
        System.out.println("TEST guess correctness");
        System.out.println("Method result: " + javaGameTest.testGuessCorrect(guessNumber)
                + " Expected value: " + true);
        System.out.println("Method result: " + javaGameTest.testGuessFalse(guessNumber) 
                + " Expected value: " + false);
        
        System.out.println("TEST count of steps");
        System.out.println("Method result: " + javaGameTest.testGuessSteps(0)
                + " Expected value: " + 0);
        System.out.println("Method result: " + javaGameTest.testGuessSteps(1)
                + " Expected value: " + 1);
        System.out.println("Method result: " + javaGameTest.testGuessSteps(5)
                + " Expected value: " + 5);
     
        System.out.println("TEST isBigger");
        System.out.println("Method result: " + javaGameTest.testIsBigger(10, 5)
                + " Expected value: " + true);
        System.out.println("Method result: " + javaGameTest.testIsBigger(10, 10)
                + " Expected value: " + false);
        System.out.println("Method result: " + javaGameTest.testIsBigger(10, 20)
                + " Expected value: " + false);
        
        
        
        //AUTO PLAY
        System.out.println("\n\nAUTO PLAY");
        //Range = 10-20
        //Secret number = 15
        byte[] buf = "10\n20\n18\n12\n17\n13\n15".getBytes();
        InputStream is = new ByteArrayInputStream(buf);
        
        try {
            JavaGameControllerView javaGame = 
                    new JavaGameControllerView(System.out, is);
            
            javaGame.setRandomGenerator(new RandomGenerator() {
                @Override
                public int rand(int min, int max) {
                    return 15;
                }
            });
            
            javaGame.play();
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                //do nothing
            }
        }
    }

    /*
        Test checks when guessNumber equals to secretNumber
    */
    private boolean testGuessCorrect(final int guessNumber) {
        JavaGame javaGame = new JavaGame();
        javaGame.setRandomGenerator(new RandomGenerator() {
                @Override
                public int rand(int min, int max) {
                    return guessNumber;
                }
            });
        javaGame.chooseSecretNumber();
        
        return javaGame.guess(guessNumber);
    }
    
    /*
        Test checks when guessNumber NOT equals to secretNumber
    */
    private boolean testGuessFalse(int guessNumber) {
        JavaGame javaGame = new JavaGame();
        javaGame.setRandomGenerator(new RandomGenerator() {
                @Override
                public int rand(int min, int max) {
                    return guessNumber;
                }
            });
        javaGame.chooseSecretNumber();
        
        return javaGame.guess(guessNumber);
    }
    
    /*
        Test makes guessSteps attempts to guess secretNumber and
        returns number of stored steps
    */
    private int testGuessSteps(final int guessSteps) {
        JavaGame javaGame = new JavaGame();
        javaGame.setRandomGenerator(new RandomGenerator() {
                @Override
                public int rand(int min, int max) {
                    return 1;
                }
            });
        javaGame.chooseSecretNumber();
        
        for (int i = 0; i < guessSteps; i++) {
            javaGame.guess(2);
        }

        return javaGame.countSteps();
    }
    
    /*
        Test if secretNumber is bigger than guessNumber
    */
    private boolean testIsBigger(final int secretNumber, final int guessNumber) {
        JavaGame javaGame = new JavaGame();
        javaGame.setRandomGenerator(new RandomGenerator() {
                @Override
                public int rand(int min, int max) {
                    return secretNumber;
                }
            });
        javaGame.chooseSecretNumber();
        
        return javaGame.isBigger(guessNumber);
    }
    
    
}
