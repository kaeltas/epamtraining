package ua.pp.kaeltas.javagame;

import java.util.Random;

/**
 *
 * @author Matsokha Andrey
 */
interface RandomGenerator {
    
    /**
     * Returns a random number between min and max, both inclusive
     * 
     * @param min Minimum value
     * @param max Maximum value
     * @return random int value between min and max, both inclusive
     */
    public int rand(int min, int max);
    
}
