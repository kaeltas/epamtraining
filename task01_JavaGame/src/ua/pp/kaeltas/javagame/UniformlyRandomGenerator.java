package ua.pp.kaeltas.javagame;

import java.util.Random;

/**
 *
 * @author Matsokha Andrey
 */
public class UniformlyRandomGenerator implements RandomGenerator {
    private Random random;

    public UniformlyRandomGenerator() {
        random = new Random();
    }
    
    /**
     * Returns a random number between min and max, both inclusive
     * 
     * @param min Minimum value
     * @param max Maximum value
     * @return random int value between min and max, both inclusive
     */
    public int rand(int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException("Minimum value must be less than Maximum value");
        }
        
        //topvalue in nextInt is exclusive, 
        // so add +1 to make it inclusive
        int rndValue = min + random.nextInt((max-min)+1);
        
        return rndValue;
    }
    
}
