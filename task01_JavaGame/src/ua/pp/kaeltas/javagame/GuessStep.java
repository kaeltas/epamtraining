package ua.pp.kaeltas.javagame;

/**
 *
 * @author Matsokha Andrey
 */
public class GuessStep {
    public final int guessNumber;
    public final int rangeMin;
    public final int rangeMax;

    public GuessStep(int guessNumber, int rangeMin, int rangeMax) {
        this.guessNumber = guessNumber;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
    }
    
    
}
