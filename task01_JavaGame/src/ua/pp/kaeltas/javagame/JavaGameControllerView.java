package ua.pp.kaeltas.javagame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * Controller and view for JavaGame.
 * 
 * @author Matsokha Andrey
 */
public class JavaGameControllerView {
    
    private OutputStream out;
    private BufferedReader br;
    
    private JavaGame javaGame;

    public JavaGameControllerView() {
        this(System.out, System.in);
    }

    public JavaGameControllerView(OutputStream out, InputStream in) {
        this.out = out;
        this.br = new BufferedReader(new InputStreamReader(in));
                
        javaGame = new JavaGame();
    }
    
    /**
     * Handle the game
     */
    public void play() {
        println("Hi, let's play a game!!!");
        
        print("Enter min range bound:  ");
        int minRange = readInt();
        print("Enter max range bound:  ");
        int maxRange;
        while (true) {
            maxRange = readInt();
            if (maxRange - minRange > 5) {
                break;
            } else {
                println("MaxRange must be five more than MinRange. Your MinRange = " + minRange);
            }
        }
        
        javaGame.setRangeMin(minRange);
        javaGame.setRangeMax(maxRange);
        javaGame.chooseSecretNumber();
        
        println("I have chosen a random number from " + minRange 
                + " to " + maxRange + ", both bounds exclusively.");
        println("Try to guess it, could you? :)");
        println("");
        
        
        while (true) {
            println("Step #" + (javaGame.countSteps()+1));
            println("Secret number is in range: " 
                    + javaGame.getRangeMin() + " - " + javaGame.getRangeMax() 
                    + ", both bounds exclusively");
            print("Enter your guess: ");
            
            int guessNumber = readIntInAllowedRange();

            boolean isCorrect = javaGame.guess(guessNumber);
            if (isCorrect) {
                println("Congratulations! My number is " + guessNumber 
                        + ". You win this game!");
                break;
            } else {
                if (javaGame.isBigger(guessNumber)) {
                    println("My number is bigger than " + guessNumber);
                } else {
                    println("My number is smaller than " + guessNumber);
                }
            }
            
            println("");
        }
        
        println("");
        println("");
        println("-------------STATISTIC-----------------");
        println("---You have made " + javaGame.countSteps() 
                + " steps to win this game---");
        println(javaGame.playerStepsToString());
        
    }
    
    /**
     * Writeln to OutputStream {@code out} string {@code str}.
     * 
     * @param str 
     */
    public void println(String str) {
        try {
            out.write(str.getBytes());
            out.write("\n".getBytes());
            out.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Write to OutputStream {@code out} string {@code str}.
     * 
     * @param str 
     */
    public void print(String str) {
        try {
            out.write(str.getBytes());
            out.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Reads int value from BufferedReader {@code br}.
     * 
     * Reads int value in loop, 
     * until user provides value of correct int format. 
     * Prints error-message to standard output, if value does not correspond 
     * to int format.
     * 
     * @return int
     */
    public int readInt() {
        int readValue = -1;
        do {
            try {
                readValue = Integer.valueOf(br.readLine());
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    //do nothing
                }
            } catch (NumberFormatException e) {
                println("Wrong format. Please input only numbers");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } while (readValue == -1);
        
        return readValue;
    }
    
    /**
     * Read number from InputStream {@code in}.
     * Read number in endless loop, until we get number in allowed range.
     * 
     * @return 
     */
    private int readIntInAllowedRange() {
        int readNumber;
        while (true) {
            readNumber = readInt();
            if (javaGame.isNumberInAllowedRange(readNumber)) {
                break;
            } else {
                println("!!!Please enter number from " + javaGame.getRangeMin() + " to " + javaGame.getRangeMax());
            }
        }
        return readNumber;
    }
    
    public void setRandomGenerator(RandomGenerator randomGenerator) {
        javaGame.setRandomGenerator(randomGenerator);
    }
    
}
