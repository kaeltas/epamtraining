/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.pp.kaeltas.arrays;

import java.util.Arrays;

/**
 *
 * @author Matsokha Andrey
 */
public class SelectionSort {

    public static void main(String[] args) {
        
        SelectionSort selectionSort = new SelectionSort();
        selectionSort.sortTest(new int[] {}, new int[] {});
        selectionSort.sortTest(new int[] {3}, new int[] {3});
        selectionSort.sortTest(new int[] {6, 3}, new int[] {3, 6});
        selectionSort.sortTest(new int[] {1, 2, 3, 4, 5}, 
                new int[] {1, 2, 3, 4, 5});
        selectionSort.sortTest(new int[] {5, 4, 3, 2, 1}, 
                new int[] {1, 2, 3, 4, 5});
        selectionSort.sortTest(new int[] {5, 4, 3, 2}, 
                new int[] {2, 3, 4, 5});
        selectionSort.sortTest(new int[] {3, 2, 5, 1}, 
                new int[] {1, 2, 3, 5});
        
    }
    
    public void sort(int[] array) {
        for (int barrier = 0; barrier < array.length-1; barrier++) {
            onePass(barrier, array);
        }
    }

    private void onePass(int barrier, int[] array) {
        for (int index = barrier+1; index < array.length; index++) {
            conditionalSwap(array, barrier, index);
        }
    }

    private void conditionalSwap(int[] array, int barrier, int index) {
        if (array[barrier] > array[index]) {
            swap(array, barrier, index);
        }
    }

    private void swap(int[] array, int barrier, int index) {
        int tmp = array[barrier];
        array[barrier] = array[index];
        array[index] = tmp;
    }
    
    public void sortTest(int[] array, int[] arrayExpected) {
        System.out.println("Original array:" + Arrays.toString(array));
        new SelectionSort().sort(array);
        System.out.println("Sorted array:  "  + Arrays.toString(array));
        System.out.println("Expected array:"  + Arrays.toString(arrayExpected));
        System.out.println("");
    }
    
    
}
