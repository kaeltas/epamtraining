package ua.pp.kaeltas.arrays;

import java.util.Arrays;

/**
 *
 * @author 
 */
public class MergeSort {
    
    public static void main(String[] args) {
        MergeSort ms = new MergeSort();
        int ar[] = new int[] {4, 2, 7, 0, 3, 6};
        int[] ar2 = ms.sort(ar);
        
        System.out.println(Arrays.toString(ar2));
        
    }
    
    public int[] sort(int[] arr) {
        //int[] aux = new int[arr.length];
        return sort(arr, 0, arr.length-1);
        
    }
    
    public int[] sort(int[] arr, int begin, int end) {
        if (begin >= end) {
            return new int[] {arr[begin]};
        }
        int mid = begin + (end-begin)/2;
        //int mid = (begin+end)/2;
        int[] ar1 = sort(arr, begin, mid);
        int[] ar2 = sort(arr, mid, end);
        
        return merge(ar1, ar2);
    }
    
//    public int[] merge(int[] arr, int beg, int mid, int end) {
//        int[] result = new int[arr1.length + arr2.length];
//        
//        int indexArr1 = 0;
//        int indexArr2 = 0;
//        
//        while (indexArr1 < arr1.length && indexArr2 < arr2.length) {
//            if (arr1[indexArr1] > arr2[indexArr2]) {
//                result[indexArr1+indexArr2] = arr2[indexArr2];
//                indexArr2++;
//            } else {
//                result[indexArr1+indexArr2] = arr1[indexArr1];
//                indexArr1++;
//            }
//        }
//        
//        copyTailOfArray(indexArr1, arr1, indexArr1+indexArr2, result, indexArr2, arr2);
//        
//        return result;
//    }
    
    public int[] merge(int[] arr1, int arr2[]) {
        int[] result = new int[arr1.length + arr2.length];
        
        int indexArr1 = 0;
        int indexArr2 = 0;
        
        while (indexArr1 < arr1.length && indexArr2 < arr2.length) {
            if (arr1[indexArr1] > arr2[indexArr2]) {
                result[indexArr1+indexArr2] = arr2[indexArr2];
                indexArr2++;
            } else {
                result[indexArr1+indexArr2] = arr1[indexArr1];
                indexArr1++;
            }
        }
        
        copyTailOfArray(indexArr1, arr1, indexArr1+indexArr2, result, indexArr2, arr2);
        
        return result;
    }

    private void copyTailOfArray(int indexArr1, int[] arr1, int indexResult, int[] result, int indexArr2, int[] arr2) {
        //copy tail of array, if arrays have had different length
        if (indexArr1 < arr1.length) {
            System.arraycopy(arr1, indexArr1, result, indexResult, arr1.length-indexArr1);
        } else if (indexArr2 < arr2.length) {
            System.arraycopy(arr2, indexArr2, result, indexResult, arr2.length-indexArr2);
        }
    }

}


