/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.pp.kaeltas.arrays;

import java.util.Arrays;

/**
 *
 * @author Matsokha Andrey
 */
public class InsertionSort {
    
    public static void main(String[] args) {
        System.out.println("Insertion sort test");
        InsertionSort insertionSort = new InsertionSort();
        insertionSort.sortTest(new int[] {}, new int[] {});
        insertionSort.sortTest(new int[] {3}, new int[] {3});
        insertionSort.sortTest(new int[] {6, 3}, new int[] {3, 6});
        insertionSort.sortTest(new int[] {1, 2, 3, 4, 5}, 
                new int[] {1, 2, 3, 4, 5});
        insertionSort.sortTest(new int[] {5, 4, 3, 2, 1}, 
                new int[] {1, 2, 3, 4, 5});
        insertionSort.sortTest(new int[] {5, 4, 3, 2}, 
                new int[] {2, 3, 4, 5});
        insertionSort.sortTest(new int[] {3, 2, 5, 1}, 
                new int[] {1, 2, 3, 5});
        
    }
    
    public void sort(int[] array) {
        for (int barrier = 1; barrier < array.length; barrier++) {
            onePass(array, barrier);
        }
    }

    private void onePass(int[] array, int barrier) {
        int newElement = array[barrier];
        int index = findPositionAndMoveElements(barrier-1, array, newElement);
        array[index+1] = newElement;
    }

    private int findPositionAndMoveElements(int index, int[] array, int newElement) {
        while (index >= 0 && array[index] > newElement) {
            array[index+1] = array[index];
            index--;
        }
        return index;
    }
    
    public void sortTest(int[] array, int[] arrayExpected) {
        System.out.println("Original array:" + Arrays.toString(array));
        new InsertionSort().sort(array);
        System.out.println("Sorted array:  "  + Arrays.toString(array));
        System.out.println("Expected array:"  + Arrays.toString(arrayExpected));
        System.out.println("");
    }
    
}
