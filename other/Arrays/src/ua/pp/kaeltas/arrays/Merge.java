/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.pp.kaeltas.arrays;

import java.util.Arrays;

/**
 *
 * @author Matsokha Andrey
 */
public class Merge {

    public static void main(String[] args) {
        
        System.out.println("Merge with duplicates test");
        Merge mergeSort = new Merge();
        mergeSort.mergeWithDuplicatesTest(new int[] {}, new int[] {}, new int[] {});
        mergeSort.mergeWithDuplicatesTest(new int[] {6}, new int[] {3}, new int[] {3, 6});
        mergeSort.mergeWithDuplicatesTest(new int[] {1, 5, 8, 14, 24}, 
                new int[] {9, 10, 15, 20, 24}, 
                new int[] {1, 5, 8, 9, 10, 14, 15, 20, 24, 24});
        mergeSort.mergeWithDuplicatesTest(new int[] {1, 5, 8}, 
                new int[] {9, 10, 15, 20, 24}, 
                new int[] {1, 5, 8, 9, 10, 15, 20, 24});
        mergeSort.mergeWithDuplicatesTest(new int[] {1, 12, 14}, 
                new int[] {9, 10, 15, 20, 24}, 
                new int[] {1, 9, 10, 12, 14, 15, 20, 24});
        
        
        System.out.println("Merge without duplicates test");
        Merge mergeSort2 = new Merge();
        mergeSort2.mergeWithoutDuplicatesTest(new int[] {}, new int[] {}, new int[] {});
        mergeSort2.mergeWithoutDuplicatesTest(new int[] {6}, new int[] {3}, new int[] {3, 6});
        mergeSort2.mergeWithoutDuplicatesTest(new int[] {1, 5, 8, 14, 24}, 
                new int[] {9, 10, 15, 20, 24}, 
                new int[] {1, 5, 8, 9, 10, 14, 15, 20, 24});
        mergeSort2.mergeWithoutDuplicatesTest(new int[] {1, 5, 8}, 
                new int[] {9, 10, 15, 20, 24}, 
                new int[] {1, 5, 8, 9, 10, 15, 20, 24});
        mergeSort2.mergeWithoutDuplicatesTest(new int[] {1, 12, 14}, 
                new int[] {9, 10, 15, 20, 24}, 
                new int[] {1, 9, 10, 12, 14, 15, 20, 24});
        mergeSort2.mergeWithoutDuplicatesTest(new int[] {1, 5, 5, 8, 8, 8}, 
                new int[] {3, 3, 5, 5, 10, 15, 20, 24}, 
                new int[] {1, 3, 5, 8, 10, 15, 20, 24});
        
        
    }
    
    public int[] mergeWithDuplicates(int[] arr1, int arr2[]) {
        int[] result = new int[arr1.length + arr2.length];
        
        int indexArr1 = 0;
        int indexArr2 = 0;
        
        while (indexArr1 < arr1.length && indexArr2 < arr2.length) {
            if (arr1[indexArr1] > arr2[indexArr2]) {
                result[indexArr1+indexArr2] = arr2[indexArr2];
                indexArr2++;
            } else {
                result[indexArr1+indexArr2] = arr1[indexArr1];
                indexArr1++;
            }
        }
        
        copyTailOfArray(indexArr1, arr1, indexArr1+indexArr2, result, indexArr2, arr2);
        
        return result;
    }

    private void copyTailOfArray(int indexArr1, int[] arr1, int indexResult, int[] result, int indexArr2, int[] arr2) {
        //copy tail of array, if arrays have had different length
        if (indexArr1 < arr1.length) {
            System.arraycopy(arr1, indexArr1, result, indexResult, arr1.length-indexArr1);
        } else if (indexArr2 < arr2.length) {
            System.arraycopy(arr2, indexArr2, result, indexResult, arr2.length-indexArr2);
        }
    }
    
    //todo remove duplicates in the same array
    public int[] mergeWithoutDuplicates(int[] arr1, int[] arr2) {
        
        int length = countMergedArrayLength(arr1, arr2);
        int[] result = new int[length];
        
        int indexArr1 = 0;
        int indexArr2 = 0;
        int indexResult = 0;
        while (indexArr1 < arr1.length && indexArr2 < arr2.length) {
            if (arr1[indexArr1] > arr2[indexArr2]) {
                result[indexResult++] = arr2[indexArr2];
                indexArr2++;
            } else if (arr1[indexArr1] < arr2[indexArr2]) {
                result[indexResult++] = arr1[indexArr1];
                indexArr1++;
            } else {
                result[indexResult++] = arr2[indexArr2];
                indexArr1++;
                indexArr2++;
            }
        }
        
        //copy tail of array
        copyTailOfArray(indexArr1, arr1, indexResult, result, indexArr2, arr2);
        
        
        return result;
    }

    private int countMergedArrayLength(int[] arr1, int[] arr2) {
        int indexArr1 = 0;
        int indexArr2 = 0;
        int lengthCounter = 0;
        while (indexArr1 < arr1.length && indexArr2 < arr2.length) {
            if (arr1[indexArr1] > arr2[indexArr2]) {
                lengthCounter++;
                indexArr2++;
            } else if (arr1[indexArr1] < arr2[indexArr2]) {
                lengthCounter++;
                indexArr1++;
            } else {
                lengthCounter++;
                indexArr1++;
                indexArr2++;
            }
        }
        
        //count tail of array, if arrays have had different length
        if (indexArr1 < arr1.length) {
            lengthCounter += arr1.length-indexArr1;
        } else if (indexArr2 < arr2.length) {
            lengthCounter +=  arr2.length-indexArr2;
        }
        
        return lengthCounter;
    }
    
    
    public void mergeWithDuplicatesTest(int[] arr1, int[] arr2, int[] arrExpected) {
        System.out.println("Original arrays:" + Arrays.toString(arr1) 
                + " " + Arrays.toString(arr2));
        int[] result = new Merge().mergeWithDuplicates(arr1, arr2);
        System.out.println("Merged array:  "  + Arrays.toString(result));
        System.out.println("Expected array:"  + Arrays.toString(arrExpected));
        System.out.println("");
    }
    
    public void mergeWithoutDuplicatesTest(int[] arr1, int[] arr2, int[] arrExpected) {
        System.out.println("Original arrays:" + Arrays.toString(arr1) 
                + " " + Arrays.toString(arr2));
        int[] result = new Merge().mergeWithoutDuplicates(arr1, arr2);
        System.out.println("Merged array:  "  + Arrays.toString(result));
        System.out.println("Expected array:"  + Arrays.toString(arrExpected));
        System.out.println("");
    }
    
}
