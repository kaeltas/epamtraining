package lab06;

import java.util.regex.Pattern;

/**
 *
 * Анализ типов аргументов, задаваемых при запуске программы. 
 * Если аргумент является правильным идентификатором языка C 
 * (шаблон: состоит из латинских букв, цифр и символа "$" и "_", 
 * считающегося буквой, и, кроме того, первый символ является буквой), 
 * то его тип "Identifier", 
 * если параметр является ключевым словом C 
 * (для примера задать несколько ключевых слов C, "if", "for", 
 * "while", "do" и "else"), то его тип "Keyword", 
 * иначе его тип считается "Illegal". 
 * Программа выводит количество заданных аргументов и, 
 * для каждого аргумента, его тип и значение.
 * 
 * @author Matsokha Andrey
 */
public class Lab06v28 {
    Pattern patternValidKeyword = 
            Pattern.compile("if|for|while|do|else");
    Pattern patternValidIdentifier = 
            Pattern.compile("[a-z_$]{1}[\\w]+", Pattern.CASE_INSENSITIVE);
    
    
    public static void main(String[] args) {
        String[] argsMock = {
            "param1",
            "$index",
            "_34e",
            "abcabc",
            "21wer",
            "146",
            "",
            "while",
            "for",
            "рри",
            "Яф"
        };
        
        System.out.println("Количество аргументов: " + argsMock.length);
        for (String arg : argsMock) {
            System.out.println(arg);
        }
        
        System.out.println("");
        new Lab06v28().printArgsTypes(argsMock);
    }
    
    /**
     * Print to sout type of each argument in {@code args}.
     * 
     * @param args 
     */
    public void printArgsTypes(String[] args) {
        
        for (String arg : args) {
            if (patternValidKeyword.matcher(arg).matches()) {
                System.out.println("Keyword: " + arg);
            } else if (patternValidIdentifier.matcher(arg).matches()) {
                System.out.println("Identifier: " + arg);
            } else {
                System.out.println("Illegal: " + arg);
            }
        }
        
    }
    
}
