package lab06;

import java.util.regex.Pattern;

/**
 *
 * Преобразование аргументов, задаваемых при запуске программы. 
 * Программа определяет тип аргумент – шестнадцатеричное число без знака 
 * (шаблон: шестнадцатеричным числом без знака считается строка, 
 * которая содержит цифры от 0 до 9 и 
 * буквы A(a), B(b), C(c), D(d), E(e),F(f)) или строка. 
 * Введенные аргументы-числа преобразуются в двоичные числа. 
 * Программа выводит количество заданных аргументов, их значения, 
 * а также количество аргументов-чисел и их двоичные значения.   
 * 
 * @author Matsokha Andrey
 */
public class Lab06v23 {
    Pattern patternHexNumber = Pattern.compile("[0-9abcdef]+", Pattern.CASE_INSENSITIVE);
    
    public static void main(String[] args) {
        
        String[] argsMock = {
            "3",
            "5",
            "B",
            "abcabc",
            "dddd",
            "ab",
            "z",
            "паарр",
            "рри",
            "Яф"
        };
        
        System.out.println("Количество аргументов: " + argsMock.length);
        for (String arg : argsMock) {
            System.out.println(arg);
        }
        
        System.out.println("\nBinary representation:");
        new Lab06v23().printBinaryRepresentation(argsMock);
        
    }
    
    /**
     * Check if {@code str} contains only allowed 
     * hex chars: 0-9, A, B, C, D, E, F.
     * 
     * @param str
     * @return 
     */
    public boolean isHexNumber(String str) {
        return patternHexNumber.matcher(str).matches();
    }
    
    /**
     * Return binary representation of {@code val}.
     * 
     * @param val
     * @return 
     */
    public String toBinaryStr(int val) {
        return Integer.toBinaryString(val);
    }
    
    /**
     * Print to sout - binary representation of each element of {@code args},
     * that is Hex number.
     * 
     * @param args 
     */
    public void printBinaryRepresentation(String[] args) {
        for (String arg : args) {
            if (isHexNumber(arg)) {
                final String binaryStr = toBinaryStr(Integer.valueOf(arg, 16));
                System.out.println(binaryStr);
            }
        }
    }
}
