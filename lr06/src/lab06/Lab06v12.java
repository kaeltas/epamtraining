package lab06;

/**
 *
 * Анализ аргументов, задаваемых при запуске программы. 
 * Программа удаляет из аргументов все повторяющиеся символы, 
 * кроме одного (например, аргумент "abcadc", преобразуется в "abcd"). 
 * Шаблон аргумента: строка либо латинских букв, либо букв кириллицы. 
 * Программа выводит количество заданных аргументов, 
 * их значения и преобразованные значения аргументов. 
 * 
 * 
 * @author Matsokha Andrey
 */
public class Lab06v12 {
    
    public static void main(String[] args) {
        
        String[] argsMock = {
            "abcabc",
            "dddd",
            "ab",
            "z",
            "паарр",
            "рри",
            "Яф"
        };
        
        System.out.println("Количество аргументов: " + argsMock.length);
        for (String arg : argsMock) {
            System.out.println(arg);
        }
        
        System.out.println("\nParams without duplicates:");
        new Lab06v12().printParamsWithoutDuplicates(argsMock);
    }
    
    /**
     * Remove all duplicate characters in {@code arg}.
     * 
     * @param arg
     * @return 
     */
    public String removeDuplicates(String arg) {
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < arg.length(); i++) {
            char c = arg.charAt(i);
            if (sb.toString().indexOf(c) == -1) {
                sb.append(c);
            }
        }
        
        return sb.toString();
    }
    
    public void printParamsWithoutDuplicates(String[] args) {
        for (String arg : args) {
            System.out.println(removeDuplicates(arg));
        }
    }
    
    
}
