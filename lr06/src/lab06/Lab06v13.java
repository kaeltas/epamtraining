package lab06;

/**
 *
 * Анализ аргументов, задаваемых при запуске программы. 
 * Программа определяет строку символов встречающиеся 
 * только в одном из аргументов 
 * (например, для аргументов "agc", "cf", "bfc" 
 * такой строкой будет строка "gb").  
 * Шаблон аргумента: строка либо латинских букв, либо букв кириллицы. 
 * Программа выводит количество заданных аргументов, 
 * их значения и найденную строку символов 
 * или сообщение о том, что таких символов нет. 
 * 
 * @author Matsokha Andrey
 */
public class Lab06v13 {
    
    public static void main(String[] args) {
        String[] argsMock = {
            "abcabc",
            "dddd",
            "ab",
            "z",
            "паарр",
            "рри",
            "Яф"
        };
        
        System.out.println("Количество аргументов: " + argsMock.length);
        for (String arg : argsMock) {
            System.out.println(arg);
        }
        
        
        System.out.println("\nChars that appeared only in one argument:");
        System.out.println(new Lab06v13().doJob(argsMock));
        
    }
    
    public String doJob(String[] args) {
        StringBuilder sb = new StringBuilder();
        StringBuilder sbFoundMoreThanOnce = new StringBuilder();
        
        String[] argsWoDuplicates = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            argsWoDuplicates[i] = removeDuplicates(args[i]);
        }
                
        
        for (String arg : argsWoDuplicates) {
            for (int i = 0; i < arg.length(); i++) {
                char c = arg.charAt(i);
                
                if (sbFoundMoreThanOnce.toString().indexOf(c) == -1) {
                    int index = sb.toString().indexOf(c);
                    if (index == -1) {
                        sb.append(c);
                    } else {
                        sb.deleteCharAt(index);
                        sbFoundMoreThanOnce.append(c);
                    }
                }
            }
            
        }
        
        return sb.toString();
    }
    
    
    /**
     * Remove all duplicate characters in {@code arg}.
     * 
     * @param arg
     * @return 
     */
    public String removeDuplicates(String arg) {
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < arg.length(); i++) {
            char c = arg.charAt(i);
            if (sb.toString().indexOf(c) == -1) {
                sb.append(c);
            }
        }
        
        return sb.toString();
    }
    
}
