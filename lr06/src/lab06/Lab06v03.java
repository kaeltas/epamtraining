package lab06;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Анализ типов аргументов, задаваемых при запуске программы. 
 * Если аргумент является числом с плавающей точкой 
 * (шаблон: состоит из мантиссы – одна или несколько цифр, 
 * возможно со знаком "+" или "-", которая может содержать 
 * десятичную точку в начале, середине или конце, 
 * а также порядка – целого числа со знаком "+" или "-" 
 * или без знака, разделителем между мантиссой 
 * и порядком служит символ "e" или символ "E"), 
 * то  тип аргумента "Float", иначе "String". 
 * Программа выводит количество заданных аргументов и, 
 * для каждого аргумента, его тип и значение. 
 * 
 * @author Matsokha Andrey
 */
public class Lab06v03 {
    private static final Pattern floatPattern = 
            Pattern.compile("[+-]?"
                    + "(?:[.]\\d+|\\d+[.]\\d+|\\d+[.])"
                    + "[eE][+-]?\\d+");
    
    
    public String argDescription(String argument) {
        String result;
        
        Matcher matcher = floatPattern.matcher(argument);
        
        if (matcher.matches()) {
            result = "Float: " + matcher.group();
        } else {
            result = "String: " + argument;
        }
        
        return result;
    }

    public static void main(String[] args) {
        
        Lab06v03 lab06v03 = new Lab06v03();
        
        System.out.println("Args count: " + args.length);
        for (String arg : args) {
            System.out.println(lab06v03.argDescription(arg));
        }
        
        
        System.out.println("TEST");
        String[] argsMock = {
            "hello",
            "1.2e3",
            "+1.2e3",
            "-1.2e3",
            "++1.2e3",
            "1.2e+3",
            "1.2e-3",
            "1.2E+3",
            ".2e3",
            "1.e3",
            "a1.2e3"
        };
        for (String arg : argsMock) {
            System.out.println(lab06v03.argDescription(arg));
        }
        
    }
    
}
