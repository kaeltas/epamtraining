package lab06;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Анализ типов аргументов, задаваемых при запуске программы. 
 * Если аргумент имеет вид "имя=значение", то он является 
 * ключевым параметром (тип "Keyed"), 
 * если аргумент имеет вид "-значение" или "/значение", 
 * то он является опцией (тип "Optional") 
 * и если имеет вид "значение", 
 * то является непосредственным параметром (тип "Immediate"). 
 * Шаблон для значения: 
 * одна или несколько цифр и букв (включая буквы кириллицы). 
 * Программа выводит количество заданных аргументов и, 
 * для каждого аргумента, его тип и значение 
 * (для ключевых параметров дополнительно выводится имя параметра).  
 * 
 * @author Matsokha Andrey
 */
public class Lab06v07 {
    private static final Pattern keyedPattern = 
            Pattern.compile("([\\w]+)=([\\wА-Яа-я]+)");
    private static final Pattern optionalPattern = 
            Pattern.compile("[-/]([\\wА-Яа-я]+)");
    private static final Pattern immediatePattern = 
            Pattern.compile("[\\wА-Яа-я]+");
    
    
    
    public String argDescription(String argument) {
        String result;
        
        Matcher matcher = keyedPattern.matcher(argument);
        if (matcher.matches()) {
            result = "Keyed: " + matcher.group(1) + " = " + matcher.group(2);
        } else if (matcher.usePattern(optionalPattern).matches()) {
            result = "Optional: " + matcher.group(1);
        } else if (matcher.usePattern(immediatePattern).matches()) {
            result = "Immediate: " + matcher.group();
        } else {
            result = "Wrong parameter: " + argument;
        }
        
        
        return result;
    }
    
    
    public static void main(String[] args) {
        
        Lab06v07 lab06v07 = new Lab06v07();
        
        System.out.println("Args count: " + args.length);
        for (String arg : args) {
            System.out.println(lab06v07.argDescription(arg));
        }
        
        
        System.out.println("TEST");
        String[] argsMock = {
            "lang=en",
            "lang=рус",
            "-verbose",
            "/help",
            "привет",
            "-lang=рус"
        };
        for (String arg : argsMock) {
            System.out.println(lab06v07.argDescription(arg));
        }
        
        
    }
    
}
