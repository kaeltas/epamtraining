package lab06;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * Анализ типов аргументов, задаваемых при запуске программы. 
 * Если аргумент является правильным целым числом 
 * (шаблон: одна или несколько цифр, первым символом 
 * может быть либо цифра,  либо знак "+" или "-"), 
 * то  тип аргумента "Integer", иначе "String". 
 * Программа выводит количество заданных аргументов и, 
 * для каждого аргумента, его тип и значение.   
 * 
 * @author Matsokha Andrey
 */
public class Lab06v01 {
    
    private static final Pattern integerPattern = Pattern.compile("[+-]?\\d+");
    
    /**
     * Return type(integer or string) and value of argument. 
     * 
     * @param argument
     * @return 
     */
    public String argDescription(String argument) {
        String result;
        
        Matcher matcher = integerPattern.matcher(argument);
        if (matcher.matches()) {
            result = "Integer: " + matcher.group();
        } else {
            result = "String: " + argument;
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        
        Lab06v01 lab06v01 = new Lab06v01();
        
        System.out.println("Args count: " + args.length);
        for (String arg : args) {
            System.out.println(lab06v01.argDescription(arg));
        }
        
        
        System.out.println("TEST");
        String[] argsMock = {
            "hello",
            "123",
            "+56",
            "qwerty",
            "-12"
        };
        for (String arg : argsMock) {
            System.out.println(lab06v01.argDescription(arg));
        }
        
        
    }
    
    
}
