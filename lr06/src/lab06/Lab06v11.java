package lab06;

/**
 *
 * Анализ аргументов, задаваемых при запуске программы. 
 * Программа определяет, какие символы содержатся во введенных аргументах 
 * (например, аргументы "abc", "cf", "bfc" содержат символы "abcf"). 
 * Шаблон аргумента: строка либо латинских букв, либо букв кириллицы. 
 * Программа выводит количество заданных аргументов, 
 * значения аргументов и строку символов, содержащихся в аргументах.   
 * 
 * 
 * @author Matsokha Andrey
 */
public class Lab06v11 {
    
    public static void main(String[] args) {
        
        String[] argsMock = {
            "abc",
            "d",
            "ab",
            "z",
            "пар",
            "ри",
            "Яф"
        };
        
        System.out.println("Количество аргументов: " + argsMock.length);
        for (String arg : argsMock) {
            System.out.println(arg);
        }
                
        System.out.println(new Lab06v11().getDistinctChars(argsMock));
        
    }
    
    /**
     * Return String that contains all characters that appeared 
     * in each string from {@code args} array.
     * 
     * @param args
     * @return 
     */
    public String getDistinctChars(String[] args) {
        StringBuilder sb = new StringBuilder();
        
        for (String arg : args) {
            for (int i = 0; i < arg.length(); i++) {
                char c = arg.charAt(i);
                if (sb.toString().indexOf(c) == -1) {
                    sb.append(c);
                } 
            }
        }
        
        return sb.toString();
    }
    
}
