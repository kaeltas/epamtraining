/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclasssample.btr;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kaeltas
 */
public class ArmoredCarrier {
    private String modelName;
    private int weight;
    private int wheelNumber;
    private int speed;
    private Engine engine;
    private List<Troop> troops = new ArrayList<>();
    
    enum FuelType {
        DIESEL, GASOLINE, ALL
    }
    
    private class Engine {
        protected String modelName;
        protected int horsePower;
        private FuelType fuelType;

        public Engine(String modelName, int horsePower, FuelType fuelType) {
            this.modelName = modelName;
            this.horsePower = horsePower;
            this.fuelType = fuelType;
        }

        @Override
        public String toString() {
            return "Engine{" + "modelName=" + modelName + ", horsePower=" + horsePower + ", fuelType=" + fuelType + '}';
        }
        
    }
    
    public class GasolineEngine extends Engine {
        private class Carburetor {
            private String modelName;

            @Override
            public String toString() {
                return "Carburetor{" + "modelName=" + modelName 
                        + " of engine hp=" + horsePower 
                        + " " + GasolineEngine.this.modelName + '}';
            }
        }
        
        private Carburetor carb; 

        public GasolineEngine(String modelName, int horsePower) {
            super(modelName, horsePower, FuelType.GASOLINE);
            this.carb = new Carburetor();
            this.carb.modelName = "Carburetor-2000";
        }

        @Override
        public String toString() {
            return "GasolineEngine{" + "carb=" + carb + '}';
        }
    }

    static class Troop {
        public enum Role {
            DRIVER, GUN_OPERATOR, COMMANDER
        }
        
        private String name;
        private Role role;

        public Troop(String name, Role role) {
            this.name = name;
            this.role = role;
        }

        @Override
        public String toString() {
            return "Troop{" + "name=" + name + ", role=" + role + '}';
        }
        
    }
    
    public ArmoredCarrier(String modelName, int weight, int wheelNumber, int speed) {
        this.modelName = modelName;
        this.weight = weight;
        this.wheelNumber = wheelNumber;
        this.speed = speed;
        this.troops.add(new Troop("Paddy", Troop.Role.DRIVER));
        this.engine = new Engine("TD-3", 500, FuelType.DIESEL);
    }

    @Override
    public String toString() {
        return "ArmoredCarrier{" + "modelName=" + modelName + ", weight=" + weight + ", wheelNumber=" + wheelNumber + ", speed=" + speed + ", engine=" + engine + ", troops=" + troops + '}';
    }

    public List<Troop> getTroops() {
        return troops;
    }
    
    public void addTroop(Troop troop) {
        troops.add(troop);
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }
    
    
}
