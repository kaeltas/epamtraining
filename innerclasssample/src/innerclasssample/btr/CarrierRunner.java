/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package innerclasssample.btr;

import java.util.Arrays;

/**
 *
 * @author kaeltas
 */
public class CarrierRunner {
    
    public static void main(String[] args) {
        ArmoredCarrier armoredCarrier =
                new ArmoredCarrier("BTR-4", 8600, 8, 60);
        System.out.println(armoredCarrier);
        
        ArmoredCarrier.Troop troop = 
                new ArmoredCarrier.Troop("Joe", 
                        ArmoredCarrier.Troop.Role.COMMANDER);
        
        armoredCarrier.addTroop(troop);
        
        ArmoredCarrier.GasolineEngine engine = 
                armoredCarrier.new GasolineEngine("Gas engine", 300);
 
        armoredCarrier.setEngine(engine);
        System.out.println(armoredCarrier);
        
        
    }
    
}
