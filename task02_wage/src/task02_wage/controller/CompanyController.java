package task02_wage.controller;

import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import task02_wage.Company;
import task02_wage.CompanyStorage;
import task02_wage.Department;
import task02_wage.Employee;
import task02_wage.wagedivider.WageDivider;
import task02_wage.exception.DepartmentAlreadyExistsException;
import task02_wage.exception.EmployeeAlreadyExistsException;
import task02_wage.exception.NoSuchDepartmentException;
import task02_wage.exception.NoWageReportException;
import task02_wage.view.ReportView;
import task02_wage.view.ReportViewSystemOut;
import task02_wage.wagedivider.EvenlyWageDivider;
import task02_wage.wagedivider.ProportionallyWageDivider;

/**
 *
 * @author Matsokha Andrey
 */
public class CompanyController {
    CompanyStorage companyStorage = CompanyStorage.getInstance();
    ReportView reportView = new ReportViewSystemOut();

    /**
     * Create company and save it to storage.
     * 
     * @param name Company name
     */
    public void createCompany(String name, Company.SalaryDivideMode salaryDivideMode) {
        WageDivider wageDivider;
        switch (salaryDivideMode) {
            case PROPORTIONALLY: {
                wageDivider = new ProportionallyWageDivider();
                break;
            }
            case EVENLY: {
                wageDivider = new EvenlyWageDivider();
                break;
            }
            default: {
                wageDivider = new EvenlyWageDivider();
                break;
            } 
        }
        Company company = new Company(name, wageDivider);
        companyStorage.setCompany(company);
    }
    
    /**
     * Add {@code amount} of money to wage funds of the company.
     * 
     * @param amount 
     */
    public void addWageFunds(int amount) {
        Company company = companyStorage.getCompany();
        company.addWageFunds(amount);
    }
    
    /**
     * Create new department and add it to list of company departments.
     * 
     * @param name Department name
     */
    public void createDepartment(String name) {
        Company company = companyStorage.getCompany();
        Department department = new Department(name);
        try {
            company.addDepartment(department);
        } catch (DepartmentAlreadyExistsException ex) {
            reportView.printError(ex.getMessage());
        }
        
    }
    
    /**
     * Get department by name.
     * 
     * @param name
     * @return 
     */
    public Department getDepartment(String name) {
        Company company = companyStorage.getCompany();
        
        try {
            Department department = company.getDepartment(name);
            return department;
        } catch (NoSuchDepartmentException ex) {
            reportView.printError(ex.getMessage());
        }
        
        return null; //make compiler happy
    }
    
    /**
     * Create new employee and assign it to department.
     * 
     * @param department employee department
     * @param name employee name
     * @param baseSalary employee base salary
     * @param birthdate employee birthdate
     */
    public void createEmployee(Department department, String name, 
            int baseSalary, Calendar birthdate) {
        try {
            department.addEmployee(new Employee(name, baseSalary, birthdate));
        } catch (EmployeeAlreadyExistsException ex) {
            reportView.printError(ex.getMessage());
        }
    }
    
    public void printCompanyEmployees() {
        Company company = companyStorage.getCompany();
        
        reportView.printCompany(company);
    }
    
    public void divideWage() {
        Company company = companyStorage.getCompany();
        company.createWageDivideReport();
        try {
            reportView.printReport(company.getLasWageReport());
        } catch (NoWageReportException ex) {
            reportView.printError(ex.getMessage());
        }
    }
    
}
