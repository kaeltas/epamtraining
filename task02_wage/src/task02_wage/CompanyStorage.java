package task02_wage;

/**
 * Storage for one company.
 * @author Matsokha Andrey
 */
public class CompanyStorage {
    private static CompanyStorage companyStorage;
    private Company company;
    
    public static CompanyStorage getInstance() {
        if (companyStorage == null) {
            companyStorage = new CompanyStorage();
        }
        return companyStorage;
    }
    
    private CompanyStorage() {
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
}
