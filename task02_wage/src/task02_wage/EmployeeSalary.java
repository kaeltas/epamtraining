package task02_wage;

/**
 *
 * @author Matsokha Andrey
 */
public class EmployeeSalary {
    private int baseSalary;
    private int birthdayBonus;
    private int usualBonus;

    public EmployeeSalary(int baseSalary, int birthdayBonus, int usualBonus) {
        this.baseSalary = baseSalary;
        this.birthdayBonus = birthdayBonus;
        this.usualBonus = usualBonus;
    }

    public int getSumSalary() {
        return baseSalary+birthdayBonus+usualBonus;
    }
    
    public int getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(int baseSalary) {
        this.baseSalary = baseSalary;
    }

    public int getBirthdayBonus() {
        return birthdayBonus;
    }

    public void setBirthdayBonus(int birthdayBonus) {
        this.birthdayBonus = birthdayBonus;
    }

    public int getUsualBonus() {
        return usualBonus;
    }

    public void setUsualBonus(int usualBonus) {
        this.usualBonus = usualBonus;
    }

    
}
