package task02_wage;

import task02_wage.wagedivider.WageDivider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import task02_wage.exception.DepartmentAlreadyExistsException;
import task02_wage.exception.NoSuchDepartmentException;
import task02_wage.exception.NoWageReportException;

/**
 *
 * @author Matsokha Andrey
 */
public class Company {
    
    public enum SalaryDivideMode{
        PROPORTIONALLY, EVENLY
    }
    
    public static final String CURRENCY = "UAH";
    public static final int BIRTHDAY_BONUS = 150;
    
    private final String name;
    private List<Department> departments = new ArrayList<>();
    private List<WageReport> wageReports = new ArrayList<>();
    private int wageFund;
    
    private WageDivider wageDivider;

    public Company(String name, WageDivider wageDivider) {
        this.name = name;
        this.wageDivider = wageDivider;
        wageFund = 0;
    }
    
    public void subtractWageFund(int amount) {
        wageFund -= amount;
    }

    public void addWageFunds(int amount) {
        wageFund += amount;
    }

    public int getWageFund() {
        return wageFund;
    }
    
    /**
     * Return sum of base salaries of all employees in all departments.
     * 
     * @return 
     */
    public int getNeededSalaryAmount() {
        int salary = 0;
        for (Department department : departments) {
            salary += department.getNeededSalaryAmount();
        }
        return salary;
    }
    
    public void addDepartment(Department department) throws DepartmentAlreadyExistsException {
        if (departments.indexOf(new Department((name))) > 0) {
            throw new DepartmentAlreadyExistsException("Department with name [" 
                    + name + "] already exists!");
        }
        departments.add(department);
    }
    
    public void addReport(WageReport wageReport) {
        wageReports.add(wageReport);
    }
    
    public Department getDepartment(String name) throws NoSuchDepartmentException {
        int index = departments.indexOf(new Department(name));
        if (index < 0) {
            throw new NoSuchDepartmentException("No department with name [" 
                    + name + "]");
        }
        return departments.get(index);
    }

    public String getName() {
        return name;
    }

    public List<Department> getDepartments() {
        return Collections.unmodifiableList(departments);
    }
    
    public void createWageDivideReport() {
        WageReport wageReport = wageDivider.createDivideWageReport(this);
        wageReports.add(wageReport);
    }
    
    public WageReport getLasWageReport() throws NoWageReportException {
        if (wageReports.size() <= 0) {
            throw new NoWageReportException("No reports available!");
        } 
        return wageReports.get(wageReports.size()-1);
    }
    
    
}
