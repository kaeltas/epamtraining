package task02_wage.exception;

/**
 *
 * @author Matsokha Andrey
 */
public class DepartmentAlreadyExistsException extends Exception {

    public DepartmentAlreadyExistsException(String message) {
        super(message);
    }

}
