package task02_wage.exception;

/**
 *
 * @author Matsokha Andrey
 */
public class EmployeeAlreadyExistsException extends Exception {

    public EmployeeAlreadyExistsException(String message) {
        super(message);
    }
    
}
