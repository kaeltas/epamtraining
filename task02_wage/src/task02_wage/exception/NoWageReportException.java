package task02_wage.exception;

/**
 *
 * @author Matsokha Andrey
 */
public class NoWageReportException extends Exception {

    public NoWageReportException(String message) {
        super(message);
    }
    
}
