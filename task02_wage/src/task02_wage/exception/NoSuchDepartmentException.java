package task02_wage.exception;

/**
 *
 * @author Matsokha Andrey
 */
public class NoSuchDepartmentException extends Exception {

    public NoSuchDepartmentException(String message) {
        super(message);
    }
    
}
