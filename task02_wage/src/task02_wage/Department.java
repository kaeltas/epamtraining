package task02_wage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import task02_wage.exception.EmployeeAlreadyExistsException;

/**
 *
 * @author Matsokha Andrey
 */
public class Department {
    private String name;
    private List<Employee> employees = new ArrayList<>();

    public Department(String name) {
        this.name = name;
    }
    
    /**
     * Return sum of base salaries of all employees in this department.
     * 
     * @return 
     */
    public int getNeededSalaryAmount() {
        int salary = 0;
        for (Employee employee : employees) {
            salary += employee.getBaseSalary();
        }
        return salary;
    }

    public void addEmployee(Employee employee) throws EmployeeAlreadyExistsException {
        if (employees.indexOf(employee) > 0) {
            throw new EmployeeAlreadyExistsException("Employee [" 
                    + employee.getName() + "] already exists!");
        }
        employees.add(employee);
    }

    public List<Employee> getEmployees() {
        return Collections.unmodifiableList(employees);
    }

    public String getName() {
        return name;
    }
    
    /**
     * Use only {@code name} to calculate hashCode.
     * 
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.name);
        return hash;
    }

    /**
     * Use only {@code name} to equals.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Department other = (Department) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    
    
}
