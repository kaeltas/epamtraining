package task02_wage.wagedivider;

import task02_wage.Company;
import task02_wage.Department;
import task02_wage.Employee;
import task02_wage.WageReport;

/**
 *
 * @author Matsokha Andrey
 */
public class EvenlyWageDivider extends WageDivider {

    @Override
    protected void doAddUsualBonus(WageReport wageReport, Company company) {
        int currentWageFund = company.getWageFund();
        
        int usualBonus = currentWageFund / getCompanyEmployeesCount(company);
        
        for (Department department : company.getDepartments()) {
            for (Employee employee : department.getEmployees()) {
                wageReport.addUsualBonus(employee, usualBonus);
                company.subtractWageFund(usualBonus);
            }
        }
    }

    private int getCompanyEmployeesCount(Company company) {
        int employeesCount = 0;
        for (Department department : company.getDepartments()) {
            for (Employee employee : department.getEmployees()) {
                employeesCount++;
            }
        }
        return employeesCount;
    }
    
}
