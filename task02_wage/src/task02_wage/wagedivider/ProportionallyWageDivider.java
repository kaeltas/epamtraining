package task02_wage.wagedivider;

import task02_wage.Company;
import task02_wage.Department;
import task02_wage.Employee;
import task02_wage.WageReport;

/**
 *
 * @author Matsokha Andrey
 */
public class ProportionallyWageDivider extends WageDivider {

    @Override
    protected void doAddUsualBonus(WageReport wageReport, Company company) {
        int currentWageFund = company.getWageFund();
        
        int sunmEmployeesWage = getSumEmployeesWage(company);
        
        for (Department department : company.getDepartments()) {
            for (Employee employee : department.getEmployees()) {
                double bonusKoef = ((double)employee.getBaseSalary() / sunmEmployeesWage);
                int usualBonus = (int)(currentWageFund * bonusKoef);
                wageReport.addUsualBonus(employee, usualBonus);
                company.subtractWageFund(usualBonus);
            }
        }
    }
    
    private int getSumEmployeesWage(Company company) {
        int sumEmployeesWage = 0;
        for (Department department : company.getDepartments()) {
            for (Employee employee : department.getEmployees()) {
                sumEmployeesWage += employee.getBaseSalary();
            }
        }
        return sumEmployeesWage;
    }
    
}
