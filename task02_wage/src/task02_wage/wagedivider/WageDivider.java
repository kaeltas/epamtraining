package task02_wage.wagedivider;

import java.util.Calendar;
import java.util.GregorianCalendar;
import task02_wage.Company;
import task02_wage.Department;
import task02_wage.Employee;
import task02_wage.WageReport;

/**
 *
 * @author Matsokha Andrey
 */
public abstract class WageDivider {

    public WageReport createDivideWageReport(Company company) {
        WageReport wageReport = new WageReport("Month Wage Report", 
                new GregorianCalendar());
        
        doAddBaseSalary(wageReport, company);
        doAddBirthdayBonus(wageReport, company);
        doAddUsualBonus(wageReport, company);
        
        return wageReport;
    }
    
    private void doAddBaseSalary(WageReport wageReport, Company company) {
        for (Department department : company.getDepartments()) {
            for (Employee employee : department.getEmployees()) {
                wageReport.addBaseSalary(employee, employee.getBaseSalary());
                company.subtractWageFund(employee.getBaseSalary());
            }
        }
    }
    
    private void doAddBirthdayBonus(WageReport wageReport, Company company) {
        for (Department department : company.getDepartments()) {
            for (Employee employee : department.getEmployees()) {
                if (employee.getBirthdate().get(Calendar.MONTH) == 
                        wageReport.getDate().get(Calendar.MONTH)) {
                    wageReport.addBirthdayBonus(employee, Company.BIRTHDAY_BONUS);
                    company.subtractWageFund(employee.getBaseSalary());
                }
            }
        }
    }
    
    protected abstract void doAddUsualBonus(WageReport wageReport, Company company);
}
