package task02_wage;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matsokha Andrey
 */
public class WageReport {
    private String name;
    private Calendar date;
    private Map<Employee, EmployeeSalary> salaries = new HashMap<>();

    public WageReport(String name, Calendar date) {
        this.name = name;
        this.date = date;
    }
    
    public String getName() {
        return name;
    }

    public Calendar getDate() {
        return date;
    }

    public Map<Employee, EmployeeSalary> getSalaries() {
        return Collections.unmodifiableMap(salaries);
    }

    public void addBaseSalary(Employee employee, int baseSalary) {
        EmployeeSalary employeeSalary = salaries.containsKey(employee) ? 
                salaries.get(employee) : 
                new EmployeeSalary(0, 0, 0);
        employeeSalary.setBaseSalary(baseSalary);
        salaries.put(employee, employeeSalary);
    }
    
    public void addBirthdayBonus(Employee employee, int bonus) {
        if (!salaries.containsKey(employee)) {
            throw new IllegalStateException("Can't add bonus! Employee doesn't have base salary!");
        }
        EmployeeSalary employeeSalary = salaries.get(employee);
        employeeSalary.setBirthdayBonus(bonus);
        salaries.put(employee, employeeSalary);
    }
    
    public void addUsualBonus(Employee employee, int bonus) {
        if (!salaries.containsKey(employee)) {
            throw new IllegalStateException("Can't add bonus! Employee doesn't have base salary!");
        }
        EmployeeSalary employeeSalary = salaries.get(employee);
        employeeSalary.setUsualBonus(bonus);
        salaries.put(employee, employeeSalary);
    }
    
    
    
}
