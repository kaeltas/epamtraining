package task02_wage.view;

import task02_wage.Company;
import task02_wage.WageReport;

/**
 *
 * @author Matsokha Andrey
 */
public interface ReportView {
    /**
     * Print report {@code wageReport}.
     * 
     * @param wageReport 
     */
    public void printReport(WageReport wageReport);
    
    public void printError(String msg);
    
    public void printSuccess(String msg);

    public void printCompany(Company company);
    
}
