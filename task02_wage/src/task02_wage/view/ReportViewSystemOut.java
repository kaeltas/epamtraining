package task02_wage.view;

import java.text.SimpleDateFormat;
import java.util.Map;
import task02_wage.Company;
import task02_wage.Department;
import task02_wage.Employee;
import task02_wage.EmployeeSalary;
import task02_wage.WageReport;

/**
 *
 * @author Matsokha Andrey
 */
public class ReportViewSystemOut implements ReportView {

    @Override
    public void printReport(WageReport wageReport) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MM dd");
        System.out.println("\nWage Report: " + wageReport.getName());
        System.out.println("Date: " + sdf.format(wageReport.getDate().getTime()));
        System.out.println("Wage of employees");
        for (Map.Entry<Employee, EmployeeSalary> entry : wageReport.getSalaries().entrySet()) {
            EmployeeSalary es = entry.getValue();
            System.out.println(entry.getKey().getName() + ": " 
                    + es.getSumSalary() 
                    + " (base " + es.getBaseSalary() + ", " 
                    + "birthdayBonus " + es.getBirthdayBonus() + ", "
                    + "bonus " + es.getUsualBonus() + ")" );
        }
    }

    @Override
    public void printError(String msg) {
        System.out.println("Error: " + msg);
    }

    @Override
    public void printSuccess(String msg) {
        System.out.println(msg);
    }

    @Override
    public void printCompany(Company company) {
        System.out.println("\nCompany: " + company.getName()
                + " (current wage fund: " + company.getWageFund() + " " 
                + Company.CURRENCY + ")");
        for (Department department : company.getDepartments()) {
            System.out.println(" - Department: " + department.getName());
            for (Employee employee : department.getEmployees()) {
                System.out.println("   - " + employee.getName() 
                        + " " + employee.getBaseSalary() + " " 
                        + Company.CURRENCY);
            }
        }
    }
    
}
