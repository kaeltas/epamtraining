package task02_wage;

import java.util.GregorianCalendar;
import task02_wage.controller.CompanyController;

/**
 *
 * @author Matsokha Andrey
 */
public class Runner {
    private CompanyController companyController = new CompanyController();
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Runner runner = new Runner();
        
        runner.fillCompany();
        runner.addCompanyWageFund();
        runner.printCompany();
        
        runner.divideWage();
        
        runner.printCompany();
    }
    
    private void fillCompany() {
        //companyController.createCompany("MyCompany", Company.SalaryDivideMode.EVENLY);
        companyController.createCompany("MyCompany", Company.SalaryDivideMode.PROPORTIONALLY);
        companyController.createDepartment("Accounting");
        companyController.createDepartment("Development");
        companyController.createDepartment("Management");
        
        Department department;
        department = companyController.getDepartment("Accounting");
        companyController.createEmployee(department, "Paddy", 800, 
                new GregorianCalendar(1980, 0, 12));
        companyController.createEmployee(department, "Ann", 400, 
                new GregorianCalendar(1983, 2, 15));
        
        department = companyController.getDepartment("Development");
        companyController.createEmployee(department, "Joe", 2000, 
                new GregorianCalendar(1985, 6, 20));
        companyController.createEmployee(department, "Seamus", 1000, 
                new GregorianCalendar(1984, 2, 26));
        companyController.createEmployee(department, "Patric", 500, 
                new GregorianCalendar(1990, 8, 22));
        
        department = companyController.getDepartment("Management");
        companyController.createEmployee(department, "Alice", 600, 
                new GregorianCalendar(1985, 4, 10));
        companyController.createEmployee(department, "Barbara", 500, 
                new GregorianCalendar(1985, 3, 4));
        
    }
    
    private void printCompany() {
        companyController.printCompanyEmployees();
    }
    
    private void addCompanyWageFund() {
        companyController.addWageFunds(10_000);
    }

    private void divideWage() {
        companyController.divideWage();
    }
    
}
