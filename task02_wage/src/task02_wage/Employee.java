package task02_wage;

import java.util.Calendar;
import java.util.Objects;

/**
 *
 * @author Matsokha Andrey
 */
public class Employee {
    private String name;
    private int baseSalary;
    private Calendar birthdate;

    public Employee(String name, int baseSalary, Calendar birthdate) {
        this.name = name;
        this.baseSalary = baseSalary;
        this.birthdate = birthdate;
    }

    public String getName() {
        return name;
    }

    public int getBaseSalary() {
        return baseSalary;
    }

    public Calendar getBirthdate() {
        return birthdate;
    }

    /**
     * Use only name to calculate hashCode.
     * @return 
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.name);
        return hash;
    }

    /**
     * Use only name to equals.
     * @param obj
     * @return 
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employee other = (Employee) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
    
    
    
}
