package ua.pp.kaeltas.taskarrays;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Matsokha Andrey
 */
public class T18 {

    /**
     * Stores multiplication taskslist.
     * multiplicationTasks[i][0] - stores first number,
     * multiplicationTasks[i][1] - stores second number.
     */
    private int[][] multiplicationTasks;

    /**
     * Creates tasklist with {@code count} random distinct multiplication tasks,
     * from minNum*minNum to maxNum*maxNum.
     * 
     * @param minNum
     * @param maxNum
     * @param count 
     */
    public T18(int count) {
        multiplicationTasks = new int[count][2];
    }
    
    /**
     * Fills tasklist with random distinct multiplication tasks,
     * from minNum*minNum to maxNum*maxNum.
     * 
     * @param minNum
     * @param maxNum
     * @param count
     * @return 
     */
    public void fillRandomDistinctMultiplicationQuestions(int minNum, int maxNum) {
        if (maxNum < minNum) {
            throw new IllegalArgumentException("maxNum must be > minNum");
        }
        
        clearMultiplicationTasks();
        
        int count = multiplicationTasks.length;
        while (count > 0) {
            Random random = new Random();
            int num1 = random.nextInt(maxNum-minNum+1)+minNum;
            int num2 = random.nextInt(maxNum-minNum+1)+minNum;
            if (isUniqMultiplicationTask(num1, num2)) {
                count--;
                multiplicationTasks[count][0] = num1;
                multiplicationTasks[count][1] = num2;
            }
        }
    }

    /**
     * Clears tasklist.
     */
    private void clearMultiplicationTasks() {
        for (int i = 0; i < multiplicationTasks.length; i++) {
            multiplicationTasks[i][0] = 0;
            multiplicationTasks[i][1] = 0;
        }
    }

    /**
     * Checks if task num1*num2 or num2*num1 already added to tasklist.
     * 
     * @param num1
     * @param num2
     * @return true, if task num1*num2 or num2*num1 is not in tasklist; false - otherwise.
     */
    private boolean isUniqMultiplicationTask(int num1, int num2) {
        for (int i = 0; i < multiplicationTasks.length; i++) {
            if ((multiplicationTasks[i][0] == num1 && multiplicationTasks[i][1] == num2) 
                || (multiplicationTasks[i][0] == num2 && multiplicationTasks[i][1] == num1)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < multiplicationTasks.length; i++) {
            sb.append((i+1) + ")" + multiplicationTasks[i][0] + "x" + multiplicationTasks[i][1] + " = ?\n");
        }
        return sb.toString();
    }
    
    public static void main(String[] args) {
        T18 t18 = new T18(15);
        t18.fillRandomDistinctMultiplicationQuestions(2, 9);
        System.out.println(t18);
    }
}
