package ua.pp.kaeltas.taskarrays;

import java.util.Arrays;

/**
 * Создайте массив из 11 случайных целых чисел из отрезка [-1;1], 
 * выведите массив на экран в строку. 
 * Определите какой элемент встречается в массиве чаще всего 
 * и выведите об этом сообщение на экран. 
 * Если два каких-то элемента встречаются одинаковое количество раз, 
 * то не выводите ничего.
 * @author 
 */
public class T10 {

    public static void main(String[] args) {
        T10 t10 = new T10();
        int elem = t10.findMostFreq(new int[] {1, 5, 0, 4, 1, 5, 1, 3});
        System.out.println(elem);
    }
    
    public int findMostFreq(int[] arr) {
        int maxFreq = getFreq(arr, arr[0]);
        int maxFreqIndex = 0;
        
        for (int i = 1; i < arr.length; i++) {
            int freq = getFreq(arr, arr[i]);
            if (freq > maxFreq) {
                maxFreq = freq;
                maxFreqIndex = i;
            }
        }
               
        return arr[maxFreqIndex];
    }

    public int getFreq(int[] arr, int val) {
        int freqCount = 0;
        for (int elem : arr) {
            if (elem == val) {
                freqCount++;
            }
        }
        return freqCount;
    }
    
}
