package ua.pp.kaeltas.taskarrays;

import java.util.Random;

/**
 * 
 * Создать двумерный массив из 6 строк по 7 столбцов 
 * в каждой из случайных целых чисел из отрезка [0;9]. 
 * Вывести массив на экран. 
 * Преобразовать массив таким образом, чтобы 
 * на первом месте в каждой строке стоял её наибольший элемент. 
 * При этом изменять состав массива нельзя, 
 * а можно только переставлять элементы в рамках одной строки. 
 * Порядок остальных элементов строки не важен 
 * (т.е. можно соврешить только одну перестановку, 
 * а можно отсортировать по убыванию каждую строку). 
 * Вывести преобразованный массив на экран.
 *
 * @author Matsokha Andrey
 */
public class T17 {
    
   /**
     * Returns array of size NxM filled with random numbers 
     * from {@code rangeMin}(inclusively) to {@code rangeMax}(inclusively).
     * 
     * @param N
     * @param M
     * @param rangeMin
     * @param rangeMax
     * @return 
     */
    public int[][] createArray(int N, int M, int rangeMin, int rangeMax) {
        int[][] result = new int[N][M];
        
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                Random random = new Random();
                int rnd = random.nextInt(rangeMax-rangeMin+1) + rangeMin;
                result[i][j] = rnd;
            }
        }
        
        return result;
    }
    
    /**
     * Return index of max element in array.
     * 
     * @param arr
     * @return 
     */
    public int getIndexOfMaxElement(int[] arr) {
        if (arr.length < 1) {
            throw new IllegalArgumentException("Array length must be > 0");
        }
        int maxElem = arr[0];
        int maxElemIndex = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > maxElem) {
                maxElem = arr[i];
                maxElemIndex = i;
            }
        }
        
        return maxElemIndex;
    }
    
    public void setFirstElemInEachRowOfArrayToMaxInRow(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            setFirstElemInArrayToMaxElement(arr[i]);
        }
    }
    
    public void setFirstElemInArrayToMaxElement(int[] arr) {
        if (arr.length < 1) {
            throw new IllegalArgumentException("Array length must be > 0");
        }
        int maxElemIndex = getIndexOfMaxElement(arr);
        int tmp = arr[maxElemIndex];
        arr[maxElemIndex] = arr[0];
        arr[0] = tmp;
    }
    
    
    /**
     * Prints array NxM in rows and columns.
     * 
     * @param arr 
     */
    public void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            printArrayInLine(arr[i]);
            System.out.println("");
        }
    }

    /**
     * Print array in line, elements separated by ' ' character.
     * 
     * @param arr 
     */
    private void printArrayInLine(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
    
    public static void main(String[] args) {
        System.out.println("TEST");
        
        T17 t17 = new T17();
        int[][] arr = t17.createArray(6, 7, 0, 9);
        
        System.out.println("Original array");
        t17.printArray(arr);
        t17.setFirstElemInEachRowOfArrayToMaxInRow(arr);
        System.out.println("Modified array");
        t17.printArray(arr);
    }
}
