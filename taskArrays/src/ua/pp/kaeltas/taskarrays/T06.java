package ua.pp.kaeltas.taskarrays;

import java.util.Arrays;
import java.util.Random;

/**
 * Создайте массив из 4 случайных целых чисел из отрезка [10;99], 
 * выведите его на экран в строку. 
 * Определить и вывести на экран сообщение о том, 
 * является ли массив строго возрастающей последовательностью.
 * 
 * @author Matsokha Andrey
 */
public class T06 {
    
    /**
     * Returns 4 random elements array.
     * Each element - is a random number from {@code minRange}(inclusively) 
     * to {@code maxRange}(inclusively)
     * 
     * @param minRange
     * @param maxRange
     * @return 
     */
    public int[] createArray(int minRange, int maxRange) {
        int[] result = new int[4];
        
        for (int i = 0; i < result.length; i++) {
            Random random = new Random();
            int rnd = minRange + random.nextInt(maxRange - minRange + 1);
            result[i] = rnd;
        }
        
        return result;
    }
    
    
    /**
     * Checks if array {@code arr} is in ascending order (a[i] > a[i+1])
     * 
     * @param arr
     * @return true, if array is in ascending order; false - otherwise.
     */
    public boolean checkAscending(int[] arr) {
        for (int i = 0; i < arr.length-1; i++) {
            if (arr[i] >= arr[i+1]) {
                return false;
            }
        }
        
        return true;
    }
    
    //TEST
    public static void main(String[] args) {
        
        T06 t06 = new T06();
        
        System.out.println("TEST");
        
        int[] arr = t06.createArray(10, 99);
        System.out.println(Arrays.toString(arr));
        System.out.println(t06.checkAscending(arr));
        
        System.out.println("");
        int[] testArr = {10, 54, 85, 90};
        System.out.println(t06.checkAscending(testArr));
        System.out.println("Expected: true");
        
        System.out.println("");
        int[] testArr2 = {10, 50, 22, 90};
        System.out.println(t06.checkAscending(testArr2));
        System.out.println("Expected: false");
        
    }
    
}
