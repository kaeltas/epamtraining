package ua.pp.kaeltas.taskarrays;

/**
 * Создайте массив из всех нечётных чисел от 1 до 99, 
 * выведите его на экран в строку, 
 * а затем этот же массив выведите на экран тоже в строку, 
 * но в обратном порядке (99 97 95 93 … 7 5 3 1).
 * 
 * @author Matsokha Andrey
 */

public class T02 {

    /**
     * Returns array filled with odd numbers from 1 to {@code maxNumber}
     * 
     * @param maxNumber
     * @return 
     */
    public int[] createOddNumbersArray(int maxNumber) {
        int count = (maxNumber%2 != 0) ? maxNumber/2+1 : maxNumber/2;
        int[] result = new int[count];
        
        for (int i = 0; i < result.length; i++) {
            result[i] = i*2+1;
        }
        return result;
    }
    
    /**
     * Prints array {@code arr} on single line, 
     * elements are divided by ' ' (space)
     * Example: 1, 3, 5, 7, ...
     * 
     * @param arr 
     */
    public void printArray(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }
    
    /**
     * Prints array {@code arr} on single line in reverse order, 
     * elements are divided by ' ' (space)
     * Example: 99, 97, 95, 93, ...
     * 
     * @param arr 
     */
    public void printArrayReverse(int[] arr) {
        for (int i = arr.length-1; i >= 0; i--) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }
    
    //TEST
    public static void main(String[] args) {
        System.out.println("TEST");
        
        T02 t02 = new T02();

        int[] arr = t02.createOddNumbersArray(99);
        t02.printArray(arr);
        t02.printArrayReverse(arr);
        
        System.out.println("");
        arr = t02.createOddNumbersArray(19);
        t02.printArray(arr);
        t02.printArrayReverse(arr);
        
        System.out.println("");
        arr = t02.createOddNumbersArray(18);
        t02.printArray(arr);
        t02.printArrayReverse(arr);
        
        System.out.println("");
        arr = t02.createOddNumbersArray(17);
        t02.printArray(arr);
        t02.printArrayReverse(arr);
        
        System.out.println("");
        arr = t02.createOddNumbersArray(20);
        t02.printArray(arr);
        t02.printArrayReverse(arr);
    }
    
}
