package ua.pp.kaeltas.taskarrays;

/**
 * Создайте массив из всех чётных чисел от 2 до 20 и выведите 
 * элементы массива на экран сначала в строку,
 * отделяя один элемент от другого пробелом, 
 * а затем в столбик (отделяя один элемент от другого началом новой строки).
 * Перед созданием массива подумайте, какого он будет размера.
 * 2 4 6 … 18 20
 * 2
 * 4
 * 6
 * …
 * 20
 * 
 * @author Matsokha Andrey
 */
public class T01 {
    
    /**
     * Returns array filled with even numbers from 2 to {@code maxNumber}
     * 
     * @param maxNumber
     * @return 
     */
    public int[] createEvenNumbersArray(int maxNumber) {
        int[] result = new int[maxNumber/2];
        
        for (int i = 0; i < result.length; i++) {
            result[i] = (i+1)*2;
        }
        
        return result;
    }
    
    /**
     * Prints array {@code arr} on single line, 
     * elements are divided by ' ' (space)
     * 
     * @param arr 
     */
    public void printArrayOnSingleLine(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("");
    }
    
    /**
     * Prints array {@code arr} on multiple lines, 
     * elements are divided by '\n'
     * 
     * @param arr 
     */
    public void printArrayOnMultipleLines(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
    
    //TEST
    public static void main(String[] args) {
        System.out.println("TEST");
        
        T01 t01 = new T01();
        int[] arr = t01.createEvenNumbersArray(20);
        
        t01.printArrayOnSingleLine(arr);
        t01.printArrayOnMultipleLines(arr);
    }
    
}
