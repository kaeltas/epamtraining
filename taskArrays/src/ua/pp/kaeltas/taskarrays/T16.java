package ua.pp.kaeltas.taskarrays;

import java.util.Arrays;
import java.util.Random;

/**
 * Cоздать двумерный массив из 7 строк по 4 столбца 
 * в каждой из случайных целых чисел из отрезка [-5;5]. 
 * Вывести массив на экран. 
 * Определить и вывести на экран индекс строки 
 * с наибольшим по модулю произведением элементов. 
 * Если таких строк несколько, то вывести индекс 
 * первой встретившейся из них.
 * 
 * 
 * @author Matsokha Andrey
 */
public class T16 {
   
    /**
     * Returns array of size NxM filled with random numbers 
     * from {@code rangeMin}(inclusively) to {@code rangeMax}(inclusively).
     * 
     * @param N
     * @param M
     * @param rangeMin
     * @param rangeMax
     * @return 
     */
    public int[][] createArray(int N, int M, int rangeMin, int rangeMax) {
        int[][] result = new int[N][M];
        
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                Random random = new Random();
                int rnd = random.nextInt(rangeMax-rangeMin+1) + rangeMin;
                result[i][j] = rnd;
            }
        }
        
        return result;
    }
    
    /**
     * Returns index of row with maximum absolute product of elements.
     * 
     * @param arr
     * @return 
     */
    public int getIndexOfRowWithMaxAbsProduct(int[][] arr) {
        if (arr.length < 1) {
            throw new IllegalArgumentException("Array length must be > 0");
        }
        int maxProductRowIndex = 0;
        int maxProduct = getArrayAbsProduct(arr[0]);
        for (int i = 1; i < arr.length; i++) {
            int rowProduct = getArrayAbsProduct(arr[i]);
            if (rowProduct > maxProduct) {
                maxProduct = rowProduct;
                maxProductRowIndex = i;
            }
        }
        
        return maxProductRowIndex;
    }

    /**
     * Return absolute product of elements of array.
     * 
     * @param arr
     * @return 
     */
    private int getArrayAbsProduct(int[] arr) {
        if (arr.length < 1) {
            throw new IllegalArgumentException("Array length must be > 0");
        }
        int product = Math.abs(arr[0]);
        for (int i = 1; i < arr.length; i++) {
            product *= Math.abs(arr[i]);
        }
        
        return product;
    }
    
    /**
     * Prints array NxM in rows and columns.
     * 
     * @param arr 
     */
    public void printArray(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            printArrayInLine(arr[i]);
            System.out.println("");
        }
    }

    /**
     * Print array in line, elements separated by ' ' character.
     * 
     * @param arr 
     */
    private void printArrayInLine(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }
    
    /**
     * Prints array NxM in rows and columns 
     * and prints absolute product of elements in the end of each row.
     * 
     * @param arr 
     */
    public void printArrayWithProduct(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            printArrayInLine(arr[i]);
            System.out.print(" (" + getArrayAbsProduct(arr[i]) + ")");
            System.out.println("");
        }
    }
    
    //TEST
    public static void main(String[] args) {
        
        System.out.println("TEST");
        
        T16 t16 = new T16();
        
        int[][] arr = t16.createArray(7, 4, -5, 5);
        t16.printArrayWithProduct(arr);
        System.out.println(t16.getIndexOfRowWithMaxAbsProduct(arr));
        
    }
    
    
}
