package ua.pp.kaeltas.lab04;

import java.util.Arrays;

/**
 *
 * @author Matsokha Andrey
 */
public class Lab04SecondTest {
    
    public static void main(String[] args) {
        
        System.out.println("TEST COUNT");
        Lab04SecondTest lab04SecondTest = new Lab04SecondTest();
        
        lab04SecondTest.calcFullCountOfEqualElementsTest(
                new int[] {1, 4, 2, 54, 20},
                new int[] {3, 4, 8, 55, 20}, 
                2);
        lab04SecondTest.calcFullCountOfEqualElementsTest(
                new int[] {1, 4, 2, 54, 20},
                new int[] {3, 0, 8, 55, 27}, 
                0);
        lab04SecondTest.calcFullCountOfEqualElementsTest(
                new int[] {1, 4, 8},
                new int[] {3, 0, 8, 55, 27}, 
                1);
        lab04SecondTest.calcFullCountOfEqualElementsTest(
                new int[] {3, 0, 8, 55, 27}, 
                new int[] {1, 4, 8},
                1);
        lab04SecondTest.calcFullCountOfEqualElementsTest(
                new int[] {},
                new int[] {}, 
                0);
        
        System.out.println("TEST INDEXES");
        
        lab04SecondTest.getFullEqualElementsIndexesTest(
                new int[] {8, 4, 2, 8, 20},
                new int[] {3, 4, 8, 55, 20}, 
                new int[][] {
                    {0, 2},
                    {1, 1},
                    {3, 2},
                    {4, 4}
                });
        lab04SecondTest.getFullEqualElementsIndexesTest(
                new int[] {1, 4, 2, 54, 20},
                new int[] {3, 0, 8, 55, 27}, 
                new int[][] {{}});
        lab04SecondTest.getFullEqualElementsIndexesTest(
                new int[] {1, 27, 8},
                new int[] {3, 0, 8, 55, 27}, 
                new int[][] {
                    {1, 4},
                    {2, 2}
                });
        lab04SecondTest.getFullEqualElementsIndexesTest(
                new int[] {3, 0, 8, 55, 27}, 
                new int[] {1, 27, 8},
                new int[][] {
                    {2, 2},
                    {4, 1}
                });
        lab04SecondTest.getFullEqualElementsIndexesTest(
                new int[] {},
                new int[] {}, 
                new int[][] {{}});
        
    }
    
    public void calcFullCountOfEqualElementsTest(int[] a, int[] b, int expectedCount) {
        Lab04Second lab04 = new Lab04Second();
        System.out.println("a: " + Arrays.toString(a));
        System.out.println("b: " + Arrays.toString(b));
        int count = lab04.calcFullCountOfEqualElements(a, b);
        System.out.println("count:          " + count);
        System.out.println("Expected count: " + expectedCount);
        System.out.println("");
    }
    
    public void getFullEqualElementsIndexesTest(int[] a, int[] b, int[][] expected) {
        Lab04Second lab04 = new Lab04Second();
        System.out.println("a: " + Arrays.toString(a));
        System.out.println("b: " + Arrays.toString(b));
        int[][] indexes = lab04.getFullEqualElementsIndexes(a, b);
        System.out.println("indexes:  " + Arrays.deepToString(indexes));
        System.out.println("Expected: " + Arrays.deepToString(expected));
        System.out.println("");
    }
    
   
    
}
