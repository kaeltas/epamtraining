package ua.pp.kaeltas.lab04;

/**
 *
 * Вариант 1-28
 * Определить количество равных элементов (a[i] == b[i]) и их индексы
 * для двух исходных одномерных массивов a и b.
 * 
 * @author Matsokha Andrey
 */
public class Lab04First {

    /**
     * Returns count of equal elements, where a[i] == b[i].
     * 
     * @param a first array
     * @param b second array
     * @return 
     */
    public int calcCountOfEqualElements(int [] a, int [] b) {
        int minLength = a.length < b.length ? a.length : b.length;
        int countEquals = 0;
        for (int i = 0; i < minLength; i++) {
            if (a[i] == b[i]) {
                countEquals++;
            }
        }
        
        return countEquals;
    }
    
    /**
     * Returns array with indexes, which correspond to equal elements
     * in arrays {@code a} and {@code b} (Equal means: a[i] == b[i])
     * 
     * @param a first array
     * @param b second array
     * @return 
     */
    public int[] getEqualElementsIndexes(int[] a, int[] b) {
        int[] indexes = new int[calcCountOfEqualElements(a, b)];
        
        int minLength = a.length < b.length ? a.length : b.length;
        int indexesIndex = 0;
        for (int i = 0; i < minLength; i++) {
            if (a[i] == b[i]) {
                indexes[indexesIndex++] = i;
            }
        }
        
        return indexes;
    }
    
}
