package ua.pp.kaeltas.lab04;

import java.util.Arrays;

/**
 * Сформировать и вывести на дисплей одномерный массив b, 
 * в котором первыми элементами являются  элементы  
 * исходного одномерного массива a с отрицательными значениями 
 * (с сохранением порядка следования), 
 * а затем элементы a с нулевыми и положительными значениями. 
 * 
 * @author 
 */
public class Lab04v01 {
    
    public int[] groupArray(int[] arr) {
        int[] result = new int[arr.length];
        
        int indexResult = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                result[indexResult++] = arr[i];
            }
        }
        for (int val : arr) {
            if (val == 0) {
                result[indexResult++] = val;
            }
        }
        for (int val : arr) {
            if (val > 0) {
                result[indexResult++] = val;
            }
        }
        
        return result;
    }
    
    
    
    public static void main(String[] args) {
        Lab04v01 lab04v01 = new Lab04v01();
        int[] arr = lab04v01.groupArray(new int[] {0, 1, 4, 0, -3, 0, 5, -2});
        System.out.println(Arrays.toString(arr));
        
        
        
    }
    
}
