package ua.pp.kaeltas.lab04;

import java.util.Arrays;

/**
 *
 * @author Matsokha Andrey
 */
public class Lab04FirstTest {
    
    public static void main(String[] args) {
        
        System.out.println("TEST COUNT");
        Lab04FirstTest lab04Test = new Lab04FirstTest();
        
        lab04Test.calcCountOfEqualElementsTest(
                new int[] {1, 4, 2, 54, 20},
                new int[] {3, 4, 8, 55, 20}, 
                2);
        lab04Test.calcCountOfEqualElementsTest(
                new int[] {1, 4, 2, 54, 20},
                new int[] {3, 0, 8, 55, 27}, 
                0);
        lab04Test.calcCountOfEqualElementsTest(
                new int[] {1, 4, 8},
                new int[] {3, 0, 8, 55, 27}, 
                1);
        lab04Test.calcCountOfEqualElementsTest(
                new int[] {3, 0, 8, 55, 27}, 
                new int[] {1, 4, 8},
                1);
        lab04Test.calcCountOfEqualElementsTest(
                new int[] {},
                new int[] {}, 
                0);
        
        System.out.println("TEST INDEXES");
        
        lab04Test.getEqualElementsIndexesTest(
                new int[] {1, 4, 2, 54, 20},
                new int[] {3, 4, 8, 55, 20}, 
                new int[] {1, 4});
        lab04Test.getEqualElementsIndexesTest(
                new int[] {1, 4, 2, 54, 20},
                new int[] {3, 0, 8, 55, 27}, 
                new int[] {});
        lab04Test.getEqualElementsIndexesTest(
                new int[] {1, 4, 8},
                new int[] {3, 0, 8, 55, 27}, 
                new int[] {2});
        lab04Test.getEqualElementsIndexesTest(
                new int[] {3, 0, 8, 55, 27}, 
                new int[] {1, 4, 8},
                new int[] {2});
        lab04Test.getEqualElementsIndexesTest(
                new int[] {},
                new int[] {}, 
                new int[] {});
        
    }
    
    public void calcCountOfEqualElementsTest(int[] a, int[] b, int expectedCount) {
        Lab04First lab04 = new Lab04First();
        System.out.println("a: " + Arrays.toString(a));
        System.out.println("b: " + Arrays.toString(b));
        int count = lab04.calcCountOfEqualElements(a, b);
        System.out.println("count:          " + count);
        System.out.println("Expected count: " + expectedCount);
        System.out.println("");
    }
    
    public void getEqualElementsIndexesTest(int[] a, int[] b, int[] expected) {
        Lab04First lab04 = new Lab04First();
        System.out.println("a: " + Arrays.toString(a));
        System.out.println("b: " + Arrays.toString(b));
        int[] indexes = lab04.getEqualElementsIndexes(a, b);
        System.out.println("indexes:  " + Arrays.toString(indexes));
        System.out.println("Expected: " + Arrays.toString(expected));
        System.out.println("");
    }
    
   
    
}
