package ua.pp.kaeltas.lab04;

/**
 *
 * Вариант 1-28
 * Определить количество равных элементов (a[i] == b[j] => i, j) и их индексы
 * для двух исходных одномерных массивов a и b.
 * 
 * @author Matsokha Andrey
 */
public class Lab04Second {

    /**
     * Returns count of equal elements, where a[i] == b[j].
     * 
     * @param a first array
     * @param b second array
     * @return 
     */
    public int calcFullCountOfEqualElements(int [] a, int [] b) {
        int countEquals = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                if (a[i] == b[j]) {
                    countEquals++;
                }
            }
        }
        return countEquals;
    }
    
    /**
     * Returns array with indexes, which correspond to equal elements
     * in arrays {@code a} and {@code b} (a[i] == b[j])
     * 
     * @param a first array
     * @param b second array
     * @return 
     */
    public int[][] getFullEqualElementsIndexes(int[] a, int[] b) {
        int[][] indexes = new int[calcFullCountOfEqualElements(a, b)][2];

        int indexesIndex = 0;
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                if (a[i] == b[j]) {
                    indexes[indexesIndex][0] = i;
                    indexes[indexesIndex][1] = j;
                    indexesIndex++;
                }
            }
        }
        
        return indexes;
    }
    
    
}
