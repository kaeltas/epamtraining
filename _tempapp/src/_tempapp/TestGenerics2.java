package _tempapp;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 *
 * @author Matsokha Andrey
 */
public class TestGenerics2 {
    
    public static void main(String[] args) throws NoSuchFieldException {
        
        Adder<Integer> adder1 = new Adder<>();
        
        Class clazz = adder1.getClass();
        Field field = clazz.getDeclaredField("sum");
        System.out.println(field.getType().getName());
        
        Method [] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            System.out.println("Method " + method.getName() + " Return type: " + method.getReturnType().getName());
        }
        

        
        Adder<Integer> adder = new Adder<>(new Integer (1));

        System.out.println(adder.getSum());
        adder.add(3);
        System.out.println(adder.getSum());
        //System.out.println(adder.getSum().getClass());
        
        
    }
    
}

class Adder<E extends Number> {
    E sum;

    public Adder(E sum) {
        this.sum = sum;
    }
    
    public Adder() {
    }


    void add(E val) {
        System.out.println("val.class = " + val.getClass());
        Double tmp = val.doubleValue();
        System.out.println("tmp.class = " + tmp.getClass());
        tmp += sum.doubleValue();
        System.out.println("sum.class before assign = " + sum.getClass());
        sum = (E)tmp;
        
        System.out.println("sum.class after assign = " + sum.getClass());
    }

    E getSum() {
        System.out.println("sum.class in getSum() = " + sum.getClass());
        return (E)sum;
    }
    
}