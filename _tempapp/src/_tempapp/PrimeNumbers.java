/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _tempapp;

/**
 *
 * @author Matsokha Andrey
 */
public class PrimeNumbers {
    
    public static void main(String[] args) {
        num:
        for (int num = 2; num <= 100; num++) {
            int n = (int)Math.sqrt(num) + 1;
            while (--n != 1) {
                if (num%n == 0) {
                    continue num;
                }
            }
            System.out.println(num + " ");
        }
    }
    
}
