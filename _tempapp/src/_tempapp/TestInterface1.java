package _tempapp;

/**
 *
 * @author Matsokha Andrey
 */
public class TestInterface1 implements Inter1, Inter2 {

    @Override
    public void f(int number) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void f(short number) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

interface Inter1 {
    void f(int number);
    
    void f(short number);
}

interface Inter2 {
    void f(int number);
}
