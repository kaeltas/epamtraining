package _tempapp;

/**
 *
 * @author Matsokha Andrey
 */
public class TestStaticInheritance {
    public static void main(String[] args) {
        StaticInheritance01.val = 3;
        StaticInheritanceChild01.val = 5;
        
        System.out.println(StaticInheritance01.val);
    }
}

class StaticInheritance01 {
    static int val = 2;
}

class StaticInheritanceChild01 extends StaticInheritance01 {
    
}
    