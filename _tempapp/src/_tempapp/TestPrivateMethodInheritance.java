package _tempapp;

/**
 *
 * @author Matsokha Andrey
 */
public class TestPrivateMethodInheritance {
    public static void main(String[] args) {
        PrivateMethodInheritance parent = new PrivateMethodInheritanceChild();
        //parent.print(); <- private method, can't call
    }
}

class PrivateMethodInheritance {
    private void print() {
        System.out.println("1");
    }
}

class PrivateMethodInheritanceChild extends PrivateMethodInheritance {
    void print() {
        System.out.println("2");
    }
}