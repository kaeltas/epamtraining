package _tempapp;

import java.util.Arrays;

/**
 *
 * @author Matsokha Andrey
 */
public class EnumTest {
    
    public static void main(String[] args) {
        
        System.out.println(Month.JANUARY.getClass() instanceof Object);
        System.out.println(Arrays.toString(Month.values()));
    }
    
}

enum Month {
    JANUARY, FEBRUARY;
}