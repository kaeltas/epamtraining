package _tempapp;

/**
 *
 * @author Matsokha Andrey
 */
public class TestInterface2 {
    
}

interface Auto {
    void print();
    void f();
    Number f1();
}

interface Truck extends Auto {
    @Override
    void print();
    //int f(); //<- compile error
    @Override
    Integer f1();
}

interface Car extends Auto {
    @Override
    void print();
}

class Car1 implements Truck, Car {

    @Override
    public void print() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Integer f1() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void f() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}