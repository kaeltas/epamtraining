package _tempapp;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Matsokha Andrey
 */
public class TestGenerics {
    
    public static void main(String[] args) {
        
        List<A> listA = new ArrayList<>();
        listA.add(new A());
        listA.add(new B());
        
        f(new ArrayList<A>());
        
    }
    
    static void f(List<A> list) {
        
        
    }
        
    
}

class A {}

class B extends A {}
