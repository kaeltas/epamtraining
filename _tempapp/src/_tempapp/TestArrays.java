package _tempapp;

/**
 *
 * @author Matsokha Andrey
 */
public class TestArrays {
    
    public static void main(String[] args) {
        
        Integer[] arrInt = {1, 2, 3};
        Number[] arrNum = arrInt;
        arrNum[2] = Short.valueOf("4");
        
        
        for (Number elem : arrNum) {
            System.out.println(elem);
        }
        
    }
    
    
    
}
