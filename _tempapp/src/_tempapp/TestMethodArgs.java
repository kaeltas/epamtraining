/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _tempapp;

import java.util.Arrays;

/**
 *
 * @author Matsokha Andrey
 */
public class TestMethodArgs {
    
    public static void main(String[] args) {
        TestMethodArgs testMethodArgs = new TestMethodArgs();
        
        int xA = 1;
        testMethodArgs.methodA(xA);
        System.out.println("xA = " + xA);
        
        Integer xB = 2;
        testMethodArgs.methodB(xB);
        System.out.println("xB = " + xB);
        
        int [] xC = {3, 3};
        testMethodArgs.methodC(xC);
        System.out.println("xC = " + Arrays.toString(xC));
        
        MyNumber xD = new MyNumber(4);
        testMethodArgs.methodD(xD);
        System.out.println("xD = " + xD.number);
        
        MyNumber xE = new MyNumber(5);
        testMethodArgs.methodE(xE);
        System.out.println("xE = " + xE.number);
    }
    
    void methodA(int arg) {
        arg = 15;
    }
    
    void methodB(Integer arg) {
        arg = 18;
    }
    
    void methodC(int [] arg) {
        arg = new int[] {14, 14, 19};
    }
    
    void methodD(MyNumber arg) {
        arg = new MyNumber(28);
    }
    
    void methodE(MyNumber arg) {
        arg.number = 29;
    }
    
}

class MyNumber {
    int number;

    public MyNumber(int number) {
        this.number = number;
    }
    
}