package _tempapp;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Matsokha Andrey
 */
public class TestGenerics3 {
    
    public static void main(String[] args) {
        
        Generics3 gen3 = new Generics3();
        
        List<Integer> listInt = new ArrayList<Integer>(){
            {
                add(1);
                add(3);
                add(5);
            }};
        List<Double> listDbl = new ArrayList<Double>(){
            {
                add(1d);
                add(3d);
                add(5d);
            }};
        List<Number> listNum = new ArrayList<Number>(){
            {
                add(1);
                add(3d);
                add(5f);
            }};
        
        gen3.f1(listInt);
        gen3.f1(listDbl);
        gen3.f1(listNum);
        
        gen3.f2(listInt);
        gen3.f2(listDbl);
        gen3.f2(listNum);
        
    }
    
}

class Generics3 {
    
    public <T extends Number> void f1(List<T> list) {
        for (T elem : list) {
            System.out.println(elem);
        }
    }
    
    public void f2(List<? extends Number> list) {
        for (Number elem : list) {
            System.out.println(elem);
        }
    }
            
}


class Generics3addon {

    public static void main(String[] args) {
        
        ArrayList<? extends ZA> arrayZA_covariant;
        
        ArrayList<ZA> arrayA = new ArrayList<>();
        ArrayList<ZB> arrayB = new ArrayList<>();
        ArrayList<ZC> arrayC = new ArrayList<>();
        ArrayList<ZD> arrayD = new ArrayList<>();
        
        arrayZA_covariant = arrayA;
        arrayZA_covariant = arrayB;
        arrayZA_covariant = arrayC;
        arrayZA_covariant = arrayD;

        ZA varA = arrayZA_covariant.get(0);

              
        //error: no suitable method found
//        arrayZA_covariant.add(new ZB());
//        arrayZA_covariant.add(new ZC());
        
//        ArrayList<ZA> arrayZA1_covariant = new ArrayList<>();
//        arrayZA1_covariant.add(new ZB());
//        arrayZA1_covariant.add(new ZC());
//        arrayZA1_covariant.add(new ZD());
        
    }
    
    void f(ArrayList<? extends ZA> list) {
        for (ZA elem : list) {
            System.out.println(elem);
        }
        //list.add(new ZB());
        
    }
    
    void g(ArrayList<? super ZC> list) {
//        for (ZB elem : list) {
//            System.out.println(elem);
//        }
        //ZA z = list.get(3);
        
        list.add(new ZD());
        
    }
    
}

class ZA {}

class ZB extends ZA{}

class ZC extends ZB {}

class ZD extends ZC {}

