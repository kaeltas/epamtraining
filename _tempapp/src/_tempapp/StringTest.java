package _tempapp;

/**
 *
 * @author Matsokha Andrey
 */
public class StringTest {
    
    public static void main(String[] args) {
        
        String s1 = "qwerty";
        StringBuilder sb = new StringBuilder(s1);
        System.out.println(sb.toString());
        StringBuilder sb2 = sb.reverse();
        
        System.out.println(sb.toString());
        System.out.println(sb2.toString());
        System.out.println(sb == sb2);
        
        
    }
    
}
