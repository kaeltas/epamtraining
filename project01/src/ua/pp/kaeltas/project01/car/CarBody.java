package ua.pp.kaeltas.project01.car;

/**
 * Class represents different types of car body.
 * 
 * @author Matsokha Andrey
 */
public enum CarBody {
    SEDAN, HATCHBACK, UNIVERSAL, LIMUSINE, MINIBUS
}
