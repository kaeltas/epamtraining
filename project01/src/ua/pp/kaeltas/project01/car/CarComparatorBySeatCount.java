package ua.pp.kaeltas.project01.car;

import java.util.Comparator;

/**
 * Comparator that compares cars by seat count.
 * 
 * @author Matsokha Andrey
 */
public class CarComparatorBySeatCount implements Comparator<Car> {

    @Override
    public int compare(Car car1, Car car2) {
        return car1.getSeatCount() - car2.getSeatCount();
    }
    
}
