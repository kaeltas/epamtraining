package ua.pp.kaeltas.project01.car;

import java.util.Comparator;

/**
 * Class represents a single car.
 * It consist of basic car parameters, like brand, model, price, maximum speed,
 * seat count, etc.
 * It implements Comparable interface, and by default sorts cars by petrol usage.
 * 
 * @author Matsokha Andrey
 */
public class Car implements Comparable<Car> {
    public final static Comparator<Car> COMPARATOR_BY_PRICE = 
            new CarComparatorByPrice();
    public final static Comparator<Car> COMPARATOR_BY_SEATCOUNT = 
            new CarComparatorBySeatCount();
    
    private String brandName; // brand name of a car
    private String modelName; // model name of a car
    private CarBody carBody; // car body configuration: hatchback, sedan, etc..
    private int price; // price of a car
    private double petrolUsage; // petrol usage per 100 kilometers
    private int maxSpeed; // maximum speed of a car by specification
    private int seatCount; // seat count including driver seat 

    public Car(String brandName, String modelName, CarBody carBody, int price, 
            double petrolUsage, int maxSpeed, int seatCount) {
        this.brandName = brandName;
        this.modelName = modelName;
        this.carBody = carBody;
        this.price = price;
        this.petrolUsage = petrolUsage;
        this.maxSpeed = maxSpeed;
        this.seatCount = seatCount;
    }
    
    /**
     * Compare by petrol usage.
     * @param that
     * @return 
     */
    @Override
    public int compareTo(Car that) {
        return (int)Math.signum(petrolUsage - that.petrolUsage);
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getPetrolUsagePer100Km() {
        return petrolUsage;
    }

    public void setPetrolUsagePer100Km(double petrolUsagePer100Km) {
        this.petrolUsage = petrolUsagePer100Km;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public String getBrandName() {
        return brandName;
    }

    public String getModelName() {
        return modelName;
    }

    public CarBody getCarBody() {
        return carBody;
    }

    @Override
    public String toString() {
        return "Car{" + "brandName=" + brandName 
                + ", modelName=" + modelName 
                + ", carBody=" + carBody 
                + ", price=" + price 
                + ", petrolUsage=" + petrolUsage 
                + ", maxSpeed=" + maxSpeed 
                + ", seatCount=" + seatCount + '}';
    }
    
    
    
}
