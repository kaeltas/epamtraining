package ua.pp.kaeltas.project01.car;

import java.util.Comparator;

/**
 * Comparator that compares cars by price.
 * 
 * @author Matsokha Andrey
 */
public class CarComparatorByPrice implements Comparator<Car> {

    @Override
    public int compare(Car car1, Car car2) {
        return car1.getPrice() - car2.getPrice();
    }
    
}
