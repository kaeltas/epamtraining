package ua.pp.kaeltas.project01.main;

import ua.pp.kaeltas.project01.car.CarBody;

/**
 * Class that demonstrates usage of TaxiStation.
 * 
 * @author Matsokha Andrey
 */
public class Runner {
    
    private TaxiStationController taxiStationController = 
            new TaxiStationController(new TaxiStationViewSystemOut());

    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.test();
    }
    
    private void test() {
        TaxiStationFillTestData taxiStationFillTestData = 
                new TaxiStationFillTestData(taxiStationController);
        taxiStationFillTestData.fillTestData();

        taxiStationController.printTotalPrice();
        System.out.println("");
        
        taxiStationController.printTaxiStation();
        System.out.println("");
        taxiStationController.printFilteredBySpeedRange(190, 220);
        System.out.println("");
        taxiStationController.printFilteredBySpeedRange(120, 170);
        
        System.out.println("");
        taxiStationController.printFilteredByCarBody(CarBody.SEDAN);
        
        System.out.println("");
        taxiStationController.printSorted(false);
        System.out.println("");
        taxiStationController.printSorted(true);
        
        System.out.println("");
        taxiStationController.printSortedByPrice(false);
    }
}
