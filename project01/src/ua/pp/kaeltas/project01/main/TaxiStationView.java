package ua.pp.kaeltas.project01.main;

import java.util.List;
import ua.pp.kaeltas.project01.car.Car;

/**
 * Instances of classes that implements this interface are used as View 
 * in MVC pattern. It visualizes data to end user.
 * 
 * @author Matsokha Andrey
 */
public interface TaxiStationView {
    
    /**
     * Print list of cars.
     * 
     * @param cars 
     */
    public void print(List<Car> cars);
    
    /**
     * Print text message {@code msg}.
     * @param msg 
     */
    public void print(String msg);
    
    /**
     * Print error text message {@code msg}.
     * 
     * @param msg 
     */
    public void printError(String msg);
}
