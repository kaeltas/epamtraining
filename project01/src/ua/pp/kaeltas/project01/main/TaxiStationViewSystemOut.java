package ua.pp.kaeltas.project01.main;

import java.util.List;
import ua.pp.kaeltas.project01.car.Car;

/**
 * Class used as View in MVC pattern, it visualizes data to System.out stream.
 * 
 * @author Matsokha Andrey
 */
public class TaxiStationViewSystemOut implements TaxiStationView {

    @Override
    public void print(List<Car> cars) {
        int counter = 0;
        for (Car car : cars) {
            System.out.println(++counter + ") " + car);
        }
    }

    @Override
    public void print(String msg) {
        System.out.println(msg);
    }

    @Override
    public void printError(String msg) {
        System.out.println("Error: " + msg);
    }
    
}
