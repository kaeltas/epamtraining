package ua.pp.kaeltas.project01.main;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import ua.pp.kaeltas.project01.car.Car;
import ua.pp.kaeltas.project01.car.CarBody;
import ua.pp.kaeltas.project01.taxistation.TaxiStation;
import ua.pp.kaeltas.project01.taxistation.filter.Filter;
import ua.pp.kaeltas.project01.taxistation.filter.FilterByCarBody;
import ua.pp.kaeltas.project01.taxistation.filter.FilterBySpeedRange;

/**
 * Class used as Controller in MVC pattern, it handles user requests. 
 * 
 * @author Matsokha Andrey
 */
public class TaxiStationController {
    private TaxiStation taxiStation = new TaxiStation();
    private TaxiStationView view;

    public TaxiStationController(TaxiStationView view) {
        this.view = view;
    }
    
    /**
     * Add car to taxi station.
     * 
     * @param car 
     */
    public void addCar(Car car) {
        taxiStation.addCar(car);
    }
    
    /**
     * Print all cars in taxi station.
     */
    public void printTaxiStation() {
        view.print("All cars: ");
        view.print(taxiStation.getCars());
    }
    
    /**
     * Print taxi station cars filtered by speed range.
     * 
     * @param minSpeed
     * @param maxSpeed 
     */
    public void printFilteredBySpeedRange(int minSpeed, int maxSpeed) {
        view.print("Filtered cars by speed range (" 
                + minSpeed + " - " + maxSpeed + ") :" );
        printFiltered(new FilterBySpeedRange(minSpeed, maxSpeed));
    }
    
    /**
     * Print taxi station cars filtered by car body.
     * 
     * @param carBody 
     */
    public void printFilteredByCarBody(CarBody carBody) {
        view.print("Filtered cars by car body (" + carBody + ") :" );
        printFiltered(new FilterByCarBody(carBody));
    }
    
    /**
     * Print taxi station cars filtered by given {@code filter}.
     * 
     * @param filter 
     */
    private void printFiltered(Filter filter) {
            List<Car> filteredCars = taxiStation.getFiltered(filter);
            view.print(filteredCars);
    }
    
    /**
     * Print taxi station cars sorted by petrol usage.
     * 
     * @param reverse if true - reverse sorting order
     */
    public void printSorted(boolean reverse) {
        String strReverse = (reverse) ? "(reverse)" : "";
        view.print("Sorted cars by petrol usage " + strReverse + ":");
        
        List<Car> sortedCars = taxiStation.getSorted();
        if (reverse) {
            Collections.reverse(sortedCars);
        }
        view.print(sortedCars);
    }
    
    /**
     * Print taxi station cars sorted by price.
     * 
     * @param reverse if true - reverse sorting order
     */
    public void printSortedByPrice(boolean reverse) {
        String strReverse = (reverse) ? "(reverse)" : "";
        view.print("Sorted cars by price " + strReverse + ":");
        if (reverse) {
            printSorted(Collections.reverseOrder(Car.COMPARATOR_BY_PRICE));
        } else {
            printSorted(Car.COMPARATOR_BY_PRICE);
        }
    }
    
    /**
     * Print taxi station cars sorted by seatcount.
     * 
     * @param reverse if true - reverse sorting order
     */
    public void printSortedBySeatCount(boolean reverse) {
        String strReverse = (reverse) ? "(reverse)" : "";
        view.print("Sorted cars by seatcount " + strReverse + ":");
        if (reverse) {
            printSorted(Collections.reverseOrder(Car.COMPARATOR_BY_SEATCOUNT));
        } else {
            printSorted(Car.COMPARATOR_BY_SEATCOUNT);
        }
    }
    
    /**
     * Print taxi station cars sorted by given {@code comparator}.
     * 
     * @param comparator 
     */
    private void printSorted(Comparator<Car> comparator) {
        List<Car> sortedCars = taxiStation.getSorted(comparator);
        view.print(sortedCars);
    }
    
    /**
     * Print taxi station total price.
     */
    public void printTotalPrice() {
        view.print("Taxi station total price: " + taxiStation.getTotalPrice());
    }

}
