package ua.pp.kaeltas.project01.main;

import ua.pp.kaeltas.project01.car.Car;
import ua.pp.kaeltas.project01.car.CarBody;

/**
 * Class fills TaxiStation with sample test data.
 * 
 * @author Matsokha Andrey
 */
public class TaxiStationFillTestData {
    private TaxiStationController taxiStationController;

    public TaxiStationFillTestData(TaxiStationController taxiStationController) {
        this.taxiStationController = taxiStationController;
    }
    
    /**
     * Fills TaxiStation with some predefined cars.
     */
    public void fillTestData() {
        taxiStationController.addCar(new Car("BMW", "520", 
                CarBody.SEDAN, 15000, 9.5, 180, 5));
        taxiStationController.addCar(new Car("Mercedes", "Sprinter", 
                CarBody.MINIBUS, 18000, 13.5, 140, 20));
        taxiStationController.addCar(new Car("Volksvagen", "LT", 
                CarBody.MINIBUS, 17200, 14.5, 150, 20));
        taxiStationController.addCar(new Car("Citroen", "Jumper", 
                CarBody.MINIBUS, 14000, 12.5, 160, 8));
        taxiStationController.addCar(new Car("Honda", "Accord", 
                CarBody.SEDAN, 16000, 8.5, 220, 5));
        taxiStationController.addCar(new Car("Ford", "Focus", 
                CarBody.HATCHBACK, 13000, 9.0, 180, 5));
        taxiStationController.addCar(new Car("Ford", "Focus MAX", 
                CarBody.UNIVERSAL, 15500, 11.0, 180, 5));
        taxiStationController.addCar(new Car("Chevrolet", "Lacetti", 
                CarBody.UNIVERSAL, 11000, 12.5, 190, 5));
        taxiStationController.addCar(new Car("Honda", "Civic", 
                CarBody.HATCHBACK, 17000, 9.0, 200, 5));
        taxiStationController.addCar(new Car("Honda", "Civic", 
                CarBody.SEDAN, 16500, 9.0, 200, 5));
    }
    
}
