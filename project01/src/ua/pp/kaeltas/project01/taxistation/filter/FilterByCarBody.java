package ua.pp.kaeltas.project01.taxistation.filter;

import ua.pp.kaeltas.project01.car.Car;
import ua.pp.kaeltas.project01.car.CarBody;

/**
 * Filter by car body. Accept only cars, which car body equals 
 * to given {@code carBody}
 * 
 * @author Matsokha Andrey
 */
public class FilterByCarBody implements Filter {
    
    private final CarBody carBody;

    public FilterByCarBody(CarBody carBody) {
        this.carBody = carBody;
    }

    @Override
    public boolean accept(Car car) {
        if (car.getCarBody() != null && car.getCarBody().equals(carBody)) {
            return true;
        }
        return false;
    }
    
    
    
}
