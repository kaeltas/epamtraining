package ua.pp.kaeltas.project01.taxistation.filter;

import ua.pp.kaeltas.project01.car.Car;

/**
 * Instances of classes that implement this interface are used to filter cars.
 * 
 * @author Matsokha Andrey
 */
public interface Filter {
    /**
     * Checks if current filter is applied to given {@code car}.
     * 
     * @param car Car which will be checked.
     * @return true, if given {@code car} should be included in filter result,
     * false - otherwise.
     */
    boolean accept(Car car);
}
