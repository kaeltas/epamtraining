package ua.pp.kaeltas.project01.taxistation.filter;

import ua.pp.kaeltas.project01.car.Car;

/**
 * Filter by seat count range. Accept only cars, which seat count is greater than 
 * {@code minSeatCount} inclusively and least than {@code maxSeatCount}
 * inclusively.
 *
 * @author Matsokha Andrey
 */
public class FilterBySeatCount implements Filter {
    private final int minSeatCount;
    private final int maxSeatCount;

    public FilterBySeatCount(int minSeatCount, int maxSeatCount) {
        this.minSeatCount = minSeatCount;
        this.maxSeatCount = maxSeatCount;
    }
    
    @Override
    public boolean accept(Car car) {
        if (car.getSeatCount() >= minSeatCount 
                && car.getSeatCount() <= maxSeatCount) {
            return true;
        }
        return false;
    }
}
