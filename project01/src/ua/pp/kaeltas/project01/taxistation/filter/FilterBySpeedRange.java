package ua.pp.kaeltas.project01.taxistation.filter;

import ua.pp.kaeltas.project01.car.Car;

/**
 * Filter by speed range. Accept only cars, which speed is greater than 
 * {@code minSpeed} inclusively and least than {@code maxSpeed} inclusively.
 * 
 * @author Matsokha Andrey
 */
public class FilterBySpeedRange implements Filter {

    private final int minSpeed;
    private final int maxSpeed;

    public FilterBySpeedRange(int minSpeed, int maxSpeed) {
        this.minSpeed = minSpeed;
        this.maxSpeed = maxSpeed;
    }
    
    @Override
    public boolean accept(Car car) {
        if (car.getMaxSpeed() >= minSpeed && car.getMaxSpeed() <= maxSpeed) {
            return true;
        }
        return false;
    }
    
}
