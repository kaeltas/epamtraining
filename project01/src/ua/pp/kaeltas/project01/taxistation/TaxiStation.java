package ua.pp.kaeltas.project01.taxistation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import ua.pp.kaeltas.project01.taxistation.filter.Filter;
import java.util.List;
import ua.pp.kaeltas.project01.car.Car;

/**
 * Class represents a taxi station, which contains a list of cars.
 * It allows to store, filter and sort cars by different rules.
 * 
 * @author Matsokha Andrey
 */
public class TaxiStation {
    private List<Car> cars = new ArrayList<>();

    /**
     * Add {@code car} to taxi station.
     * 
     * @param car 
     */
    public void addCar(Car car) {
        cars.add(car);
    }
    
    /**
     * Return sum of all cars prices.
     * 
     * @return 
     */
    public int getTotalPrice() {
        int totalPrice = 0;
        for (Car car : cars) {
            totalPrice += car.getPrice();
        }
        
        return totalPrice;
    }
    
    /**
     * Filter list of taxi station cars by given {@code filter}.
     * 
     * @param filter
     * @return new filtered list
     */
    public List<Car> getFiltered(Filter filter) {

        List<Car> filteredCars = new LinkedList<>();
        for (Car car : cars) {
            if (filter.accept(car)) {
                filteredCars.add(car);
            }
        }
        
        return filteredCars;
    }

    /**
     * Return list of taxi station cars sorted by petrol usage.
     * 
     * @return new sorted list 
     */
    public List<Car> getSorted() {
        List<Car> sortedCars = new ArrayList<>(cars);
        sortedCars.sort(null); //sort using comparable
                
        return sortedCars;
    }
    
    /**
     * Return list of taxi station cars sorted by given {@code comparator}.
     * 
     * @param comparator
     * @return new sorted list
     */
    public List<Car> getSorted(Comparator<Car> comparator) {
        if (comparator == null) {
            throw new IllegalArgumentException("Comparator can't be null!");
        }
        List<Car> sortedCars = new ArrayList<>(cars);
        sortedCars.sort(comparator); //sort using comparator

        return sortedCars;
    }
    
    /**
     * Return unmodifiable list of all cars.
     * 
     * @return 
     */
    public List<Car> getCars() {
        return Collections.unmodifiableList(cars);
    }
}
