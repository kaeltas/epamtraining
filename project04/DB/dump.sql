-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: hospital_db
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Appointment`
--

DROP TABLE IF EXISTS `Appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `AppointmentType_id` int(11) NOT NULL,
  `Reception_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Appointment_AppointmentType1_idx` (`AppointmentType_id`),
  KEY `fk_Appointment_Reception1_idx` (`Reception_id`),
  CONSTRAINT `fk_Appointment_AppointmentType1` FOREIGN KEY (`AppointmentType_id`) REFERENCES `AppointmentType` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Appointment_Reception1` FOREIGN KEY (`Reception_id`) REFERENCES `Reception` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Appointment`
--

LOCK TABLES `Appointment` WRITE;
/*!40000 ALTER TABLE `Appointment` DISABLE KEYS */;
INSERT INTO `Appointment` VALUES (1,'aspirine',1,2),(2,'asp',1,3),(3,'asp2',1,3),(4,'proc1',2,4),(5,'surg1',3,4),(6,'put leg in plaster',2,5),(7,'morphine 0.2ml',1,5),(9,'пломбировка кариеса',3,11),(10,'чистка зубного камня ультразвуком',2,13),(11,'удаление зуба',3,14);
/*!40000 ALTER TABLE `Appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AppointmentExecutionHistory`
--

DROP TABLE IF EXISTS `AppointmentExecutionHistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AppointmentExecutionHistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Appointment_id` int(11) NOT NULL,
  `MedicalWorker_id` int(11) NOT NULL,
  `execution_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `comment` varchar(145) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_AppointmentExecutionHistory_Appointment1_idx` (`Appointment_id`),
  KEY `fk_AppointmentExecutionHistory_MedicalWorker1_idx` (`MedicalWorker_id`),
  CONSTRAINT `fk_AppointmentExecutionHistory_Appointment1` FOREIGN KEY (`Appointment_id`) REFERENCES `Appointment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_AppointmentExecutionHistory_MedicalWorker1` FOREIGN KEY (`MedicalWorker_id`) REFERENCES `MedicalWorker` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AppointmentExecutionHistory`
--

LOCK TABLES `AppointmentExecutionHistory` WRITE;
/*!40000 ALTER TABLE `AppointmentExecutionHistory` DISABLE KEYS */;
INSERT INTO `AppointmentExecutionHistory` VALUES (1,5,1,'2015-05-13 09:51:22','done successfully'),(2,2,1,'2015-05-13 09:54:44','готово'),(3,5,1,'2015-05-13 09:55:03','ок'),(4,3,3,'2015-05-13 10:38:50','done aspirine');
/*!40000 ALTER TABLE `AppointmentExecutionHistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AppointmentType`
--

DROP TABLE IF EXISTS `AppointmentType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AppointmentType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AppointmentType`
--

LOCK TABLES `AppointmentType` WRITE;
/*!40000 ALTER TABLE `AppointmentType` DISABLE KEYS */;
INSERT INTO `AppointmentType` VALUES (1,'drugs'),(2,'procedure'),(3,'surgery');
/*!40000 ALTER TABLE `AppointmentType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Disease`
--

DROP TABLE IF EXISTS `Disease`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Disease` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `MedicalWorker_id` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closed` timestamp NULL DEFAULT NULL,
  `Patient_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Disease_MedicalWorker1_idx` (`MedicalWorker_id`),
  KEY `fk_Disease_Patient1_idx` (`Patient_id`),
  CONSTRAINT `fk_Disease_MedicalWorker1` FOREIGN KEY (`MedicalWorker_id`) REFERENCES `MedicalWorker` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Disease_Patient1` FOREIGN KEY (`Patient_id`) REFERENCES `Patient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Disease`
--

LOCK TABLES `Disease` WRITE;
/*!40000 ALTER TABLE `Disease` DISABLE KEYS */;
INSERT INTO `Disease` VALUES (3,1,'2015-05-12 06:11:37',NULL,1),(8,1,'2015-05-12 09:59:18','2015-05-11 21:00:00',1),(9,1,'2015-05-12 22:53:59',NULL,1),(15,1,'2015-05-15 09:38:39','2015-05-14 21:00:00',2),(16,1,'2015-05-15 10:32:13',NULL,2);
/*!40000 ALTER TABLE `Disease` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MedicalWorker`
--

DROP TABLE IF EXISTS `MedicalWorker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalWorker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hire_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `User_id` int(11) NOT NULL,
  `office_number` varchar(45) NOT NULL,
  `MedicalWorkerProfession_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_MedicalWorker_User1_idx` (`User_id`),
  KEY `fk_MedicalWorker_MedicalWorkerProfession1_idx` (`MedicalWorkerProfession_id`),
  CONSTRAINT `fk_MedicalWorker_MedicalWorkerProfession1` FOREIGN KEY (`MedicalWorkerProfession_id`) REFERENCES `MedicalWorkerProfession` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_MedicalWorker_User1` FOREIGN KEY (`User_id`) REFERENCES `User` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MedicalWorker`
--

LOCK TABLES `MedicalWorker` WRITE;
/*!40000 ALTER TABLE `MedicalWorker` DISABLE KEYS */;
INSERT INTO `MedicalWorker` VALUES (1,'2014-12-31 22:00:00',1,'10',1),(2,'2014-12-31 22:00:00',2,'15',2),(3,'2014-12-31 22:00:00',3,'26',5);
/*!40000 ALTER TABLE `MedicalWorker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MedicalWorkerProfession`
--

DROP TABLE IF EXISTS `MedicalWorkerProfession`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MedicalWorkerProfession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MedicalWorkerProfession`
--

LOCK TABLES `MedicalWorkerProfession` WRITE;
/*!40000 ALTER TABLE `MedicalWorkerProfession` DISABLE KEYS */;
INSERT INTO `MedicalWorkerProfession` VALUES (1,'Therapist'),(2,'Surgeon'),(3,'Ophthalmologist'),(4,'Neurologist'),(5,'Nurse');
/*!40000 ALTER TABLE `MedicalWorkerProfession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Patient`
--

DROP TABLE IF EXISTS `Patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Patient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Patient`
--

LOCK TABLES `Patient` WRITE;
/*!40000 ALTER TABLE `Patient` DISABLE KEYS */;
INSERT INTO `Patient` VALUES (1,'Joe'),(2,'Paddy'),(3,'Monroe'),(11,'Джон');
/*!40000 ALTER TABLE `Patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reception`
--

DROP TABLE IF EXISTS `Reception`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reception` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `symptoms` varchar(255) NOT NULL,
  `reception_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `diagnosis` varchar(255) NOT NULL,
  `Disease_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ScheduledVisit_Disease1_idx` (`Disease_id`),
  CONSTRAINT `fk_ScheduledVisit_Disease1` FOREIGN KEY (`Disease_id`) REFERENCES `Disease` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reception`
--

LOCK TABLES `Reception` WRITE;
/*!40000 ALTER TABLE `Reception` DISABLE KEYS */;
INSERT INTO `Reception` VALUES (1,'headache','2015-05-12 07:25:56','high pressure',3),(2,'2','2015-05-12 07:35:07','2',3),(3,'3','2015-05-12 09:06:51','3',3),(4,'4','2015-05-12 09:16:49','4',3),(5,'pain in leg','2015-05-12 09:59:18','broken len',8),(6,'<close disease>','2015-05-12 10:21:34','certainly broken leg',8),(7,'test','2015-05-12 22:53:59','test',9),(8,'болит голова','2015-05-14 15:30:33','мигрень',9),(11,'болит зуб','2015-05-14 21:00:00','средний кариес',15),(12,'<closed disease>','2015-05-15 10:31:05','кариес',15),(13,'кровоточат десна','2015-05-15 10:32:13','зубной камень',16),(14,'сломался зуб','2015-05-15 10:50:58','сломан зуб',16);
/*!40000 ALTER TABLE `Reception` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Role`
--

DROP TABLE IF EXISTS `Role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Role`
--

LOCK TABLES `Role` WRITE;
/*!40000 ALTER TABLE `Role` DISABLE KEYS */;
INSERT INTO `Role` VALUES (1,'Admin'),(2,'Doctor'),(3,'Nurse');
/*!40000 ALTER TABLE `Role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password_hash` varchar(128) NOT NULL,
  `Role_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_User_Role1_idx` (`Role_id`),
  CONSTRAINT `fk_User_Role1` FOREIGN KEY (`Role_id`) REFERENCES `Role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'doc','4QrcOUm6Wau+VuBX8g+IPg==',2,'doctor Haus'),(2,'doc2','4QrcOUm6Wau+VuBX8g+IPg==',2,'doctor Who'),(3,'nurse','4QrcOUm6Wau+VuBX8g+IPg==',3,'nurse Jackie'),(4,'admin','4QrcOUm6Wau+VuBX8g+IPg==',1,'admin');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-15 14:04:58
