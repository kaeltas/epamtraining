<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tags/hospital.tld" prefix="h" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.viewPatient" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-10">
	
		<h4><fmt:message key="headline"/></h4>
		<fmt:message key="patient"/>: <c:out value="${patient.firstName}"/> <br>
		
		<h:hasrole roles="Doctor">
			<a class="btn btn-default"
				href="<c:url value="/patient/disease/add?patient=${patient.id}"/>">
				<fmt:message key="buttonNewDisease"/></a>
		</h:hasrole>
		
		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><fmt:message key="thDate"/></th>
					<th><fmt:message key="thLastSymptoms"/></th>
					<th><fmt:message key="thLastDiagnosis"/></th>
					<th><fmt:message key="thAction"/></th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach items="${diseases }" var="disease">
					<tr>
						<td> <c:out value="${disease.created}"/> </td>
						<td> <c:if test="${not empty disease.receptions[0]}"> 
							<c:out value="${disease.receptions[0].symptoms}"/> 
						</c:if></td>
						<td> <c:if test="${not empty disease.receptions[0]}">
							<c:out value="${disease.receptions[0].diagnosis}"/> 
						</c:if></td>
						<td>
						 
						<h:hasrole roles="Doctor">
							<h:isdiseaseauth disease="${disease}">
					<a class="btn btn-default"
				href="<c:url value="/patient/disease/edit?id=${disease.id}"/>">
				<fmt:message key="buttonContinueTreatment"/></a> 
							</h:isdiseaseauth>
						</h:hasrole>
						
						
						<h:hasrole roles="Doctor, Nurse">
				<a class="btn btn-default"
			href="<c:url value="/patient/disease/appointments/view?disease=${disease.id}"/>">
			<fmt:message key="buttonDoAppointment"/></a>
						</h:hasrole>
						</td>
					
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<br>
		
		<h4><fmt:message key="headline2"/></h4>
		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><fmt:message key="thDate"/></th>
					<th><fmt:message key="thLastSymptoms"/></th>
					<th><fmt:message key="thLastDiagnosis"/></th>
					<th><fmt:message key="thAction"/></th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach items="${closedDiseases }" var="disease">
					<tr>
						<td> <c:out value="${disease.created}"/> </td>
						<td> <c:if test="${not empty disease.receptions[0]}"> 
							<c:out value="${disease.receptions[0].symptoms}"/> 
						</c:if></td>
						<td> <c:if test="${not empty disease.receptions[0]}">
							<c:out value="${disease.receptions[0].diagnosis}"/> 
						</c:if></td>
						<td> <%-- <c:out value="${completedVisit.diagnosis}"/> --%> </td>
						<td> <a class="btn btn-default"
			href="<c:url value="/patient/disease/view?id=${disease.id}"/>">
			<fmt:message key="buttonView"/></a> </td>
					
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<a href="<c:url value="/patient/select"/>">
			<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<fmt:message key="back"/></a>
	</div>
	
</div>


<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>