<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tags/hospital.tld" prefix="h" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.addPatient" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

	<div class="row">
		<div class="col-md-offset-1 col-md-5">
			<c:if test="${not empty successRegister}">
				<h4><fmt:message key="${successRegister}"/></h4>
			</c:if>
			<form method="post">
				<label for="nameLabel"><fmt:message key="name"/> </label>
				<div class="form-group">
					<input class="form-control" type="text" name="name" id="nameLabel">
				</div>
				
				<button type="submit" class="btn btn-default"><fmt:message key="buttonRegister"/> </button>
			</form>
			<%-- validation errors --%>
			<div class="bg-danger">
				<h:printlst list="${validatorErrors}" lang="${language}"
					basename="ua.pp.kaeltas.hospital.lang.validatorErrors"/>
			</div>
		</div>
	</div>

<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>