<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tags/hospital.tld" prefix="h" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.addAppointment" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-4">
		
		<h4><fmt:message key="appointments"/></h4>
		<%-- Patient: <c:out value="${currentPatient.firstName}"/> <br> --%>
		
		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><fmt:message key="thType"/></th>
					<th><fmt:message key="thDescription"/></th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach items="${appointments}" var="appointment">
					<tr>
						<td>
						<%-- <c:out value="${appointment.appointmentType.type}"/> --%>
						<fmt:bundle basename="ua.pp.kaeltas.hospital.lang.appointmentTypes">
							<fmt:message key="${appointment.appointmentType.type}"/>
						</fmt:bundle></td>
						<td><c:out value="${appointment.description}"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<form method="post">
			<label for="appointmentTypeLabel"><fmt:message key="appointmentType"/></label>
			<div class="form-group">
				<select class="form-control" name="appointmentType" id="appointmentTypeLabel">
					<c:forEach items="${appointmentTypes}" var="appointmentType">
						<option value="${appointmentType.id}">
							<fmt:bundle basename="ua.pp.kaeltas.hospital.lang.appointmentTypes">
								<fmt:message key="${appointmentType.type}"/>
							</fmt:bundle>
					</c:forEach>
				</select>
			</div>
			
			<label for="appointmentLabel"><fmt:message key="appointment"/></label>
			<div class="form-group">
				<textarea class="form-control" rows="4" 
					name="description" id="appointmentLabel"><c:out value="${currentAppointment.description}"/></textarea>
			</div>
			
			<button type="submit" class="btn btn-default"><fmt:message key="buttonAdd"/></button>
		</form>
			
		<%-- validation errors --%>
		<div class="bg-danger">
			<h:printlst list="${validatorErrors}" lang="${language}"
				basename="ua.pp.kaeltas.hospital.lang.validatorErrors"/>
		</div>
			
		<br>
		
		<a href="<c:url value="/patient/disease/reception/edit?id=${param.reception}"/>">
			<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<fmt:message key="back"/></a>
		
	</div>
</div>


<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>