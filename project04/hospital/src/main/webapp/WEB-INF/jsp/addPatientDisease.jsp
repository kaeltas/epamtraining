<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tags/hospital.tld" prefix="h" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.addPatientDisease" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-5">
		<h4><fmt:message key="headline"/></h4>
		<form method="post">
			<label for="symptomsLabel"><fmt:message key="symptoms"/></label>
			<div class="form-group">
				<textarea class="form-control" rows="3" name="symptoms" 
				id="symptomsLabel"><c:out value="${reception.symptoms}"/></textarea>
			</div>
			
			<label for="diagnosisLabel"><fmt:message key="diagnosis"/></label>
			<div class="form-group">
				<textarea class="form-control" rows="3" name="diagnosis" 
				id="diagnosisLabel"><c:out value="${reception.diagnosis}"/></textarea>
			</div>
			
			<button type="submit" class="btn btn-default"><fmt:message key="buttonSubmit"/></button>
		</form>
		<%-- validation errors --%>
		<div class="bg-danger">
			<h:printlst list="${validatorErrors}" lang="${language}" 
				basename="ua.pp.kaeltas.hospital.lang.validatorErrors"/>
		</div>
		
		<br>
		<a href="<c:url value="/patient/view?id=${param.patient}"/>">
			<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<fmt:message key="back"/></a>
	</div>
</div>


<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>