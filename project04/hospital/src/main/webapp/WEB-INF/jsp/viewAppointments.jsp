<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tags/hospital.tld" prefix="h" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.viewAppointments" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		
		<h4><fmt:message key="headline"/></h4>
		<fmt:message key="patient"/>: <c:out value="${currentPatient.firstName}"/> <br>
		
		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><fmt:message key="thAppointmentDate"/></th>
					<th><fmt:message key="thType"/></th>
					<th><fmt:message key="thDescription"/></th>
					<th><fmt:message key="thExecutionDate"/></th>
					<th><fmt:message key="thResponsible"/></th>
					<th><fmt:message key="thComment"/></th>
					<th><fmt:message key="thAction"/></th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach items="${receptions}" var="reception">
					<c:forEach items="${reception.appointments}" var="appointment">
						<tr>
							<td><c:out value="${reception.receptionDate}"/></td>
							<td>
							<%-- <c:out value="${appointment.appointmentType.type}"/> --%>
							<fmt:bundle basename="ua.pp.kaeltas.hospital.lang.appointmentTypes">
								<fmt:message key="${appointment.appointmentType.type}"/>
							</fmt:bundle></td>
							<td><c:out value="${appointment.description}"/></td>
							<td><c:out value="${appointment.appointmentExecutionHistory.executionDate}"/></td>
							<td><c:out value="${appointment.appointmentExecutionHistory.medicalWorker.user.firstName}"/></td>
							<td><c:out value="${appointment.appointmentExecutionHistory.comment}"/></td>
							
							<td>
								<c:if test="${empty appointment.appointmentExecutionHistory}">
									<h:isappointmentallowed appointment="${appointment}">
							<a class="btn btn-default"
			href="<c:url value="/patient/disease/appointments/do?id=${appointment.id}"/>">
			<fmt:message key="buttonDo"/></a>
									</h:isappointmentallowed>
								</c:if>
							</td>
						</tr>
					</c:forEach>
				</c:forEach>
			</tbody>
		</table>
		
		<br>
		
		<a href="<c:url value="/patient/view?id=${currentPatient.id}"/>">
			<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<fmt:message key="back"/></a>
		
	</div>
</div>




<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>