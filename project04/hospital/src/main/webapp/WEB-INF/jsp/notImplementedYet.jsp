<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.notImplementedYet" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<h4><fmt:message key="headline"/></h4>
<fmt:message key="message"/>

<a href="<c:url value="/"/>"> <fmt:message key="main"/> </a>

<%-- <hr>
Debug parameters <br>
a1 = ${a1}<br>
a2 = ${a2}<br>
a3 = ${a3}<br>
a4 = ${a4}<br>
a5 = ${a5}<br>
 --%>

<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>

