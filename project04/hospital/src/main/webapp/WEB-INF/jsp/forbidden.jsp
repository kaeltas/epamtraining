<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.forbidden" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-4">
		
		<h4><fmt:message key="headline"/></h4>
		<fmt:message key="forbiddenMessage"/>
		<br>
		
		<a href="<c:url value="/"/>"><fmt:message key="main"/></a>
		
	</div>
</div>




<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>