<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tags/hospital.tld" prefix="h" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.doAppointment" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-4">
		
		<h4><fmt:message key="headline"/></h4>
		<%-- Patient: <c:out value="${currentPatient.firstName}"/> <br> --%>
		
		<fmt:message key="type"/>: <%-- <c:out value="${appointment.appointmentType.type}"/> --%> 
			<fmt:bundle basename="ua.pp.kaeltas.hospital.lang.appointmentTypes">
				<fmt:message key="${appointment.appointmentType.type}"/>
			</fmt:bundle><br>
		<fmt:message key="description"/>: <c:out value="${appointment.description}"/> <br>
		
		
		<form method="post">
			<label for="commentLabel"><fmt:message key="comment"/></label>
			<div class="form-group">
				<textarea class="form-control" rows="4" 
				name="comment" id="commentLabel"><c:out value="${appExecHist.comment}"/></textarea>
			</div>
			
			<button type="submit" class="btn btn-default"><fmt:message key="buttonSubmit"/></button>
		</form>
			
		<%-- validation errors --%>
		<div class="bg-danger">
			<h:printlst list="${validatorErrors}" lang="${language}" 
				basename="ua.pp.kaeltas.hospital.lang.validatorErrors"/>
		</div>
			
		<br>
		
		<a href="<c:url value="/patient/disease/appointments/view?disease=
				${appointment.reception.disease.id}"/>">
			<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<fmt:message key="back"/></a>
		
	</div>
</div>




<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>