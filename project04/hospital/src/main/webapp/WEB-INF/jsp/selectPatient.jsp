<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.selectPatient" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-3">
		<h4><fmt:message key="headline"/> </h4>
		
		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><fmt:message key="thName"/></th>
					<th><fmt:message key="thAction"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${patients}" 
					var="patient">
					<tr>
						<td> <c:out value="${patient.firstName}"/> </td>
						<td> <a class="btn btn-default" href="
							<c:url value="/patient/view?id=${patient.id}"/>"
							><fmt:message key="buttonSelect"/></a> </td>
					</tr>
				</c:forEach>
			
			</tbody>
		
		</table>

	</div>
</div>



<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>
