<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

<!-- Custom styles for navbar-static -->
<link href="<c:url value="/include/css/navbar-static-top.css"/>" rel="stylesheet">

<!-- Datepicker -->
<script type="text/javascript" src="<c:url value="/include/datepicker/js/bootstrap-datepicker.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/include/datepicker/css/datepicker.css"/>">

<title>${title}</title>
</head>
<body>

<jsp:include page="/WEB-INF/jsp/_topmenu.jsp"></jsp:include>

<div class="container">
