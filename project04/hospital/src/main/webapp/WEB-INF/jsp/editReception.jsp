<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tags/hospital.tld" prefix="h" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.editReception" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-5">
		<h4><fmt:message key="headline"/></h4>
		<form method="post">
			<label for="symptomsLabel"><fmt:message key="symptoms"/></label>
			<div class="form-group">
				<textarea class="form-control" rows="3" name="symptoms" 
				id="symptomsLabel"><c:out value="${reception.symptoms}"/></textarea>
			</div>
			
			<label for="diagnosisLabel"><fmt:message key="diagnosis"/></label>
			<div class="form-group">
				<textarea class="form-control" rows="3" name="diagnosis" 
				id="diagnosisLabel"><c:out value="${reception.diagnosis}"/></textarea>
			</div>
			
			<button type="submit" class="btn btn-default"><fmt:message key="buttonSave"/></button>
		</form>
		<%-- validation errors --%>
		<div class="bg-danger">
			<h:printlst list="${validatorErrors}" lang="${language}" 
				basename="ua.pp.kaeltas.hospital.lang.validatorErrors"/>
		</div>
		<br>
		<h4><fmt:message key="headline2"/></h4>
		<a class="btn btn-default"
			href="<c:url value="/patient/disease/reception/appointment/add?reception=${reception.id}"/>">
			<fmt:message key="buttonAddAppointment"/></a> <br>

		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><fmt:message key="thType"/></th>
					<th><fmt:message key="thDescription"/></th>
				</tr>
			</thead>
			
			<tbody>
				<c:forEach items="${reception.appointments}" var="appointment">
					<tr>
						<td>
						<%-- <c:out value="${appointment.appointmentType.type}"/> --%>
						<fmt:bundle basename="ua.pp.kaeltas.hospital.lang.appointmentTypes">
							<fmt:message key="${appointment.appointmentType.type}"/>
						</fmt:bundle></td>
						<td><c:out value="${appointment.description}"/></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

		<br>
		<a href="<c:url value="/patient/disease/edit?id=${reception.disease.id}"/>">
			<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<fmt:message key="back"/></a>
			
	</div>
</div>




<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>