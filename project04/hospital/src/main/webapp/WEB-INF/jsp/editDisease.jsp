<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.editDisease" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<div class="row">
	<div class="col-md-offset-1 col-md-10">
		<h4><fmt:message key="headline"/></h4>
		<fmt:message key="patient"/>: <c:out value="${disease.patient.firstName}"/> <br>
		
		<a class="btn btn-default"
			href="<c:url value="/patient/disease/reception/add?disease=${disease.id}"/>">
			<fmt:message key="buttonNewReception"/></a>
		<a class="btn btn-default"
			href="<c:url value="/patient/disease/close?disease=${disease.id}"/>">
			<fmt:message key="buttonCloseDisease"/></a> <br>

		<table class="table table-striped table-condensed">
			<thead>
				<tr>
					<th><fmt:message key="thDate"/></th>
					<th><fmt:message key="thSymptoms"/></th>
					<th><fmt:message key="thDiagnosis"/></th>
					<th><fmt:message key="thAppointments"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${disease.receptions}" var="reception">
					<tr> 
						<td><c:out value="${reception.receptionDate}"/></td>
						<td><c:out value="${reception.symptoms}"/></td>
						<td><c:out value="${reception.diagnosis}"/></td>
						<td>
							<c:forEach items="${reception.appointments}" 
							var="appointment" varStatus="status">
								<c:out value="${status.count}"/>) 
								<fmt:bundle basename="ua.pp.kaeltas.hospital.lang.appointmentTypes">
									<fmt:message key="${appointment.appointmentType.type}"/>
								</fmt:bundle>
								<%-- <c:out value="${appointment.appointmentType.type}"/> - --%> 
								<c:out value="${appointment.description}"/> <br>
							</c:forEach>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<a href="<c:url value="/patient/view?id=${disease.patient.id}"/>">
			<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<fmt:message key="back"/></a>
	</div>
</div>




<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>