<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

	</div>

	<footer class="footer">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-offset-5 col-md-2">
        			<span class="text-muted">&copy; Matsokha Andrey</span>
        		</div>
        	</div>
    	</div>
    </footer>
</body>
</html>