<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tags/hospital.tld" prefix="h" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.login" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

	<div class="row">
		<div class="col-md-offset-2 col-md-4">
			<form method="post">
				<div class="form-group">
					<label for="login"><fmt:message key="login"/></label>
				 	<input class="form-control" type="text" name="username" id="login"/>
				 </div>
				 <div class="form-group">
					<label for="password"><fmt:message key="password"/></label>
					<input class="form-control" type="password" name="password" id="password"/>
				</div>
				<button type="submit" class="btn btn-default"><fmt:message key="signIn"/></button>
			</form>
			
			<div class="bg-danger">
				<%-- validation errors --%>
				<h:printlst list="${validatorErrors}" lang="${language}" 
					basename="ua.pp.kaeltas.hospital.lang.validatorErrors"/>
				<c:if test="${not empty loginError}">
					<fmt:message key="${loginError}"/>
				</c:if>
			</div>
		</div>
	</div>
	
	<hr>
	
	<div class="row">
		<div class="col-md-offset-2 col-md-4">
			<b>Hint</b> <br>
			Available user logins <br>
			<c:forEach items="${allusers}" var="user" varStatus="status">
				${status.count}) ${user.email} <br>
			</c:forEach>
			<br>
			Password for all users: 123456 <br>
		</div>
	</div>

<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>