<%@page import="org.jboss.security.auth.spi.Util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="ua.pp.kaeltas.hospital.lang.selectPatient" />

<c:set var="title" scope="request"><fmt:message key="title"/></c:set>
<jsp:include page="/WEB-INF/jsp/_header.jsp"/>

<%-- <%=
Util.createPasswordHash("MD5", Util.BASE64_ENCODING, null, null, "123456")
%> --%>

Welcome to Hospital!

<jsp:include page="/WEB-INF/jsp/_footer.jsp"/>