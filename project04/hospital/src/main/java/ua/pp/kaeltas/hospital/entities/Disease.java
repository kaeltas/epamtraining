package ua.pp.kaeltas.hospital.entities;

import java.sql.Date;
import java.util.List;

/**
 * Disease entity.
 * It holds information about one row from DB table Disease.
 *  
 * @author Matsokha Andrey
 */
public class Disease {
	
	private int id;
	
	private Date created;
	
	private Patient patient;
	
	private MedicalWorker medicalWorker;
	
	private Date closed;
	
	private List<Reception> receptions;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public MedicalWorker getMedicalWorker() {
		return medicalWorker;
	}

	public void setMedicalWorker(MedicalWorker medicalWorker) {
		this.medicalWorker = medicalWorker;
	}

	public Date getClosed() {
		return closed;
	}

	public void setClosed(Date closed) {
		this.closed = closed;
	}

	public List<Reception> getReceptions() {
		return receptions;
	}

	public void setReceptions(List<Reception> receptions) {
		this.receptions = receptions;
	}
	
	
	
}
