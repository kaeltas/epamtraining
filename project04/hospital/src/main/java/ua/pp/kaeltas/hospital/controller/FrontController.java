package ua.pp.kaeltas.hospital.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.controller.command.Command;
import ua.pp.kaeltas.hospital.controller.command.CommandFactory;

/**
 * Class represents Front controller pattern.
 * It is a main servlet, which receives all requests. 
 * Depending on request URI - it chooses and executes some action.
 * Then it forwards or redirects request to a view page.    
 * @author Matsokha Andrey
 */
public class FrontController extends HttpServlet {
	
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String ERROR_MSG_SYSTEM = "systemError";
	private static final String ERROR_ATTRIBUTE = "error";

	private static final Logger LOGGER = Logger.getLogger(FrontController.class);
	
	private static final long serialVersionUID = 1L;

	/**
	 * Process HTTP GET request 
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req, resp);
	}

	/**
	 * Process HTTP POST request 
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		process(req, resp);
	}
	
	/**
	 * Single method for processing both, HTTP GET and POST requests.
	 * Depending on request URI it chooses specific command, executes it and
	 * then passes processing to view. 
	 */
	private void process(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		CommandFactory commandFactory = new CommandFactory();
		
		Command command = commandFactory.createCommand(req);
		
		String viewPage = null;
		try {
			viewPage = command.execute(req, resp);
		} catch (Exception e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_SYSTEM);
			LOGGER.error(e);
			viewPage = VIEW_ERROR;
		}
		
		if (viewPage.startsWith("redirect:")) {
			String redirectPage = req.getContextPath() + viewPage.substring(viewPage.indexOf("/"));
			resp.sendRedirect(redirectPage);
		} else {
			dispatch(req, resp, viewPage);
		}
	}
	
	/**
	 * Forward request and response to view page.
	 * 
	 * @param req HttpServletRequest to forward
	 * @param resp HttpServletResponse to forward
	 * @param viewPage {@code String} representation of JSP or other page to forward
	 * @throws ServletException
	 * @throws IOException
	 */
	private void dispatch(HttpServletRequest req, HttpServletResponse resp, 
			String viewPage) throws ServletException, IOException {
		RequestDispatcher dispatcher = req.getRequestDispatcher(viewPage);
		dispatcher.forward(req, resp);
	}
	
	
}
