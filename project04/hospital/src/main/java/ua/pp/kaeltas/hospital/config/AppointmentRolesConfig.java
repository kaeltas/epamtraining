package ua.pp.kaeltas.hospital.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Class loads appointment execution roles from properties-file 
 * and provides access to them.
 *  
 * @author Matsokha Andrey
 */
public class AppointmentRolesConfig {

	// Private class for loading on demand singleton instance
	private static class AppointmentRolesConfigHolder {
		public static final AppointmentRolesConfig instance = new AppointmentRolesConfig();
	}
	
	private static final String APPOINTMENT_ROLES_PROPERTIES = 
			"ua/pp/kaeltas/hospital/config/appointmentAllowedRoles.properties";

	private static final Logger LOGGER = Logger.getLogger(AppointmentRolesConfig.class);
	
	private Properties appointmentRoles;

	/**
	 * Constructor, initializes Properties instance and loads properties file.
	 */
	private AppointmentRolesConfig() {
		appointmentRoles = new Properties();
		
		InputStream stream = getClass().getClassLoader()
				.getResourceAsStream(APPOINTMENT_ROLES_PROPERTIES);
		
		try {
			appointmentRoles.load(stream);
		} catch (IOException e) {
			LOGGER.error("Can't load Appointment Roles Config", e);
			throw new RuntimeException(e);
		}
		
	}
	
	/**
	 * Return instance of singleton config class
	 * @return
	 */
	public static AppointmentRolesConfig getInstance() {
		return AppointmentRolesConfigHolder.instance;
	}
	
	/**
	 * Return array {@code String} roles, who could execute given 
	 * {@code String} appointment type.
	 * @param appointmentType
	 * @return
	 */
	public String[] getRoles(String appointmentType) {
		String rolesForAppointment = appointmentRoles.getProperty(appointmentType);

		//if appointmentType found - return list of roles
		if (rolesForAppointment != null) {
			return rolesForAppointment.split(",");
		}
		
		return new String[0];
	}
	
}
