package ua.pp.kaeltas.hospital.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation.
 * 
 * Put variable {@code language} into session scope, which holds
 * current UI language.
 * 
 * @author Matsokha Andrey
 */
public class InternationalizationFilter implements Filter {

	private static final String LANGUAGE_PARAM = "language";

	/**
	 * Put variable {@code language} into session scope if it exists, 
	 * or in request scope otherwise.
	 * Variable {@code language} holds current UI language.
	 * 
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest)request;
		HttpSession session = req.getSession(false);
		if (session != null) {
			String language = req.getParameter(LANGUAGE_PARAM);
			if (language != null) {
				session.setAttribute(LANGUAGE_PARAM, language);
			} else if (session.getAttribute(LANGUAGE_PARAM) == null) {
				session.setAttribute(LANGUAGE_PARAM, req.getLocale().getLanguage());
			}
		} else {
			req.setAttribute(LANGUAGE_PARAM, req.getLocale());
		}

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// Do nothing
	}
	
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// Do nothing
	}



}
