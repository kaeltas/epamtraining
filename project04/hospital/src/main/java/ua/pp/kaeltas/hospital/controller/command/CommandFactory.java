package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ua.pp.kaeltas.hospital.controller.RequestHelper;

/**
 * Command factory that creates specific commands, depending on request URI
 * and authentication.
 *  
 * @author Matsokha Andrey
 */
public class CommandFactory {
	
	private RequestHelper requestHelper = new RequestHelper();
	
	/**
	 * Checks if user is authenticated, using HTTP session.
	 * Saves to request scope request URI and Query Parameters. 
	 * @param req
	 * @return true, if user is already authenticated;
	 * 			false - otherwise.
	 */
	private boolean checkAuthentication(HttpServletRequest req) {
		HttpSession session = req.getSession(false);
		if (session != null) {
			if (session.getAttribute("user") != null) {
				return true;
			}
		}
		
		req.setAttribute("redirectPath", requestHelper
				.getRelativeRequestURI(req));
		req.setAttribute("redirectPathQueryString", req.getQueryString());
		
		return false;
		
	}
	
	/**
	 * Creates command depending on request URI.
	 * If user is not authenticated - create login command.  
	 * 
	 * @param req
	 * @return
	 */
	public Command createCommand(HttpServletRequest req) {

		if (!checkAuthentication(req)) {
			return new CommandLogin();
		}
		
		
		String path = requestHelper.getRelativeRequestURI(req);
		
		switch (path) {
			case "/" :
			case "/index.jsp" :
			case "/patient/select" : {
				return new CommandSelectPatient();
			}
			
			case "/patient/add" : {
				return new CommandAddPatient();
			}
			case "/patient/view" : {
				return new CommandViewPatient();
			}
			
			case "/patient/disease/add" : {
				return new CommandAddDisease();
			}
			case "/patient/disease/view" : {
				return new CommandViewDisease();
			}
			case "/patient/disease/edit" : {
				return new CommandEditDisease();
			}
			case "/patient/disease/close" : {
				return new CommandCloseDisease();
			}
			
			case "/patient/disease/reception/add" : {
				return new CommandAddReception();
			}
			case "/patient/disease/reception/edit" : {
				return new CommandEditReception();
			}
			case "/patient/disease/reception/appointment/add" : {
				return new CommandAddAppointment();
			}
			
			case "/patient/disease/appointments/view" : {
				return new CommandViewAppointments();
			}
			case "/patient/disease/appointments/do" : {
				return new CommandDoAppointment();
			}
			
			case "/logout" : {
				return new CommandLogout();
			}
			
//			case "/index.jsp" : 
//			case "/" : {
//				
//				return new CommandIndex();
//			}
		}
		
		return new CommandNotImplementedYet();
	}

}
