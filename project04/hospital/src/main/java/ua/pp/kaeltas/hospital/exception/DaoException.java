package ua.pp.kaeltas.hospital.exception;

/**
 * Base exception for all Dao-layer exceptions. 
 * @author Matsokha Andrey
 */
public class DaoException extends Exception {

	/**
	 * Default SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public DaoException(String message) {
		super(message);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}
	
}
