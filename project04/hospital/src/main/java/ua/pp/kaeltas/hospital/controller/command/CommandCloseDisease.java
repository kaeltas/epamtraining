package ua.pp.kaeltas.hospital.controller.command;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.DiseaseService;
import ua.pp.kaeltas.hospital.service.ReceptionService;
import ua.pp.kaeltas.hospital.validator.RequestValidator;
import ua.pp.kaeltas.hospital.validator.ValidatorFactory;

/**
 * Command represents close disease functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandCloseDisease implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/closeDisease.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String VIEW_FORBIDDEN = "/WEB-INF/jsp/forbidden.jsp";
	private static final String VIEW_REDIRECT_ADDED = "redirect:/patient/view?id=";
	
	//string - used to fill symptoms field, when disease is closed
	private static final String CLOSED_DISEASE_STUB_SYMPTOMS = "<closed disease>";
	
	//used attributes
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorDiseaseNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String FINAL_RECEPTION_ATTRIBUTE = "finalReception";
	
	//used request parameters
	private static final String DIAGNOSIS_PARAM = "diagnosis";
	private static final String DISEASE_ID_PARAM = "disease";

	private static final Logger LOGGER = Logger.getLogger(CommandCloseDisease.class);
	
	private ReceptionService receptionService = new ReceptionService();
	
	private DiseaseService diseaseService = new DiseaseService();
	
	/**
	 * Show and process http form, validate parameters and close disease. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		if ("post".equals(req.getMethod().toLowerCase())) {
			try {
				int diseaseId = Integer.parseInt(req.getParameter(DISEASE_ID_PARAM));
				
				if (diseaseService.isDiseaseOpened(diseaseId)) {
				
					String diagnosis = req.getParameter(DIAGNOSIS_PARAM);
					
					Disease d = diseaseService.getDisease(diseaseId);
					d.setClosed(new Date(System.currentTimeMillis()));
					
					Reception r = new Reception();
					r.setDiagnosis(diagnosis);
					r.setDisease(d);
					r.setSymptoms(CLOSED_DISEASE_STUB_SYMPTOMS);
					
					req.setAttribute(FINAL_RECEPTION_ATTRIBUTE, r);
					
					RequestValidator requestValidator = new RequestValidator(req);
					requestValidator.validate(diagnosis, 
							ValidatorFactory.getInstance().createDiagnosisValidator());
					
					if (!requestValidator.hasErrors()) {
						receptionService.addReception(r);
						diseaseService.updateDisease(d);
						
						return VIEW_REDIRECT_ADDED + d.getPatient().getId();
					}
				} else {
					return VIEW_FORBIDDEN;
				}
			} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
				req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
				LOGGER.error(e);
				return VIEW_ERROR;
			} catch (DaoException e) {
				req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
				LOGGER.error(e);
				return VIEW_ERROR;
			}
		}
		
		return VIEW_SUCCESS;
		
	}

}
