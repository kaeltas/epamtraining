package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.ReceptionDao;
import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcReceptionDao implements ReceptionDao {

	private static final String UPDATE = 
			"UPDATE Reception r "
			+ "SET r.symptoms = ?, "
			+ "r.diagnosis = ?, "
			+ "r.reception_date = ? "
			+ "WHERE r.id = ?";
	private static final String SELECT_ALL_BY_DISEASE_ID_ORDER_BY_RECEPTION_DESC = 
			"SELECT * "
			+ "FROM Reception r "
			+ "WHERE r.Disease_id = ? "
			+ "ORDER BY r.reception_date DESC";
	private static final String SELECT_BY_ID = 
			"SELECT * "
			+ "FROM Reception r "
			+ "WHERE r.id = ? ";
	private static final String INSERT = 
			"INSERT INTO Reception "
			+ "(symptoms, diagnosis, "
			+ "reception_date, Disease_id) "
			+ "VALUES (?, ?, ?, ?)";

	@Override
	public int createAndGetId(Reception reception) throws DaoException {

		try (Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(
					INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, reception.getSymptoms());
			ps.setString(2, reception.getDiagnosis());
			ps.setDate(3, reception.getReceptionDate());
			ps.setInt(4, reception.getDisease().getId());
			
			ps.execute();
			int generatedKey =-1;
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.first()) {
				generatedKey = rs.getInt(1);
			}
			
			rs.close();
			ps.close();
			
			return generatedKey;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}

		//return -1;
	}

	@Override
	public Reception find(int id) throws DaoException {
		
		try(Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(SELECT_BY_ID);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			Reception reception = null;
			if (rs.first()) {
				reception = new Reception();
				reception.setId(rs.getInt("r.id"));
				reception.setDiagnosis(rs.getString("r.diagnosis"));
				reception.setReceptionDate(rs.getDate("r.reception_date"));
				reception.setSymptoms(rs.getString("r.symptoms"));
				
				Disease disease = new Disease();
				disease.setId(rs.getInt("r.Disease_id"));
				reception.setDisease(disease);
				
			}
			
			rs.close();
			ps.close();
			
			if (reception == null) {
				throw new DaoExceptionNoSuchEntity("Reception NOT found by id=" + id);
			}
			
			return reception;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public List<Reception> findAll() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Reception reception) throws DaoException {
		
		try (Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(UPDATE);
			
			ps.setString(1, reception.getSymptoms());
			ps.setString(2, reception.getDiagnosis());
			ps.setDate(3, new Date(System.currentTimeMillis()));
			ps.setInt(4, reception.getId());
			
			ps.execute();

			ps.close();
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}

	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Reception> findAllByDisease(Disease disease) throws DaoException {
		
		try(Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(
					SELECT_ALL_BY_DISEASE_ID_ORDER_BY_RECEPTION_DESC);
			ps.setInt(1, disease.getId());
			
			ResultSet rs = ps.executeQuery();
			List<Reception> receptions = new LinkedList<>();
			while(rs.next()) {
				Reception r = new Reception();
				r.setId(rs.getInt("r.id"));
				r.setSymptoms(rs.getString("r.symptoms"));
				r.setDiagnosis(rs.getString("r.diagnosis"));
				r.setReceptionDate(rs.getDate("r.reception_date"));

				r.setDisease(disease);
				
				receptions.add(r);
			}
			
			rs.close();
			ps.close();
			
			return receptions;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public void create(Reception t) {
		throw new UnsupportedOperationException();
	}

	

}
