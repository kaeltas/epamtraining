package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command represents not implemented yet functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandNotImplementedYet implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/notImplementedYet.jsp";

	/**
	 * Show not implemented yet message. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		return VIEW_SUCCESS;
	}

}
