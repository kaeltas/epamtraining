package ua.pp.kaeltas.hospital.entities;

/**
 * AppointmentType entity.
 * It holds information about one row from DB table AppointmentType.
 *  
 * @author Matsokha Andrey
 */
public class AppointmentType {
	private int id;
	
	private String type;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
