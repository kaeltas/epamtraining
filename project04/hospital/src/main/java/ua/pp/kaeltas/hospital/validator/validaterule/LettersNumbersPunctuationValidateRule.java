package ua.pp.kaeltas.hospital.validator.validaterule;

/**
 * Validation rule for checking if string value contains En/Ru letters, 
 * digits, punctuation symbols and some common used symbols.
 *  
 * @author Matsokha Andrey
 */
public class LettersNumbersPunctuationValidateRule implements ValidateRule {

	/**
	 * Return true if value contains ONLY En/Ru letters, digits, 
	 * punctuation symbols and some common used symbols;
	 * false - otherwise.
	 */
	@Override
	public boolean valid(String value) {
		
		if (value == null) {
			return false;
		}
		
		if (!value.matches("[a-zA-Zа-яА-Я0-9,.!? -=)(#$%;\"\'№]*")) {
			return false;
		}
		
		return true;
	}

	@Override
	public String getErrorMessage() {
		return "validatorErrorLettersNumbersPunctuation";
	}

}
