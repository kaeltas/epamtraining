package ua.pp.kaeltas.hospital.dao.interfaces;

import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.AppointmentExecutionHistory;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * DAO for accessing AppointmentExecutionHistory entity.
 *  
 * @author Matsokha Andrey
 */
public interface AppointmentExecutionHistoryDao extends BaseDao<AppointmentExecutionHistory> {
	
	/**
	 * Find and return AppointmentExecutionHistory entity related to given appointment.
	 * @param appointment
	 * @return {@code AppointmentExecutionHistory}
	 * @throws DaoException 
	 */
	AppointmentExecutionHistory findByAppointment(Appointment appointment) throws DaoException;
	
}
