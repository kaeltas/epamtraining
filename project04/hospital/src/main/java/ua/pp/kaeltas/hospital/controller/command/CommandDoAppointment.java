package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.AppointmentExecutionHistory;
import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.AppointmentService;
import ua.pp.kaeltas.hospital.service.MedicalWorkerService;
import ua.pp.kaeltas.hospital.validator.RequestValidator;
import ua.pp.kaeltas.hospital.validator.ValidatorFactory;

/**
 * Command represents do appointment functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandDoAppointment implements Command {

	//returned view constants 
	private static final String VIEW_FORBIDDEN = "/WEB-INF/jsp/forbidden.jsp";
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/doAppointment.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String VIEW_REDIRECT_ADDED = "redirect:/patient/disease/appointments/view?disease=";
	
	//used attributes
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorAppointmentNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String APP_EXEC_HIST_ATTRIBUTE = "appExecHist";
	private static final String APPOINTMENT_ATTRIBUTE = "appointment";
	private static final String USER_ATTRIBUTE = "user";
	
	//used request parameters
	private static final String COMMENT_PARAM = "comment";
	private static final String APPOINTMENT_ID_PARAM = "id";

	private static final Logger LOGGER = Logger.getLogger(CommandDoAppointment.class);
	
	private AppointmentService appointmentService = new AppointmentService();
	
	private MedicalWorkerService medicalWorkerService = new MedicalWorkerService();
	
	/**
	 * Show and process http form, validate parameters, check if 
	 * appointment execution is allowed to current user and execute appointment. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			int appointmentId = Integer.parseInt(req.getParameter(APPOINTMENT_ID_PARAM));
			
			Appointment appointment = appointmentService.getAppointment(appointmentId);
			
			HttpSession session = req.getSession();
			User user = (User)session.getAttribute(USER_ATTRIBUTE);
			
			if (appointmentService.isAppointmentAllowedToUser(appointment, user)
					&& appointmentService.isAppointmentOpened(appointment)) {
				
				req.setAttribute(APPOINTMENT_ATTRIBUTE, appointment);
				
				if ("post".equals(req.getMethod().toLowerCase())) {
					String comment = req.getParameter(COMMENT_PARAM);
					
					AppointmentExecutionHistory appExecHist = 
							new AppointmentExecutionHistory();
					appExecHist.setAppointment(appointment);
					appExecHist.setComment(comment);
					
					req.setAttribute(APP_EXEC_HIST_ATTRIBUTE, appExecHist);
					
					RequestValidator requestValidator = new RequestValidator(req);
					requestValidator.validate(comment, 
							ValidatorFactory.getInstance().createCommentValidator());
					
					if (!requestValidator.hasErrors()) {
					
						String email =  user.getEmail();
						
						MedicalWorker medicalWorker = medicalWorkerService
								.getMedicalWorkerByEmail(email);
						
						appExecHist.setMedicalWorker(medicalWorker);
						
						appointmentService.doAppointment(appExecHist);
						
						return VIEW_REDIRECT_ADDED
							+ appointment.getReception().getDisease().getId();
					}
				}
			} else {
				return VIEW_FORBIDDEN;
			}
		
		} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
			LOGGER.error(e);
			return VIEW_ERROR;
		} catch (DaoException e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
			LOGGER.error(e);
			return VIEW_ERROR;
		}
		
		return VIEW_SUCCESS;
	}

}
