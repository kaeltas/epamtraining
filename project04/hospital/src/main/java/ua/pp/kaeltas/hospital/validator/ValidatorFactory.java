package ua.pp.kaeltas.hospital.validator;

import ua.pp.kaeltas.hospital.validator.validaterule.EmptyValidateRule;
import ua.pp.kaeltas.hospital.validator.validaterule.LettersEnRuNumbersValidateRule;
import ua.pp.kaeltas.hospital.validator.validaterule.LettersNumbersPunctuationValidateRule;
import ua.pp.kaeltas.hospital.validator.validaterule.SizeValidateRule;

/**
 * Factory which creates validators.
 *  
 * @author Matsokha Andrey
 */
public class ValidatorFactory {
	private static final String ERRORS_PREFIX_COMMENT = "comment";
	private static final String ERRORS_PREFIX_DESCRIPTION = "description";
	private static final String ERRORS_PREFIX_DIAGNOSIS = "diagnosis";
	private static final String ERRORS_PREFIX_SYMPTOMS = "symptoms";
	private static final String ERRORS_PREFIX_NAME = "name";
	private static final String ERRORS_PREFIX_PASSWORD = "password";
	private static final String ERRORS_PREFIX_LOGIN = "login";
	
	private static ValidatorFactory instance = new ValidatorFactory();
	
	public static ValidatorFactory getInstance() {
		return instance;
	}
	
	/**
	 * Return validator for login.
	 * @return
	 */
	public Validator createLoginValidator() {
		return  new Validator(ERRORS_PREFIX_LOGIN)
						.add(new EmptyValidateRule());
	}
	
	/**
	 * Return validator for password.
	 * @return
	 */
	public Validator createPasswordValidator() {
		return  new Validator(ERRORS_PREFIX_PASSWORD)
						.add(new EmptyValidateRule());
	}

	/**
	 * Return validator for patients name.
	 * @return
	 */
	public Validator createNameValidator() {
		return  new Validator(ERRORS_PREFIX_NAME)
						.add(new EmptyValidateRule())
						.add(new SizeValidateRule(3, 20))
						.add(new LettersEnRuNumbersValidateRule());

	}

	/**
	 * Return validator for symptoms.
	 * @return
	 */
	public Validator createSymptomsValidator() {
		return  new Validator(ERRORS_PREFIX_SYMPTOMS)
						.add(new EmptyValidateRule())
						.add(new SizeValidateRule(5, 200))
						.add(new LettersNumbersPunctuationValidateRule());
	}

	/**
	 * Return validator for diagnosis.
	 * @return
	 */
	public Validator createDiagnosisValidator() {
		return  new Validator(ERRORS_PREFIX_DIAGNOSIS)
						.add(new EmptyValidateRule())
						.add(new SizeValidateRule(5, 200))
						.add(new LettersNumbersPunctuationValidateRule());
	}

	/**
	 * Return validator for description of appointment.
	 * @return
	 */
	public Validator createDescriptionValidator() {
		return  new Validator(ERRORS_PREFIX_DESCRIPTION)
						.add(new EmptyValidateRule())
						.add(new SizeValidateRule(5, 200))
						.add(new LettersNumbersPunctuationValidateRule());
	}

	/**
	 * Return validator for comment of execution appointment record.
	 * @return
	 */
	public Validator createCommentValidator() {
		return  new Validator(ERRORS_PREFIX_COMMENT)
						.add(new EmptyValidateRule())
						.add(new SizeValidateRule(5, 200))
						.add(new LettersNumbersPunctuationValidateRule());
	}
	
}
