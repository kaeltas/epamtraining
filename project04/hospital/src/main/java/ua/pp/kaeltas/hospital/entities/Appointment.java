package ua.pp.kaeltas.hospital.entities;

/**
 * Appointment entity.
 * It holds information about one row from DB table Appointment.
 *  
 * @author Matsokha Andrey
 */
public class Appointment {

	private int id;

	private String description;
	private AppointmentType appointmentType;
	private Reception reception;
	private AppointmentExecutionHistory appointmentExecutionHistory;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AppointmentType getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(AppointmentType appointmentType) {
		this.appointmentType = appointmentType;
	}


	public Reception getReception() {
		return reception;
	}

	public void setReception(Reception reception) {
		this.reception = reception;
	}
	
	public AppointmentExecutionHistory getAppointmentExecutionHistory() {
		return appointmentExecutionHistory;
	}

	public void setAppointmentExecutionHistory(
			AppointmentExecutionHistory appointmentExecutionHistory) {
		this.appointmentExecutionHistory = appointmentExecutionHistory;
	}

	@Override
	public String toString() {
		return "Appointment [id=" + id + ", description=" + description
				+ ", appointmentType=" + appointmentType + ", scheduledVisit="
				+ reception + "]";
	}
	

}
