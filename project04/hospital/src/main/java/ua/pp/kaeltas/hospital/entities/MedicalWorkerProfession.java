package ua.pp.kaeltas.hospital.entities;

/**
 * MedicalWorkerProfession entity.
 * It holds information about one row from DB table MedicalWorkerProfession.
 *  
 * @author Matsokha Andrey
 */
public class MedicalWorkerProfession {
	private int id;
	private String name;
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
}
