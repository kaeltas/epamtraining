package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.DiseaseService;

/**
 * Command represents view disease functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandViewDisease implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/viewDisease.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	
	//used attributes
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorDiseaseNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String DISEASE_ATTRIBUTE = "disease";
	
	//used request parameters
	private static final String ID_PARAM = "id";
	
	private static final Logger LOGGER = Logger.getLogger(CommandViewDisease.class);
	
	private DiseaseService diseaseService = new DiseaseService();

	/**
	 * Show disease page by id. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {

		try {
			int diseaseId = Integer.parseInt(req.getParameter(ID_PARAM));
			
			req.setAttribute(DISEASE_ATTRIBUTE, diseaseService.getDisease(diseaseId));
		
		} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
			LOGGER.error(e);
			return VIEW_ERROR;
		} catch (DaoException e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
			LOGGER.error(e);
			return VIEW_ERROR;			
		}
	
		return VIEW_SUCCESS;
	}

}
