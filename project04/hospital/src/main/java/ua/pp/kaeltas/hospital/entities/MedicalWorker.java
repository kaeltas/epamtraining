package ua.pp.kaeltas.hospital.entities;

import java.sql.Date;

/**
 * MedicalWorker entity.
 * It holds information about one row from DB table MedicalWorker.
 *  
 * @author Matsokha Andrey
 */
public class MedicalWorker {
	private int id;
	private String officeNumber;
	private Date hireDate;
	private User user;
	private MedicalWorkerProfession medicalWorkerProfession;
	
	
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getOfficeNumber() {
		return officeNumber;
	}
	
	public void setOfficeNumber(String officeNumber) {
		this.officeNumber = officeNumber;
	}
	
	public Date getHireDate() {
		return hireDate;
	}
	
	public void setHireDate(Date hireDate) {
		this.hireDate = hireDate;
	}
	
	public User getUser() {
		return user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	
	public MedicalWorkerProfession getMedicalWorkerProfession() {
		return medicalWorkerProfession;
	}
	
	public void setMedicalWorkerProfession(
			MedicalWorkerProfession medicalWorkerProfession) {
		this.medicalWorkerProfession = medicalWorkerProfession;
	}
	
	
}
