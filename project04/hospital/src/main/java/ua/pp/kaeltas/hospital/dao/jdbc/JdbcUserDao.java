package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.UserDao;
import ua.pp.kaeltas.hospital.entities.Role;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcUserDao implements UserDao {
	private static final String SELECT_USER_WITH_ROLE = 
			"SELECT * "
			+ "FROM User u, Role r "
			+ "WHERE u.id = ? "
			+ "AND u.Role_id = r.id";
	private static final String SELECT_USER = 
			"SELECT * "
			+ "FROM User u "
			+ "WHERE u.email = ?";
	private static final String SELECT_ALL_USERS = 
			"SELECT * FROM User";
	
	

	@Override
	public void create(User user) {
		throw new UnsupportedOperationException();
	}

	@Override
	public User find(int id) throws DaoException {
		
		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn
					.prepareStatement(SELECT_USER_WITH_ROLE);
			
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			User user = null;
			if (rs.first()) {
				user = new User();
				user.setId(rs.getInt("u.id"));
				user.setEmail(rs.getString("u.email"));
				user.setFirstName(rs.getString("u.first_name"));
				user.setPasswordHash(rs.getString("u.password_hash"));
				
				Role role = new Role();
				role.setId(rs.getInt("r.id"));
				role.setName(rs.getString("r.name"));
				
				user.setRole(role);
			}
			
			rs.close();
			ps.close();
			
			if (user == null) {
				throw new DaoExceptionNoSuchEntity("User NOT found by id=" + id);
			}
			
			return user;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}
	
	@Override
	public User findByEmail(String email) throws DaoException {
		
		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn
					.prepareStatement(SELECT_USER);
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			
			User user = null;
			if (rs.first()) {		
				user = find(rs.getInt("u.id"));
			}
			
			rs.close();
			ps.close();
			
			if (user == null) {
				throw new DaoExceptionNoSuchEntity("User NOT found by email=" + email);
			}
			
			return user;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public List<User> findAll() throws DaoException {
		
		try (Connection conn = new JdbcConnection().getConnection()) {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(SELECT_ALL_USERS);
			
			List<User> users = new LinkedList<>();
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setEmail(rs.getString("email"));
				user.setFirstName(rs.getString("first_name"));
				user.setPasswordHash(rs.getString("password_hash"));
				
				Role role = new Role();
				role.setId(rs.getInt("Role_id"));
				user.setRole(role);
			
				users.add(user);
			}
			
			rs.close();
			st.close();
			
			return users;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public void update(User user) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}


}
