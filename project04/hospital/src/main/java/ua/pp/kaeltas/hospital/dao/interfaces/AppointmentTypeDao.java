package ua.pp.kaeltas.hospital.dao.interfaces;

import ua.pp.kaeltas.hospital.entities.AppointmentType;

/**
 * DAO for accessing AppointmentType entity.
 *  
 * @author Matsokha Andrey
 */
public interface AppointmentTypeDao extends BaseDao<AppointmentType>{
}
