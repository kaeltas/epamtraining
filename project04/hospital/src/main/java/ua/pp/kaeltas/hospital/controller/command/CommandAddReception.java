package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.DiseaseService;
import ua.pp.kaeltas.hospital.service.ReceptionService;
import ua.pp.kaeltas.hospital.validator.RequestValidator;
import ua.pp.kaeltas.hospital.validator.ValidatorFactory;

/**
 * Command represents add reception functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandAddReception implements Command {
	
	//returned view constants 
	private static final String VIEW_REDIRECT_ADDED = "redirect:/patient/disease/reception/edit?id=";
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/addReception.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String VIEW_FORBIDDEN = "/WEB-INF/jsp/forbidden.jsp";
	
	//used attributes
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorDiseaseNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String RECEPTION_ATTRIBUTE = "reception";
	
	//used request parameters
	private static final String DIAGNOSIS_PARAM = "diagnosis";
	private static final String SYMPTOMS_PARAM = "symptoms";
	private static final String DISEASE_ID_PARAM = "disease";

	private static final Logger LOGGER = Logger.getLogger(CommandAddReception.class);
	
	private ReceptionService receptionService = new ReceptionService();
	
	private DiseaseService diseaseService = new DiseaseService();
	
	/**
	 * Show and process http form, validate parameters and add reception. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {

		if ("post".equals(req.getMethod().toLowerCase())) {
			try {
				int diseaseId = Integer.parseInt(req.getParameter(DISEASE_ID_PARAM));
				
				if (diseaseService.isDiseaseOpened(diseaseId)) {
					
					String symptoms = req.getParameter(SYMPTOMS_PARAM);
					String diagnosis = req.getParameter(DIAGNOSIS_PARAM);
					
					Reception r = new Reception();
					r.setDisease(diseaseService.getDisease(diseaseId));
					r.setDiagnosis(diagnosis);
					r.setSymptoms(symptoms);
					
					req.setAttribute(RECEPTION_ATTRIBUTE, r);
					
					RequestValidator requestValidator = new RequestValidator(req);
					requestValidator.validate(symptoms, 
							ValidatorFactory.getInstance().createSymptomsValidator());
					requestValidator.validate(diagnosis, 
							ValidatorFactory.getInstance().createDiagnosisValidator());
					
					if (!requestValidator.hasErrors()) {
						int receptionId = receptionService.addReception(r);
						
						return VIEW_REDIRECT_ADDED + receptionId;
					}
				} else {
					return VIEW_FORBIDDEN;
				}
				
			} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
				req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
				LOGGER.error(e);
				return VIEW_ERROR;
			} catch (DaoException e) {
				req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
				LOGGER.error(e);
				return VIEW_ERROR;
			}
		} 
		
		return VIEW_SUCCESS;
	}

}
