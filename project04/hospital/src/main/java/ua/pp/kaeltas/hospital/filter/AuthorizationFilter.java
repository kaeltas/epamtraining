package ua.pp.kaeltas.hospital.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ua.pp.kaeltas.hospital.config.AuthorizationConfig;
import ua.pp.kaeltas.hospital.controller.RequestHelper;
import ua.pp.kaeltas.hospital.entities.User;

/**
 * Servlet Filter implementation.
 * 
 * Provides authorization: if user is not allowed to access this page
 * than it will be redirected to page showing forbidden message; 
 * otherwise - do nothing, just pass to next filter in chain.
 * 
 * @author Matsokha Andrey
 */
public class AuthorizationFilter implements Filter {

	private static final String VIEW_FORBIDDEN_JSP = "/WEB-INF/jsp/forbidden.jsp";
	private static final String USER_SESSION_ATTR = "user";
	
	private RequestHelper requestHelper;
	
	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		requestHelper = new RequestHelper();
	}
	
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// nothing to do here
	}

	/**
	 * Take user from HttpSession. If it exists - check if that user has 
	 * access to current page.
	 * Access rules are written in config file "authorizationRules.properties"
	 * 
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest)request;
		HttpSession session = req.getSession(false);
		if (session != null) {
			User user = (User)session.getAttribute(USER_SESSION_ATTR);
			if (user != null) {
				String reqURI = requestHelper.getRelativeRequestURI(req);
				boolean isForbidden = AuthorizationConfig.getInstance()
						.isForbidden(reqURI, user.getRole()); 
				if (isForbidden) {
					req.getRequestDispatcher(VIEW_FORBIDDEN_JSP)
						.forward(request, response);
					
					return;
				}
			}
		}

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

}
