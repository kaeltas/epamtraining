package ua.pp.kaeltas.hospital.jsptags;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.service.AppointmentService;

/**
 * JSP tag. 
 * Check if current user (e.g. user in HttpSession) could view 
 * body content of this tag.
 * It checks if user can execute given appointment.
 * 
 * @author Matsokha Andrey
 */
public class IsAppointmentAllowed extends SimpleTagSupport {

	private static final String USER_SESSION_ATTR = "user";
	
	/**
	 * Appointment to check
	 */
	private Appointment appointment;
	
	private AppointmentService appointmentService = new AppointmentService();

	/**
	 * Get current user from HttpSession and check if it allowed to
	 * execute given appointment.
	 * If yes - print body content of tag; otherwise - print nothing.
	 */
	@Override
	public void doTag() throws JspException, IOException {
		StringWriter sw = new StringWriter();
		
		getJspBody().invoke(sw); //get body of tag
		PageContext pageContext = (PageContext)getJspContext();
		HttpServletRequest req = (HttpServletRequest)pageContext.getRequest();
		HttpSession session = req.getSession(false);
		if (session != null) { 
			User user = (User)session.getAttribute(USER_SESSION_ATTR);
			if (user != null) {
				if (appointmentService.isAppointmentAllowedToUser(appointment, user)) {
					getJspContext().getOut().print(sw);
					return;
				}
			}
		}
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}
	
}
