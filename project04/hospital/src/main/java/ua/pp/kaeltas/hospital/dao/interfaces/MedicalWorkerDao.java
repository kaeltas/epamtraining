package ua.pp.kaeltas.hospital.dao.interfaces;

import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * DAO for accessing MedicalWorker entity.
 *  
 * @author Matsokha Andrey
 */
public interface MedicalWorkerDao extends BaseDao<MedicalWorker> {

	/**
	 * Find and return MedicalWorker entity related to given User.
	 * @param user
	 * @return
	 * @throws DaoException 
	 */
	MedicalWorker findByUser(User user) throws DaoException;
}
