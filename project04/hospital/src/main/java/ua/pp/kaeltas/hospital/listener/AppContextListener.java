package ua.pp.kaeltas.hospital.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.config.ApplicationConfig;
import ua.pp.kaeltas.hospital.config.AppointmentRolesConfig;
import ua.pp.kaeltas.hospital.config.AuthorizationConfig;
import ua.pp.kaeltas.hospital.dao.interfaces.DaoFactory;
import ua.pp.kaeltas.hospital.dao.interfaces.UserDao;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Servlet context listener.
 * Loads config files at startup. If some of them could not be found 
 * or some other error occurs - application will not start.
 *  
 * @author Matsokha Andrey
 */
public class AppContextListener implements ServletContextListener {

	private static final String ALLUSERS_ATTR = "allusers";
	
	private UserDao userDao = DaoFactory.getInstance().createUserDao();
	private static final Logger LOGGER = Logger.getLogger(AppContextListener.class);
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		
		try {
			sce.getServletContext().setAttribute(ALLUSERS_ATTR, userDao.findAll());
		} catch (DaoException e) {
			LOGGER.error(e);
		}
		
		//Load config singleton classes at application startUp. 
		//If smth goes wrong(properties fileNotFound, etc..) - app will not start.
		ApplicationConfig.getInstance();
		AuthorizationConfig.getInstance();
		AppointmentRolesConfig.getInstance();
		
		LOGGER.info("Application Context Initialized");
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// Do nothing 
	}

}
