package ua.pp.kaeltas.hospital.controller.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.Patient;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.DiseaseService;
import ua.pp.kaeltas.hospital.service.PatientService;
import ua.pp.kaeltas.hospital.service.ReceptionService;

/**
 * Command represents view appointments functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandViewAppointments implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/viewAppointments.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String VIEW_FORBIDDEN = "/WEB-INF/jsp/forbidden.jsp";
	
	
	//used attributes	
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorAppointmentNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String CURRENT_PATIENT_ATTRIBUTE = "currentPatient";
	private static final String RECEPTIONS_ATTRIBUTE = "receptions";
	
	//used request parameters
	private static final String DISEASE_PARAM = "disease";

	private static final Logger LOGGER = Logger.getLogger(CommandViewAppointments.class);
	
	private ReceptionService receptionService = new ReceptionService();

	private PatientService patientService = new PatientService();
	
	private DiseaseService diseaseService = new DiseaseService();
	
	/**
	 * Show appointments by given disease id. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			int diseaseId = Integer.parseInt(req.getParameter(DISEASE_PARAM));
		
			if (diseaseService.isDiseaseOpened(diseaseId)) {
				Disease disease = new Disease();
				disease.setId(diseaseId);
				
				Patient patient = patientService.getPatientByDisease(disease);
				
				List<Reception> receptions = 
						receptionService.getAllReceptionsByDisease(disease);
				
				req.setAttribute(RECEPTIONS_ATTRIBUTE, receptions);
				req.setAttribute(CURRENT_PATIENT_ATTRIBUTE, patient);
			} else {
				return VIEW_FORBIDDEN;
			}
			
		} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
			LOGGER.error(e);
			return VIEW_ERROR;
		} catch (DaoException e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
			LOGGER.error(e);
			return VIEW_ERROR;
		}
		
		return VIEW_SUCCESS;
		
	}

}
