package ua.pp.kaeltas.hospital.entities;

import java.sql.Date;

/**
 * AppointmentExecutionHistory entity.
 * It holds information about one row from DB table AppointmentExecutionHistory.
 *  
 * @author Matsokha Andrey
 */
public class AppointmentExecutionHistory {
	
	private int id;
	
	private Appointment appointment;
	
	private MedicalWorker medicalWorker;
	
	private Date executionDate;
	
	private String comment;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public MedicalWorker getMedicalWorker() {
		return medicalWorker;
	}

	public void setMedicalWorker(MedicalWorker medicalWorker) {
		this.medicalWorker = medicalWorker;
	}

	public Date getExecutionDate() {
		return executionDate;
	}

	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	
}
