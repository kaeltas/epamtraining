package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.DiseaseDao;
import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.entities.Patient;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcDiseaseDao implements DiseaseDao {

	private static final String SELECT_ALL_BY_PATIENT_ID_ORDER_BY_CREATED_DESC = 
			"SELECT * "
			+ "FROM Disease d "
			+ "WHERE d.Patient_id = ? "
			+ "ORDER BY d.created DESC";
	private static final String UPDATE_BY_ID = 
			"UPDATE Disease "
			+ "SET Patient_id = ?, "
			+ "MedicalWorker_id = ?, "
			+ "closed = ? "
			+ "WHERE id = ? ";
	private static final String SELECT_BY_ID = 
			"SELECT * "
			+ "FROM Disease d "
			+ "WHERE d.id = ? ";
	private static final String INSERT = 
			"INSERT INTO Disease "
			+ "(Patient_id, MedicalWorker_id) "
			+ "VALUES (?, ?)";

	@Override
	public int createAndGetId(Disease disease) throws DaoException {
		
		try (Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(
					INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
			
			ps.setInt(1, disease.getPatient().getId());
			ps.setInt(2, disease.getMedicalWorker().getId());
			
			ps.execute();
			
			ResultSet rs = ps.getGeneratedKeys();
			int generatedKey = -1;
			if (rs.first()) {
				generatedKey = rs.getInt(1);
			}

			rs.close();
			ps.close();
			
			return generatedKey;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return -1;

	}

	@Override
	public Disease find(int id) throws DaoException {
		
		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn
					.prepareStatement(SELECT_BY_ID);
			
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Disease d = null;
			if (rs.first()) {
				d = new Disease();
				d.setId(rs.getInt("d.id"));
				d.setCreated(rs.getDate("d.created"));
				d.setClosed(rs.getDate("d.closed"));
				
				MedicalWorker mw = new MedicalWorker();
				mw.setId(rs.getInt("d.MedicalWorker_id"));
				d.setMedicalWorker(mw);
				
				Patient p = new Patient();
				p.setId(rs.getInt("d.Patient_id"));
				d.setPatient(p);
			}
			
			rs.close();
			ps.close();
			
			if (d == null) {
				throw new DaoExceptionNoSuchEntity("Disease NOT found by id=" + id);
			}
			
			return d;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		
		//return null;
	}

	@Override
	public List<Disease> findAll() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Disease disease) throws DaoException {
		
		try (Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(
					UPDATE_BY_ID);
			
			ps.setInt(1, disease.getPatient().getId());
			ps.setInt(2, disease.getMedicalWorker().getId());
			ps.setDate(3, disease.getClosed());
			ps.setInt(4, disease.getId());
			
			ps.execute();
			
			ps.close();
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}

	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Disease> findAllByPatient(Patient p) throws DaoException {
		
		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn
					.prepareStatement(SELECT_ALL_BY_PATIENT_ID_ORDER_BY_CREATED_DESC);
			
			ps.setInt(1, p.getId());
			
			ResultSet rs = ps.executeQuery();
			List<Disease> diseases = new LinkedList<>();
			while (rs.next()) {
				Disease d = new Disease();
				d.setId(rs.getInt("d.id"));
				d.setCreated(rs.getDate("d.created"));
				d.setClosed(rs.getDate("d.closed"));
				
				MedicalWorker mw = new MedicalWorker();
				mw.setId(rs.getInt("d.MedicalWorker_id"));
				d.setMedicalWorker(mw);
				
				d.setPatient(p);
				
				diseases.add(d);
			}
			
			rs.close();
			ps.close();
			
			return diseases;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public void create(Disease t) {
		throw new UnsupportedOperationException();
	}

}
