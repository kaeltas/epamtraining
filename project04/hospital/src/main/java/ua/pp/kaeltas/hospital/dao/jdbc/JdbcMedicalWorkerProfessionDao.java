package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.MedicalWorkerProfessionDao;
import ua.pp.kaeltas.hospital.entities.MedicalWorkerProfession;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcMedicalWorkerProfessionDao implements
		MedicalWorkerProfessionDao {

	private static final String SELECT_BY_ID = 
			"SELECT * "
			+ "FROM MedicalWorkerProfession mwp "
			+ "WHERE mwp.id = ?";

	@Override
	public void create(MedicalWorkerProfession medicalWorkerProfession) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MedicalWorkerProfession find(int id) throws DaoException {
		
		try (Connection conn = new JdbcConnection().getConnection()) {
			
			PreparedStatement ps = conn.prepareStatement(SELECT_BY_ID);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			MedicalWorkerProfession mwp = null;
			if (rs.first()) {
				mwp = new MedicalWorkerProfession();
				mwp.setName(rs.getString("mwp.name"));
				mwp.setId(id);
			}
			
			rs.close();
			ps.close();
			
			if (mwp == null) {
				throw new DaoExceptionNoSuchEntity(
						"MedicalWorkerProfession NOT found by id=" + id);
			}
			
			return mwp;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public List<MedicalWorkerProfession> findAll() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(MedicalWorkerProfession medicalWorkerProfession) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}

}
