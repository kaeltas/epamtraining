package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Command represents logout functionality. 
 * Destroys http session.
 * 
 * @author Matsokha Andrey
 */
public class CommandLogout implements Command {

	//returned view constants 
	private static final String VIEW_REDIRECT_SUCCESS = "redirect:/";

	/**
	 * Destroy session and show main page. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		HttpSession session = req.getSession(false);
		if (session != null) {
			session.invalidate();
		}
		
		return VIEW_REDIRECT_SUCCESS;
	}

}
