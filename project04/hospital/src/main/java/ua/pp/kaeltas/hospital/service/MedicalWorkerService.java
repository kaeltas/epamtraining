package ua.pp.kaeltas.hospital.service;

import ua.pp.kaeltas.hospital.dao.interfaces.DaoFactory;
import ua.pp.kaeltas.hospital.dao.interfaces.MedicalWorkerDao;
import ua.pp.kaeltas.hospital.dao.interfaces.MedicalWorkerProfessionDao;
import ua.pp.kaeltas.hospital.dao.interfaces.UserDao;
import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.entities.MedicalWorkerProfession;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Service class for operations with MedicalWorkers
 * 
 * @author Matsokha Andrey
 */
public class MedicalWorkerService {
	
	private MedicalWorkerDao medicalWorkerDao = DaoFactory.getInstance()
			.createMedicalWorkerDao();

	private MedicalWorkerProfessionDao medicalWorkerProfessionDao = DaoFactory.getInstance()
			.createMedicalWorkerProfessionDao();

	private UserDao userDao = DaoFactory.getInstance()
			.createUserDao();
	

	/**
	 * Load medical worker by id. And load its dependent entities.
	 * @param id id of medical worker to load
	 * @return {@code MedicalWorker} medical worker entity for given id
	 * @throws DaoException 
	 */
	public MedicalWorker getMedicalWorker(int id) throws DaoException {
		
		MedicalWorker medicalWorker = medicalWorkerDao.find(id);
		User user = userDao.find(medicalWorker.getUser().getId());
		medicalWorker.setUser(user);
		
		MedicalWorkerProfession mwp = medicalWorker.getMedicalWorkerProfession();
		mwp = medicalWorkerProfessionDao.find(mwp.getId());
		medicalWorker.setMedicalWorkerProfession(mwp);
		
		return medicalWorker;
	}

	/**
	 * Load medical worker by email. And load its dependent entities.
	 * @param email email of medical worker to load
	 * @return {@code MedicalWorker} medical worker entity for given email
	 * @throws DaoException 
	 */
	public MedicalWorker getMedicalWorkerByEmail(String email) throws DaoException {
		
		User user = userDao.findByEmail(email);
		MedicalWorker medicalWorker = medicalWorkerDao.findByUser(user);
		
		MedicalWorkerProfession mwp = medicalWorker.getMedicalWorkerProfession();
		mwp = medicalWorkerProfessionDao.find(mwp.getId());
		medicalWorker.setMedicalWorkerProfession(mwp);
		
		return medicalWorker;
	}
	
}
