package ua.pp.kaeltas.hospital.dao.interfaces;

import ua.pp.kaeltas.hospital.entities.Role;

/**
 * DAO for accessing Role entity.
 *  
 * @author Matsokha Andrey
 */
public interface RoleDao extends BaseDao<Role>{

}
