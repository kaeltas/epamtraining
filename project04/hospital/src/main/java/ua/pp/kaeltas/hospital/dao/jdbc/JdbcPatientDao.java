package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.PatientDao;
import ua.pp.kaeltas.hospital.entities.Patient;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcPatientDao implements PatientDao {
	private static final String SELECT_BY_DISEASE_ID = 
			"SELECT * "
			+ "FROM Patient p, Disease d "
			+ "WHERE d.id = ? "
			+ "AND d.Patient_id = p.id "
			+ "LIMIT 1 ";
	private static final String SELECT_ALL = 
			"SELECT * "
			+ "FROM Patient p ";
	private static final String SELECT_BY_ID = 
			"SELECT * "
			+ "FROM Patient p "
			+ "WHERE p.id = ? ";
	private static final String INSERT = 
			"INSERT INTO Patient "
			+ "(first_name) "
			+ "VALUES (?)";

	@Override
	public void create(Patient patient) throws DaoException {
		
		try (Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(INSERT);
			
			ps.setString(1, patient.getFirstName());
			
			ps.execute();
			
			ps.close();
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		

	}

	@Override
	public Patient find(int id) throws DaoException {
		
		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(SELECT_BY_ID);
			
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Patient p = null;
			if (rs.first()) {
				p = new Patient();
				p.setId(rs.getInt("p.id"));
				p.setFirstName(rs.getString("p.first_name"));
			}
			
			rs.close();
			ps.close();
			
			if (p == null) {
				throw new DaoExceptionNoSuchEntity("Patient NOT found by id=" + id);
			}
			
			return p;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		
		//return null;
	}

	@Override
	public List<Patient> findAll() throws DaoException {
		
		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(SELECT_ALL);

			ResultSet rs = ps.executeQuery();
			List<Patient> patients = new LinkedList<>();
			while (rs.next()) {
				Patient p = new Patient();
				p.setId(rs.getInt("p.id"));
				p.setFirstName(rs.getString("p.first_name"));
				
				patients.add(p);
			}
			
			rs.close();
			ps.close();
			
			return patients;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public void update(Patient patient) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Patient findByDiseaseId(int id) throws DaoException {
		
		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn
					.prepareStatement(SELECT_BY_DISEASE_ID);
			
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Patient p = null;
			if (rs.first()) {
				p = new Patient();
				p.setId(rs.getInt("p.id"));
				p.setFirstName(rs.getString("p.first_name"));
			}
			
			rs.close();
			ps.close();
			
			if (p == null) {
				throw new DaoExceptionNoSuchEntity(
						"Patient NOT found by disease.id=" + id);
			}
			
			return p;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

}
