package ua.pp.kaeltas.hospital.dao.interfaces;

import ua.pp.kaeltas.hospital.entities.MedicalWorkerProfession;

/**
 * DAO for accessing MedicalWorkerProfession entity.
 *  
 * @author Matsokha Andrey
 */
public interface MedicalWorkerProfessionDao extends BaseDao<MedicalWorkerProfession> {
}
