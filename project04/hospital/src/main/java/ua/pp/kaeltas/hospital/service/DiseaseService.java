package ua.pp.kaeltas.hospital.service;

import java.util.Iterator;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.DaoFactory;
import ua.pp.kaeltas.hospital.dao.interfaces.DiseaseDao;
import ua.pp.kaeltas.hospital.dao.interfaces.MedicalWorkerDao;
import ua.pp.kaeltas.hospital.dao.interfaces.PatientDao;
import ua.pp.kaeltas.hospital.dao.interfaces.UserDao;
import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.entities.Patient;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Service class for operations with Diseases
 * 
 * @author Matsokha Andrey
 */
public class DiseaseService {

	private UserDao userDao = DaoFactory.getInstance()
			.createUserDao();
	
	private MedicalWorkerDao medicalWorkerDao = DaoFactory.getInstance()
			.createMedicalWorkerDao();
	
	private DiseaseDao diseaseDao = DaoFactory.getInstance()
			.createDiseaseDao();
	
	private PatientDao patientDao = DaoFactory.getInstance()
			.createPatientDao();
	
	private ReceptionService receptionService =
			new ReceptionService();
	
	private MedicalWorkerService medicalWorkerService =
			new MedicalWorkerService();
	
	/**
	 * Get all opened diseases for given patient. 
	 * And load dependent entities for each disease.
	 * @param patientId id of patient
	 * @return {@code List<Disease>}
	 * @throws DaoException 
	 */
	public List<Disease> getAllOpenedDiseasesByPatientId(int patientId) throws DaoException {

		Patient p = patientDao.find(patientId);
		List<Disease> diseases = diseaseDao.findAllByPatient(p);
		
		//remove all closed diseases (e.g. where closed date not null)
		Iterator<Disease> it = diseases.iterator();
		for(; it.hasNext() ;) {
			if (it.next().getClosed() != null) {
				it.remove();
			}
		}
		
		//load receptions for each disease
		for (Disease disease : diseases) {
			List<Reception> receptions = 
					receptionService.getAllReceptionsByDisease(disease);
			
			disease.setReceptions(receptions);
		}
		
		return diseases;
		
	}

	/**
	 * Assign new disease entity for given doctor and patient. 
	 * @param doctorEmail email of doctor
	 * @param patientId id of patient
	 * @return {@ int} id of created entity in DB table
	 * @throws DaoException 
	 */
	public int addDisease(String doctorEmail, int patientId) throws DaoException {
		
		User u = userDao.findByEmail(doctorEmail);
		MedicalWorker mw = medicalWorkerDao.findByUser(u);
		Patient p = patientDao.find(patientId);
	
		Disease d = new Disease();
		d.setMedicalWorker(mw);
		d.setPatient(p);
		
		int diseaseId = diseaseDao.createAndGetId(d);
		
		return diseaseId;
	}

	/**
	 * Load disease by id. And load its dependent entities.
	 * @param diseaseId id of disease to load
	 * @return {@code Disease} disease entity for given id
	 * @throws DaoException 
	 */
	public Disease getDisease(int diseaseId) throws DaoException {
		Disease d = diseaseDao.find(diseaseId);
		
		Patient p = patientDao.find(d.getPatient().getId());
		d.setPatient(p);
		
		MedicalWorker mw = medicalWorkerService.getMedicalWorker(
				d.getMedicalWorker().getId());
		d.setMedicalWorker(mw);
		
		List<Reception> receptions =
				receptionService.getAllReceptionsByDisease(d);
		d.setReceptions(receptions);
		
		return d;
	}

	/**
	 * Update given disease entity in DB
	 * @param disease disease to update
	 * @throws DaoException 
	 */
	public void updateDisease(Disease disease) throws DaoException {
		diseaseDao.update(disease);
	}

	/**
	 * Get all closed diseases for given patient. 
	 * And load dependent entities for each disease.
	 * @param patientId id of patient
	 * @return {@code List<Disease>}
	 * @throws DaoException 
	 */
	public List<Disease> getAllClosedDiseasesByPatientId(int patientId) throws DaoException {

		Patient p = patientDao.find(patientId);
		List<Disease> diseases = diseaseDao.findAllByPatient(p);
		
		//remove all opened diseases (e.g. where closed date eq null)
		Iterator<Disease> it = diseases.iterator();
		for(; it.hasNext() ;) {
			if (it.next().getClosed() == null) {
				it.remove();
			}
		}
		
		//load all reception for each disease
		for (Disease disease : diseases) {
			List<Reception> receptions =
					receptionService.getAllReceptionsByDisease(disease);
			
			disease.setReceptions(receptions);
		}
		
		return diseases;

	}
	
	/**
	 * Check if current disease is allowed to continue treatment, i.e. if 
	 * disease is not closed.
	 * @param diseaseId
	 * @return
	 */
	public boolean isDiseaseOpened(int diseaseId) throws DaoException {
		Disease d = diseaseDao.find(diseaseId);
		return (d.getClosed() == null);
	}
	
	/**
	 * Check if current disease is allowed to continue treatment by current user,
	 * i.e. if doctor that created this disease is equal to current 
	 * authenticated user.
	 * @param diseaseId, user
	 * @return
	 */
	public boolean isDiseaseAuth(int diseaseId, User user) throws DaoException {
		Disease d = diseaseDao.find(diseaseId);
		MedicalWorker medicalWorker = medicalWorkerDao.findByUser(user);
		
		return (d.getMedicalWorker().getId() == medicalWorker.getId());
	}

	
}
