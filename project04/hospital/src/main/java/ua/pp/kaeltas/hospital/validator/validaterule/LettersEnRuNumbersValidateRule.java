package ua.pp.kaeltas.hospital.validator.validaterule;

/**
 * Validation rule for checking if string value contains En/Ru letters and 
 * digits.
 *  
 * @author Matsokha Andrey
 */
public class LettersEnRuNumbersValidateRule implements ValidateRule {

	/**
	 * Return true if value contains ONLY En/Ru letters and digits;
	 * false - otherwise.
	 */
	@Override
	public boolean valid(String value) {
		
		if (value == null) {
			return false;
		}
		
		if (!value.matches("[a-zA-Zа-яА-Я0-9]*")) {
			return false;
		}
		
		return true;
	}

	@Override
	public String getErrorMessage() {
		return "validatorErrorLetters";
	}

}
