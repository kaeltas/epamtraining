package ua.pp.kaeltas.hospital.service;

import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.DaoFactory;
import ua.pp.kaeltas.hospital.dao.interfaces.DiseaseDao;
import ua.pp.kaeltas.hospital.dao.interfaces.MedicalWorkerDao;
import ua.pp.kaeltas.hospital.dao.interfaces.ReceptionDao;
import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Service class for operations with Receptions
 * 
 * @author Matsokha Andrey
 */
public class ReceptionService {
	
	private ReceptionDao receptionDao = DaoFactory.getInstance()
			.createReceptionDao();

	private DiseaseDao diseaseDao = DaoFactory.getInstance()
			.createDiseaseDao();
	
	private MedicalWorkerDao medicalWorkerDao = DaoFactory.getInstance()
			.createMedicalWorkerDao();
	
	private AppointmentService appointmentService =
			new AppointmentService();
	
	/**
	 * Load all receptions for given disease. And load dependent entities
	 *  for each reception. 
	 * @param disease id of disease, for which all receptions 
	 * will be loaded
	 * @return {@code List<Reception>} list of receptions for given disease
	 * @throws DaoException 
	 */
	public List<Reception> getAllReceptionsByDisease(Disease disease) throws DaoException {

		List<Reception> receptions = receptionDao.findAllByDisease(disease);
		
		for (Reception reception : receptions) {
			List<Appointment> appointments = 
					appointmentService.getAppointmentsByReception(reception.getId());
			reception.setAppointments(appointments);
		}
		
		return receptions;
	}

	/**
	 * Save reception in DB.
	 * @param reception reception to save
	 * @return {@code int} id of saved reception
	 * @throws DaoException 
	 */
	public int addReception(Reception reception) throws DaoException {
		int  receptionId = receptionDao.createAndGetId(reception);
		return receptionId;
	}

	/**
	 * Load reception by id. And load its dependent entities.
	 * @param receptionId id of reception to load
	 * @return {@code Reception} reception entity for given id
	 * @throws DaoException 
	 */
	public Reception getReception(int receptionId) throws DaoException {
		
		Reception r = receptionDao.find(receptionId);
		List<Appointment> appointments = 
				appointmentService.getAppointmentsByReception(receptionId);
		r.setAppointments(appointments);
		
		return r;
	}
	
	/**
	 * Update given reception entity in DB
	 * @param r reception to update
	 * @throws DaoException 
	 */
	public void updateReception(Reception r) throws DaoException {
		receptionDao.update(r);
	}

	/**
	 * Check if disease related to given reception is opened.
	 * @param receptionId
	 * @return
	 * @throws DaoException
	 */
	public boolean isDiseaseForReceptionOpened(int receptionId) throws DaoException {
		Reception r = receptionDao.find(receptionId);
		
		Disease d = diseaseDao.find(r.getDisease().getId());
		
		return d.getClosed() == null;
	}
	
	/**
	 * Check if disease related to given reception is allowed 
	 * to current user, i.e. if doctor that created related disease 
	 * is equal to current authenticated user.
	 * @param receptionId
	 * @return
	 * @throws DaoException
	 */
	public boolean isDiseaseForReceptionAuth(int receptionId, User user) throws DaoException {
		Reception r = receptionDao.find(receptionId);
		Disease d = diseaseDao.find(r.getDisease().getId());
		
		MedicalWorker medicalWorker = medicalWorkerDao.findByUser(user);
		
		return d.getMedicalWorker().getId() == medicalWorker.getId();
	}

}