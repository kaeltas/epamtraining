package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.AppointmentType;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.AppointmentService;
import ua.pp.kaeltas.hospital.service.ReceptionService;
import ua.pp.kaeltas.hospital.validator.RequestValidator;
import ua.pp.kaeltas.hospital.validator.ValidatorFactory;

/**
 * Command represents add appointment functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandAddAppointment implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/addAppointment.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String VIEW_FORBIDDEN = "/WEB-INF/jsp/forbidden.jsp";

	//used attributes
	private static final String ERROR_MSG_DAO = "errorDao";
	private static final String ERROR_MSG_NOT_FOUND = "errorReceptionNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String APPOINTMENTS_ATTRIBUTE = "appointments";
	private static final String APPOINTMENT_TYPES_ATTRIBUTE = "appointmentTypes";
	private static final String CURRENT_APPOINTMENT_ATTRIBUTE = "currentAppointment";
	private static final String USER_ATTRIBUTE = "user";
	
	//used request parameters
	private static final String APPOINTMENT_TYPE_PARAM = "appointmentType";
	private static final String DESCRIPTION_PARAM = "description";
	private static final String RECEPTION_ID_PARAM = "reception";

	private static final Logger LOGGER = Logger.getLogger(CommandAddAppointment.class);
	
	private AppointmentService appointmentService = 
			new AppointmentService();
	
	private ReceptionService receptionService = 
			new ReceptionService();
	
	/**
	 * Show and process http form, validate parameters and add appointment. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			int receptionId = Integer.parseInt(req.getParameter(RECEPTION_ID_PARAM));
			
			HttpSession session = req.getSession();
			User user = (User)session.getAttribute(USER_ATTRIBUTE);
			
			if (receptionService.isDiseaseForReceptionOpened(receptionId) 
					&& receptionService.isDiseaseForReceptionAuth(receptionId, user)) {
		
				if ("post".equals(req.getMethod().toLowerCase())) {
					String description = req.getParameter(DESCRIPTION_PARAM);
					int appointmentTypeId = Integer.parseInt(
							req.getParameter(APPOINTMENT_TYPE_PARAM));
					
					Appointment appointment = new Appointment();
					appointment.setDescription(description);
					
					AppointmentType at = new AppointmentType();
					at.setId(appointmentTypeId);
					appointment.setAppointmentType(at);
					
					Reception r = receptionService.getReception(receptionId);
					appointment.setReception(r);
	
					req.setAttribute(CURRENT_APPOINTMENT_ATTRIBUTE, appointment);
					
					RequestValidator requestValidator = new RequestValidator(req);
					requestValidator.validate(description, 
							ValidatorFactory.getInstance().createDescriptionValidator());
					
					if (!requestValidator.hasErrors()) {
						appointmentService.saveAppointment(appointment);
					}
				}
			
			
				req.setAttribute(APPOINTMENT_TYPES_ATTRIBUTE, 
						appointmentService.getAllAppointmentTypes());
				
				req.setAttribute(APPOINTMENTS_ATTRIBUTE, 
						appointmentService.getAppointmentsByReception(receptionId));
			} else {
				return VIEW_FORBIDDEN;
			}

		} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
			LOGGER.error(e);
			return VIEW_ERROR;
		} catch (DaoException e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
			LOGGER.error(e);
			return VIEW_ERROR;
		}
		
		return VIEW_SUCCESS;
	}

}
