package ua.pp.kaeltas.hospital.jsptags;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.service.DiseaseService;

/**
 * JSP tag. 
 * Check if current user (e.g. user in HttpSession) could view 
 * body content of this tag.
 * It checks if user can continue treatment of given disease.
 * 
 * @author Matsokha Andrey
 */
public class IsDiseaseAuth extends SimpleTagSupport {

	private static final String USER_SESSION_ATTR = "user";
	
	private DiseaseService diseaseService = new DiseaseService();

	private Disease disease;

	public void setDisease(Disease disease) {
		this.disease = disease;
	}

	/**
	 * Get current user from HttpSession and check if it allowed to
	 * continue treatment of given disease.
	 * If yes - print body content of tag; otherwise - print nothing.
	 */
	@Override
	public void doTag() throws JspException, IOException {
		StringWriter sw = new StringWriter();
		
		getJspBody().invoke(sw); //get body of tag
		PageContext pageContext = (PageContext)getJspContext();
		HttpServletRequest req = (HttpServletRequest)pageContext.getRequest();
		HttpSession session = req.getSession(false);
		if (session != null) { 
			User user = (User)session.getAttribute(USER_SESSION_ATTR);
			if (user != null) {
				try {
					if (diseaseService.isDiseaseAuth(disease.getId(), user)) {
						getJspContext().getOut().print(sw);
						return;
					}
				} catch (DaoException e) {
					Logger.getLogger(IsDiseaseAuth.class).error(e);
					getJspContext().getOut().print("Error. Refresh page.");
				}
			}
		}
	}
	
}
