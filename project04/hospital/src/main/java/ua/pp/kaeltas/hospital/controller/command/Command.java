package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Interface representing command, which would be executed by FrontController
 * servlet. 
 *  
 * @author Matsokha Andrey
 */
public interface Command {

	/**
	 * Command, executed by FrontController servlet.
	 * @param req HttpServletRequest
	 * @param resp HttpServletResponse
	 * @return {@code String} view page
	 */
	String execute(HttpServletRequest req, HttpServletResponse resp);

}
