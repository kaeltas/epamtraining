package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.DiseaseService;
import ua.pp.kaeltas.hospital.service.ReceptionService;
import ua.pp.kaeltas.hospital.validator.RequestValidator;
import ua.pp.kaeltas.hospital.validator.ValidatorFactory;

/**
 * Command represents add disease functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandAddDisease implements Command {

	//returned view constants 
	private static final String VIEW_REDIRECT_ADDED = "redirect:/patient/disease/reception/edit?id=";
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/addPatientDisease.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	
	//used attributes
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorPatientNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String RECEPTION_ATTRIBUTE = "reception";
	private static final String USER_ATTRIBUTE = "user";
	
	//used request parameters
	private static final String DIAGNOSIS_PARAM = "diagnosis";
	private static final String SYMPTOMS_PARAM = "symptoms";
	private static final String PATIENT_ID_PARAM = "patient";

	private static final Logger LOGGER = Logger.getLogger(CommandAddDisease.class);
	
	private DiseaseService diseaseService = new DiseaseService();
	
	private ReceptionService receptionService = new ReceptionService();
	
	/**
	 * Show and process http form, validate parameters and add disease. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		if ("post".equals(req.getMethod().toLowerCase())) {
			try {
				int patientId = Integer.parseInt(req.getParameter(PATIENT_ID_PARAM));
				
				HttpSession session = req.getSession();
				User user = (User)session.getAttribute(USER_ATTRIBUTE);
				
				String doctorEmail =  user.getEmail();
				
				int diseaseId = diseaseService.addDisease(doctorEmail, patientId);
				
				String symptoms = req.getParameter(SYMPTOMS_PARAM);
				String diagnosis = req.getParameter(DIAGNOSIS_PARAM);
				
				Reception r = new Reception();
				r.setDisease(diseaseService.getDisease(diseaseId));
				r.setDiagnosis(diagnosis);
				r.setSymptoms(symptoms);
				
				req.setAttribute(RECEPTION_ATTRIBUTE, r);
				
				RequestValidator requestValidator = new RequestValidator(req);
				requestValidator.validate(symptoms, 
						ValidatorFactory.getInstance().createSymptomsValidator());
				requestValidator.validate(diagnosis, 
						ValidatorFactory.getInstance().createDiagnosisValidator());
				
				if (!requestValidator.hasErrors()) {
					int receptionId = receptionService.addReception(r);
					
					return VIEW_REDIRECT_ADDED + receptionId;
				}
				
			} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
				req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
				LOGGER.error(e);
				return VIEW_ERROR;
			} catch (DaoException e) {
				req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
				LOGGER.error(e);
				return VIEW_ERROR;
			}
		}

		return VIEW_SUCCESS;
	}

}
