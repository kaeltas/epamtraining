package ua.pp.kaeltas.hospital.dao.interfaces;

import java.util.List;

import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * DAO for accessing Reception entity.
 *  
 * @author Matsokha Andrey
 */
public interface ReceptionDao extends BaseDao<Reception> {
	
	/**
	 * Save instance of Reception in DB and return {@code id} of stored instance.  
	 * @param reception
	 * @return
	 * @throws DaoException 
	 */
	int createAndGetId(Reception reception) throws DaoException;
	
	/**
	 * Fint and return all Receptions related to given Disease.
	 * @param disease
	 * @return
	 * @throws DaoException 
	 */
	List<Reception> findAllByDisease(Disease disease) throws DaoException;

}
