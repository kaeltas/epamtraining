package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Patient;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.service.PatientService;
import ua.pp.kaeltas.hospital.validator.RequestValidator;
import ua.pp.kaeltas.hospital.validator.ValidatorFactory;

/**
 * Command represents add patient functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandAddPatient implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/addPatient.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";

	//used attributes
	private static final String SUCCESS_MSG_ATTRIBUTE = "successRegister";
	private static final String SUCCESS_MSG = "successRegister";
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_ATTRIBUTE = "error";
	
	//used request parameters
	private static final String NAME_PARAM = "name";
	
	private static final Logger LOGGER = Logger.getLogger(CommandAddPatient.class);

	private PatientService patientService = new PatientService();
	
	/**
	 * Show and process http form, validate parameters and register patient. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {

		if ("post".equals(req.getMethod().toLowerCase())) {
			String name = req.getParameter(NAME_PARAM);
			
			RequestValidator requestValidator = new RequestValidator(req);
			requestValidator.validate(name, 
					ValidatorFactory.getInstance().createNameValidator());

			if (!requestValidator.hasErrors()) {
				Patient p = new Patient();
				p.setFirstName(name);
				
				try {
					patientService.addPatient(p);
				} catch (DaoException e) {
					req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
					LOGGER.error(e);
					return VIEW_ERROR;
				}
				
				req.setAttribute(SUCCESS_MSG_ATTRIBUTE, SUCCESS_MSG);
			}
			
		}
		
		return VIEW_SUCCESS;
	}

}
