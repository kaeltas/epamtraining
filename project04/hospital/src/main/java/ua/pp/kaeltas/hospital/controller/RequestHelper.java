package ua.pp.kaeltas.hospital.controller;

import javax.servlet.http.HttpServletRequest;

/**
 * Class for auxiliary operations with HttpServletRequest.
 *  
 * @author Matsokha Andrey
 */
public class RequestHelper {
	
	/**
	 * Return relative requestURI, excluding application name, e.g. ContextPath. 
	 * @param req request URI
	 * @return request URI excluding application name, e.g. ContextPath
	 */
	public String getRelativeRequestURI(HttpServletRequest req) {
		String path = req.getRequestURI().toLowerCase();
		String appName = req.getContextPath();
		if (path.startsWith(appName)) {
			path = path.substring(
				path.indexOf(appName) + appName.length())
				.toLowerCase();
		}
		return path;
	}
	
}
