package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.DiseaseService;

/**
 * Command represents edit disease functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandEditDisease implements Command {
	
	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/editDisease.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String VIEW_FORBIDDEN = "/WEB-INF/jsp/forbidden.jsp";
	
	//used attributes
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorDiseaseNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String DISEASE_ATTRIBUTE = "disease";
	private static final String USER_ATTRIBUTE = "user";
	
	//used request parameters
	private static final String ID_PARAM = "id";
	
	private static final Logger LOGGER = Logger.getLogger(CommandEditDisease.class);
	
	private DiseaseService diseaseService = new DiseaseService();

	/**
	 * Show and process http form, validate parameters and edit disease. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			int diseaseId = Integer.parseInt(req.getParameter(ID_PARAM));
			
			HttpSession session = req.getSession();
			User user = (User)session.getAttribute(USER_ATTRIBUTE);
			
			if (diseaseService.isDiseaseOpened(diseaseId)
					&& diseaseService.isDiseaseAuth(diseaseId, user)) {
				req.setAttribute(DISEASE_ATTRIBUTE, diseaseService.getDisease(diseaseId));
			} else {
				return VIEW_FORBIDDEN;
			}
			
		} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
			LOGGER.error(e);
			return VIEW_ERROR;
		} catch (DaoException e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
			LOGGER.error(e);
			return VIEW_ERROR;
		}
		
		return VIEW_SUCCESS;
	}

}
