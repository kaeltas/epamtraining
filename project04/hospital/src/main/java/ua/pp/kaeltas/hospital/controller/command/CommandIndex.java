package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Command represents index page. 
 * 
 * @author Matsokha Andrey
 */
public class CommandIndex implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/applicationIndex.jsp";

	/**
	 * Show index page. 
	 */
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		return VIEW_SUCCESS;
	}

}
