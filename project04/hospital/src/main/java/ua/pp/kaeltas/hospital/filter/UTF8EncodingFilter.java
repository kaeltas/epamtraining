package ua.pp.kaeltas.hospital.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Servlet Filter implementation.
 * 
 * Set request & response character encoding.
 * Must be first filter in chain.
 * 
 * @author Matsokha Andrey
 */
public class UTF8EncodingFilter implements Filter {

	private static final String REQ_RESP_ENCODING = "UTF-8";

	/**
	 * Set request & response character encoding.
	 * 
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		request.setCharacterEncoding(REQ_RESP_ENCODING);
        response.setCharacterEncoding(REQ_RESP_ENCODING);

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// Do nothing
	}
	
	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// Do nothing
	}


}
