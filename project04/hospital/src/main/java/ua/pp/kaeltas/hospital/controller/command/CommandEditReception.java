package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.ReceptionService;
import ua.pp.kaeltas.hospital.validator.RequestValidator;
import ua.pp.kaeltas.hospital.validator.ValidatorFactory;

/**
 * Command represents edit reception functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandEditReception implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/editReception.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String VIEW_FORBIDDEN = "/WEB-INF/jsp/forbidden.jsp";
	
	//used attributes
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorReceptionNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String RECEPTION_ATTRIBUTE = "reception";
	private static final String USER_ATTRIBUTE = "user";
	
	//used request parameters
	private static final String ID_PARAM = "id";
	private static final String DIAGNOSIS_PARAM = "diagnosis";
	private static final String SYMPTOMS_PARAM = "symptoms";
	
	private static final Logger LOGGER = Logger.getLogger(CommandEditReception.class);
	
	private ReceptionService receptionService = new ReceptionService();
	
	/**
	 * Show and process http form, validate parameters and edit reception. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			int receptionId = Integer.parseInt(req.getParameter(ID_PARAM));
			
			HttpSession session = req.getSession();
			User user = (User)session.getAttribute(USER_ATTRIBUTE);
			
			if (receptionService.isDiseaseForReceptionOpened(receptionId)
					&& receptionService.isDiseaseForReceptionAuth(receptionId, user)) {
				
				Reception r = receptionService.getReception(receptionId);
				
				if ("post".equals(req.getMethod().toLowerCase())) {
					String symptoms = req.getParameter(SYMPTOMS_PARAM);
					String diagnosis = req.getParameter(DIAGNOSIS_PARAM);
					
					r.setDiagnosis(diagnosis);
					r.setSymptoms(symptoms);
					
					RequestValidator requestValidator = new RequestValidator(req);
					requestValidator.validate(symptoms, 
							ValidatorFactory.getInstance().createSymptomsValidator());
					requestValidator.validate(diagnosis, 
							ValidatorFactory.getInstance().createDiagnosisValidator());
					
					if (!requestValidator.hasErrors()) {
						receptionService.updateReception(r);
					}
				}
				
				req.setAttribute(RECEPTION_ATTRIBUTE, r);
			} else {
				return VIEW_FORBIDDEN;
			}
		
		} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
			LOGGER.error(e);
			return VIEW_ERROR;
		} catch (DaoException e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
			LOGGER.error(e);
			return VIEW_ERROR;
		}
		
		return VIEW_SUCCESS;
	}

}
