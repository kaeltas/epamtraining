package ua.pp.kaeltas.hospital.service;

import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.DaoFactory;
import ua.pp.kaeltas.hospital.dao.interfaces.PatientDao;
import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.Patient;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Service class for operations with Patients
 * 
 * @author Matsokha Andrey
 */
public class PatientService {
	
	private PatientDao patientDao = DaoFactory.getInstance()
			.createPatientDao();
	
	/**
	 * Save given patient in DB
	 * @param p patient to save
	 * @throws DaoException 
	 */
	public void addPatient(Patient p) throws DaoException {
		patientDao.create(p);
	}

	/**
	 * Load all patients from DB
	 * @return {@code List<Patient>}
	 * @throws DaoException 
	 */
	public List<Patient> getAllPatients() throws DaoException {
		return patientDao.findAll();
	}
	
	/**
	 * Load patient by id.
	 * @param id id of patient to load
	 * @return {@code Patient} patient entity for given id
	 * @throws DaoException 
	 */
	public Patient getPatient(int id) throws DaoException {
		return patientDao.find(id);
	}

	/**
	 * Load patient by disease.
	 * @param disease disease of patient to load
	 * @return {@code Patient} patient entity for given disease
	 * @throws DaoException 
	 */
	public Patient getPatientByDisease(Disease disease) throws DaoException {
		return patientDao.findByDiseaseId(disease.getId());
	}
}
