package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentDao;
import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.AppointmentType;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcAppointmentDao implements AppointmentDao {

	private static final String FIND_ALL_BY_ID = "SELECT * "
			+ "FROM Appointment a "
			+ "WHERE a.Reception_id = ? ";
	private static final String SELECT_BY_ID = "SELECT * "
			+ "FROM Appointment a "
			+ "WHERE a.id = ? ";
	private static final String INSERT = "INSERT INTO Appointment "
			+ "(description, AppointmentType_id, Reception_id) "
			+ "VALUES "
			+ "(?, ?, ?)";

	@Override
	public void create(Appointment appointment) throws DaoException {

		try(Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(INSERT);
			ps.setString(1, appointment.getDescription());
			ps.setInt(2, appointment.getAppointmentType().getId());
			ps.setInt(3, appointment.getReception().getId());
			
			ps.execute();
			
			ps.close();
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
	}

	@Override
	public Appointment find(int id) throws DaoException {

		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn
					.prepareStatement(SELECT_BY_ID);
			
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			Appointment a = null;
			if (rs.first()) {
				a = new Appointment();
				a.setId(rs.getInt("a.id"));
				a.setDescription(rs.getString("a.description"));
				
				Reception r = new Reception();
				r.setId(rs.getInt("a.Reception_id"));
				a.setReception(r);
				
				AppointmentType at = new AppointmentType();
				at.setId(rs.getInt("a.AppointmentType_id"));
				a.setAppointmentType(at);
				
			}
			
			rs.close();
			ps.close();
			
			if (a == null) {
				throw new DaoExceptionNoSuchEntity("Appointment NOT found by id=" + id);
			}
			
			return a;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public List<Appointment> findAll() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(Appointment appointment) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Appointment> findAllByReception(Reception reception) throws DaoException {
		
		try(Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(FIND_ALL_BY_ID);
			ps.setInt(1, reception.getId());
			
			ResultSet rs = ps.executeQuery();
			List<Appointment> appointments = new LinkedList<>();
			while(rs.next()) {
				Appointment appointment = new Appointment();
				
				appointment.setId(rs.getInt("a.id"));
				
				appointment.setReception(reception);
				
				AppointmentType at = new AppointmentType();
				at.setId(rs.getInt("a.AppointmentType_id"));
				appointment.setAppointmentType(at);
				
				appointment.setDescription(rs.getString("a.description"));
				
				appointments.add(appointment);
			}
			
			rs.close();
			ps.close();
			
			return appointments;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}


}
