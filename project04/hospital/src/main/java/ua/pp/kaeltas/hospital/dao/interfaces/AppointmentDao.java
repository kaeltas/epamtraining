package ua.pp.kaeltas.hospital.dao.interfaces;

import java.util.List;

import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * DAO for accessing Appointment entity.
 *  
 * @author Matsokha Andrey
 */
public interface AppointmentDao extends BaseDao<Appointment> {

	/**
	 * Find and return all appointments related to given reception.
	 * 
	 * @param reception
	 * @return {@code List<Appointments>}
	 * @throws DaoException 
	 */
	List<Appointment> findAllByReception(Reception reception) throws DaoException;
	
}
