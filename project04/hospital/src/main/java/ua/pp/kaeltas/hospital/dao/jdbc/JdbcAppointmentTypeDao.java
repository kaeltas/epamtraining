package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentTypeDao;
import ua.pp.kaeltas.hospital.entities.AppointmentType;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcAppointmentTypeDao implements AppointmentTypeDao {

	private static final String SELECT_ALL = "SELECT * FROM AppointmentType at";
	private static final String SELECT_BY_ID = 
			"SELECT * "
			+ "FROM AppointmentType at "
			+ "WHERE at.id = ?";

	@Override
	public void create(AppointmentType appointmentType) {
		throw new UnsupportedOperationException();
	}

	@Override
	public AppointmentType find(int id) throws DaoException {
		
		try(Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(SELECT_BY_ID);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			AppointmentType at = null;
			if (rs.first()) {
				at = new AppointmentType();
				at.setId(id);
				at.setType(rs.getString("at.type"));
			}
			
			rs.close();
			ps.close();
			
			if (at == null) {
				throw new DaoExceptionNoSuchEntity("AppointmentType NOT found by id=" + id);
			}
			
			return at;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public List<AppointmentType> findAll() throws DaoException {
		
		try(Connection conn = new JdbcConnection().getConnection()) {
			Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(SELECT_ALL);
			
			List<AppointmentType> appointmentTypes = new LinkedList<>();
			while(rs.next()) {
				AppointmentType at = new AppointmentType();
				
				at.setId(rs.getInt("at.id"));
				at.setType(rs.getString("at.type"));
				
				appointmentTypes.add(at);
			}
			
			rs.close();
			st.close();
			
			return appointmentTypes;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		
		//return null;
	}

	@Override
	public void update(AppointmentType appointmentType) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}

}
