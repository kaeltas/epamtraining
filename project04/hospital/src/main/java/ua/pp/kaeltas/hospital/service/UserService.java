package ua.pp.kaeltas.hospital.service;

import org.jboss.security.auth.spi.Util;

import ua.pp.kaeltas.hospital.dao.interfaces.DaoFactory;
import ua.pp.kaeltas.hospital.dao.interfaces.UserDao;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.entities.immutable.UserImmutable;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Service class for operations with Users
 * 
 * @author Matsokha Andrey
 */
public class UserService {

	private static final String HASH_ALGORITHM = "MD5";
	
	private UserDao userDao = DaoFactory.getInstance().createUserDao();
	
	/**
	 * User authentication,
	 * Check login and password pair.
	 * @param email
	 * @param password
	 * @return true, if given login/password pair equals to stored in DB 
	 * login/password pair; false - otherwise.
	 * @throws DaoException 
	 */
	public boolean checkUser(String email, String password) throws DaoException {
		
		String passwordHash = Util.createPasswordHash(HASH_ALGORITHM, 
				Util.BASE64_ENCODING, null, null, password);
		User user = userDao.findByEmail(email);
		if (passwordHash != null 
				&& user != null 
				&& passwordHash.equals(user.getPasswordHash())) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * Load user entity by email.
	 * @param email email of user to load
	 * @return {@code User} user entity for given email
	 * @throws DaoException 
	 */
	public User getUser(String email) throws DaoException {
		User user = userDao.findByEmail(email);
		return user;
	}
	
	/**
	 * Load immutable version of user entity by email.
	 * @param email email of user to load
	 * @return {@code UserImmutable} immutable user entity for given email
	 * @throws DaoException 
	 */
	public User getUserImmutable(String email) throws DaoException {
		User user = getUser(email);
		return new UserImmutable(user);
	}
	
}
