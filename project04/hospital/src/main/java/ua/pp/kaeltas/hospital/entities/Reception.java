package ua.pp.kaeltas.hospital.entities;

import java.sql.Date;
import java.util.List;

/**
 * Reception entity.
 * It holds information about one row from DB table Reception.
 *  
 * @author Matsokha Andrey
 */
public class Reception {
	private int id;
	
	private String symptoms;
	
	private String diagnosis;
	
	private Date receptionDate;
	
	private Disease disease;
	
	private List<Appointment> appointments;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getSymptoms() {
		return symptoms;
	}

	public void setSymptoms(String symptoms) {
		this.symptoms = symptoms;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public Date getReceptionDate() {
		return receptionDate;
	}

	public void setReceptionDate(Date reseptionDate) {
		this.receptionDate = reseptionDate;
	}

	public Disease getDisease() {
		return disease;
	}

	public void setDisease(Disease disease) {
		this.disease = disease;
	}

	public List<Appointment> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

	
	
}
