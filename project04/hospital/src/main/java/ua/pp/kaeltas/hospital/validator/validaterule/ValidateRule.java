package ua.pp.kaeltas.hospital.validator.validaterule;

/**
 * Interface of validating rule, used in validator. 
 * 
 * @author Matsokha Andrey
 */
public interface ValidateRule {

	/**
	 * Provides validation of given string value.
	 * @param value
	 * @return {@code boolean} true if no errors while validation found;
	 * 	false - otherwise;
	 */
	boolean valid(String value);

	/**
	 * Return error message that corresponds to this validation rule.
	 * @return {@code String}
	 */
	String getErrorMessage();
	
}
