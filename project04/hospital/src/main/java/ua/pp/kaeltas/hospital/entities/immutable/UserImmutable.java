package ua.pp.kaeltas.hospital.entities.immutable;

import ua.pp.kaeltas.hospital.entities.Role;
import ua.pp.kaeltas.hospital.entities.User;

public final class UserImmutable extends User {

	public UserImmutable(User user) {
		super();
		super.setEmail(user.getEmail());
		super.setFirstName(user.getFirstName());
		super.setId(user.getId());
		super.setPasswordHash(user.getPasswordHash());
		super.setRole(user.getRole());
		super.setSecondName(user.getSecondName());
	}
	
	@Override
	public void setId(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setEmail(String email) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setPasswordHash(String passwordHash) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setFirstName(String firstName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setSecondName(String secondName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setRole(Role role) {
		throw new UnsupportedOperationException();
	}
	
	

}
