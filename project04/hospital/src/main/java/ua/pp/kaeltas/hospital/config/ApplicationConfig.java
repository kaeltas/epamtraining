package ua.pp.kaeltas.hospital.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class loads application specific properties and provides access to them.
 *  
 * @author Matsokha Andrey
 */
public class ApplicationConfig {

	// Private class for loading on demand singleton instance 
	private static class ApplicationConfigHolder {
		public static final ApplicationConfig instance = new ApplicationConfig();
	}
	
	private static final String APPLICATION_CONFIG_PROPERTIES = 
			"ua/pp/kaeltas/hospital/config/applicationConfig.properties";

	private Properties applicationProp;
	
	/**
	 * Constructor, initializes Properties instance and loads properties file.
	 */
	private ApplicationConfig() {
		applicationProp = new Properties();
		
		InputStream is = getClass().getClassLoader().getResourceAsStream(APPLICATION_CONFIG_PROPERTIES);
		try {
			applicationProp.load(is);
		} catch (IOException e) {
			throw new RuntimeException(e);
			//e.printStackTrace();
		}
	}
	
	/**
	 * Return instance of singleton config class
	 * @return
	 */
	public static ApplicationConfig getInstance() {
		return ApplicationConfigHolder.instance;
	}
	
	/**
	 * Return {@code String} property by given {@code String} key.
	 * @param key
	 * @return
	 */
	public String getProperty(String key) {
		return applicationProp.getProperty(key);
	}
	
}
