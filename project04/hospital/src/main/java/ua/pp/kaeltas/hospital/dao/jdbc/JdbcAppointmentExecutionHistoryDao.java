package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentExecutionHistoryDao;
import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.AppointmentExecutionHistory;
import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcAppointmentExecutionHistoryDao implements
		AppointmentExecutionHistoryDao {

	private static final String SELECT_BY_APPOINTMENT_ID = 
			"SELECT * "
			+ "FROM AppointmentExecutionHistory aeh "
			+ "WHERE aeh.Appointment_id = ? ";
	private static final String INSERT = 
			"INSERT INTO AppointmentExecutionHistory "
			+ "(Appointment_id, MedicalWorker_id, comment) "
			+ "VALUES "
			+ "(?, ?, ?)";

	@Override
	public void create(AppointmentExecutionHistory appExecHist) throws DaoException {
		
		try(Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(INSERT);
			ps.setInt(1, appExecHist.getAppointment().getId());
			ps.setInt(2, appExecHist.getMedicalWorker().getId());
			ps.setString(3, appExecHist.getComment());
			
			ps.execute();
			
			ps.close();
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}

	}

	@Override
	public AppointmentExecutionHistory find(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<AppointmentExecutionHistory> findAll() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void update(AppointmentExecutionHistory appointmentExecutionHistory) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public AppointmentExecutionHistory findByAppointment(Appointment appointment) throws DaoException {
		
		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn
					.prepareStatement(SELECT_BY_APPOINTMENT_ID);
			
			ps.setInt(1, appointment.getId());
			ResultSet rs = ps.executeQuery();
			AppointmentExecutionHistory aeh = null;
			if (rs.first()) {
				aeh = new AppointmentExecutionHistory();
				aeh.setId(rs.getInt("aeh.id"));
				aeh.setComment(rs.getString("aeh.comment"));
				
				aeh.setAppointment(appointment);
				aeh.setExecutionDate(rs.getDate("aeh.execution_date"));

				MedicalWorker mw = new MedicalWorker();
				mw.setId(rs.getInt("aeh.MedicalWorker_id"));
				aeh.setMedicalWorker(mw);
				
			}
			
			rs.close();
			ps.close();
			
			return aeh;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

}
