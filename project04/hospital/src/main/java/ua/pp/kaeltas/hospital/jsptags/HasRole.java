package ua.pp.kaeltas.hospital.jsptags;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import ua.pp.kaeltas.hospital.entities.User;

/**
 * JSP tag. 
 * Check if current user (e.g. user in HttpSession) could view 
 * body content of this tag.
 * It equals user role with list of allowed roles.
 * 
 * @author Matsokha Andrey
 */
public class HasRole extends SimpleTagSupport {
	
	private static final String ROLES_SEPARATOR = ",";
	private static final String USER_SESSION_ATTR = "user";
	
	/**
	 * List of allowed roles
	 */
	private String roles;

	/**
	 * Get role of current user from HttpSession and check if it equals to
	 * one of the allowed {@code roles}.
	 * If yes - print body content of tag; otherwise - print nothing.
	 */
	@Override
	public void doTag() throws JspException, IOException {
		
		StringWriter sw = new StringWriter();
		
		getJspBody().invoke(sw); //get body of tag
		PageContext pageContext = (PageContext)getJspContext();
		HttpServletRequest req = (HttpServletRequest)pageContext.getRequest();
		HttpSession session = req.getSession(false);
		if (session != null) { 
			User user = (User)session.getAttribute(USER_SESSION_ATTR);
			if (user != null) {
				String[] arRoles = roles.split(ROLES_SEPARATOR);
				
				for (String role : arRoles) {
					if (user.getRole().getName().equals((role.trim()))) {
						getJspContext().getOut().print(sw);
						return;
					}
				}
			}
		}
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}
	
}
