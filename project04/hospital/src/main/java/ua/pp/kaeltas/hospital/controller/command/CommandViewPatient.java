package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;
import ua.pp.kaeltas.hospital.service.DiseaseService;
import ua.pp.kaeltas.hospital.service.PatientService;

/**
 * Command represents view patient functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandViewPatient implements Command {

	//returned view constants 
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/viewPatient.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	
	//used attributes
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_MSG_NOT_FOUND = "errorPatientNotFound";
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String CLOSED_DISEASES_ATTRIBUTE = "closedDiseases";
	private static final String PATIENT_ATTRIBUTE = "patient";
	private static final String DISEASES_ATTRIBUTE = "diseases";
	
	//used request parameters
	private static final String ID_PARAM = "id";

	private static final Logger LOGGER = Logger.getLogger(CommandViewPatient.class);
	
	private DiseaseService diseaseService = new DiseaseService();
	
	private PatientService patientService =	new PatientService();
	
	/**
	 * Show patient page by id. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			int patientId = Integer.parseInt(req.getParameter(ID_PARAM));
			
			req.setAttribute(DISEASES_ATTRIBUTE, 
					diseaseService.getAllOpenedDiseasesByPatientId(patientId));
			
			req.setAttribute(PATIENT_ATTRIBUTE, 
					patientService.getPatient(patientId));
			
			req.setAttribute(CLOSED_DISEASES_ATTRIBUTE,
					diseaseService.getAllClosedDiseasesByPatientId(patientId));
			
			
		} catch (NumberFormatException | DaoExceptionNoSuchEntity e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_NOT_FOUND);
			LOGGER.error(e);
			return VIEW_ERROR;
		} catch (DaoException e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
			LOGGER.error(e);
			return VIEW_ERROR;
		}
		
		return VIEW_SUCCESS;
	}

}
