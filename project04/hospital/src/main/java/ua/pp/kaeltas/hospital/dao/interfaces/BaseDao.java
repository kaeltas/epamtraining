package ua.pp.kaeltas.hospital.dao.interfaces;

import java.util.List;

import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Base interface for CRUD DAO methods.  
 * 
 * @author Matsokha Andrey
 */
public interface BaseDao<T> {
	
	/**
	 * Save instance of class T into DB. 
	 * @param t
	 * @throws DaoException 
	 */
	void create(T t) throws DaoException;
	
	/**
	 * Find and return object by {@code id} in DB. 
	 * @param id
	 * @return
	 * @throws DaoException 
	 */
	T find(int id) throws DaoException;
	
	/**
	 * Find and return all objects in DB.
	 * @return
	 * @throws DaoException 
	 */
	List<T> findAll() throws DaoException;
	
	/**
	 * Modify object in DB.
	 * @param t
	 * @throws DaoException 
	 */
	void update(T t) throws DaoException;
	
	/**
	 * Delete object by {@code id} from DB.
	 * @param id
	 */
	void delete(int id);
}
