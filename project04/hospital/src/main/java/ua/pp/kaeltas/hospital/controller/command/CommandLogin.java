package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.service.UserService;
import ua.pp.kaeltas.hospital.validator.RequestValidator;
import ua.pp.kaeltas.hospital.validator.ValidatorFactory;

/**
 * Command represents login functionality. 
 * When user is authenticated - creates http session for him.
 * 
 * @author Matsokha Andrey
 */
public class CommandLogin implements Command {
	
	//returned view constants 
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/login.jsp";
	private static final String VIEW_REDIRECT_SUCCESS = "redirect:";
	
	//used attributes
	private static final String REDIRECT_PATH_QUERY_STRING_ATTRIBUTE = "redirectPathQueryString";
	private static final String REDIRECT_PATH_ATTRIBUTE = "redirectPath";
	private static final String USER_ATTRIBUTE = "user";
	//error attribute and message for login failure
	private static final String ERROR_LOGIN_ATTRIBUTE = "loginError";
	private static final String ERROR_LOGIN_MSG = "loginError";
	//error attribute and message for general errors
	private static final String ERROR_ATTRIBUTE = "error";
	private static final String ERROR_MSG_DAO = "errorDao";
	
	//used request parameters
	private static final String USERNAME_PARAM = "username";
	private static final String PASSWORD_PARAM = "password";
	
	private static final Logger LOGGER = Logger.getLogger(CommandLogin.class);
	
	UserService userService = new UserService();
	
	/**
	 * Show and process http form, validate parameters and authenticate user. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		if ("post".equals(req.getMethod().toLowerCase())) {
			String email = req.getParameter(USERNAME_PARAM);
			String password = req.getParameter(PASSWORD_PARAM);
			
			RequestValidator requestValidator = new RequestValidator(req);
			requestValidator.validate(email, 
					ValidatorFactory.getInstance().createLoginValidator());
			requestValidator.validate(password, 
					ValidatorFactory.getInstance().createPasswordValidator());
			
			
			if (!requestValidator.hasErrors()) {
				try {
					if (userService.checkUser(email, password)) {
						HttpSession session = req.getSession();
						session.setAttribute(USER_ATTRIBUTE, userService.getUserImmutable(email));
						
						String redirectUri = buildRedirectURI(
								(String)req.getAttribute(REDIRECT_PATH_ATTRIBUTE),
								(String)req.getAttribute(REDIRECT_PATH_QUERY_STRING_ATTRIBUTE));
	
						return VIEW_REDIRECT_SUCCESS + redirectUri;
					} else {
						req.setAttribute(ERROR_LOGIN_ATTRIBUTE, ERROR_LOGIN_MSG);
					}
				} catch(DaoException e) {
					req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
					LOGGER.error(e);
					return VIEW_ERROR;
				}
			}
			
		}
		
		return VIEW_SUCCESS;
	}
	
	/**
	 * Creates reditect URI with query parameters and maps "index.jsp" to "/".
	 * @param path
	 * @param queryString
	 * @return built redirect URI
	 */
	private String buildRedirectURI(String path, String queryString) {
		StringBuilder redirectUri = new StringBuilder();
		if ("/index.jsp".equals(path)) {
			redirectUri.append("/");
		} else {
			redirectUri.append(path);
		}
		if (queryString != null) {
			redirectUri.append("?")
				.append(queryString);
		}
		
		return redirectUri.toString();
	}

}
