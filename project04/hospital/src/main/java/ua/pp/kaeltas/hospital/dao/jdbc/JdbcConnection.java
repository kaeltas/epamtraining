package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import ua.pp.kaeltas.hospital.config.ApplicationConfig;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Class retrieves JDBC connection from connection pool.
 *  
 * @author Matsokha Andrey
 */
public class JdbcConnection {

	/**
	 * Return Connection to DB retrieved from connection pool.
	 * @return
	 * @throws DaoException 
	 */
	public Connection getConnection() throws DaoException {
		
		try {
			String jndiDatasource = ApplicationConfig.getInstance()
					.getProperty("jndi_datasource");
			InitialContext initialContext = new InitialContext();
			DataSource ds = (DataSource)initialContext
					.lookup(jndiDatasource);
			return ds.getConnection();
			
		} catch (NamingException | SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}
	
}
