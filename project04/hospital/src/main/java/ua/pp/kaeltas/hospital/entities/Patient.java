package ua.pp.kaeltas.hospital.entities;

/**
 * Patient entity.
 * It holds information about one row from DB table Patient.
 *  
 * @author Matsokha Andrey
 */
public class Patient {
	
	private int id;
	
	private String firstName;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", firstName=" + firstName + "]";
	}
	
	
	
}
