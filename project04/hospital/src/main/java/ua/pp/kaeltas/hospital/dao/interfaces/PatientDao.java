package ua.pp.kaeltas.hospital.dao.interfaces;

import ua.pp.kaeltas.hospital.entities.Patient;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * DAO for accessing Patient entity.
 *  
 * @author Matsokha Andrey
 */
public interface PatientDao extends BaseDao<Patient> {

	/**
	 * Find and return Patient entity related to given disease id.
	 * @param id disease id
	 * @return
	 * @throws DaoException 
	 */
	Patient findByDiseaseId(int id) throws DaoException;
	
}
