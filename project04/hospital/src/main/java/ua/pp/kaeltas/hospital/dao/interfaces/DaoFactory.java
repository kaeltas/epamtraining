package ua.pp.kaeltas.hospital.dao.interfaces;

import ua.pp.kaeltas.hospital.dao.jdbc.JdbcDaoFactory;

/**
 * Abstract factory that creates Dao's.  
 * @author Matsokha Andrey
 */
public abstract class DaoFactory {

	public abstract UserDao createUserDao();
	public abstract RoleDao createRoleDao();
	public abstract ReceptionDao createReceptionDao();
	public abstract MedicalWorkerDao createMedicalWorkerDao();
	public abstract MedicalWorkerProfessionDao createMedicalWorkerProfessionDao();
	public abstract AppointmentDao createAppointmentDao();
	public abstract AppointmentTypeDao createAppointmentTypeDao();
	public abstract AppointmentExecutionHistoryDao createAppointmentExecutionHistoryDao();
	public abstract PatientDao createPatientDao();
	public abstract DiseaseDao createDiseaseDao();
	
	/**
	 * Return instance of factory.
	 * @return
	 */
	public static DaoFactory getInstance() {
		return new JdbcDaoFactory();
	}
	
	
	
}
