package ua.pp.kaeltas.hospital.exception;

/**
 * Exception thrown if no entity found by given sql-query. 
 * @author Matsokha Andrey
 */
public class DaoExceptionNoSuchEntity extends DaoException {

	/**
	 * Default SerialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	public DaoExceptionNoSuchEntity(String message) {
		super(message);
	}

	public DaoExceptionNoSuchEntity(Throwable cause) {
		super(cause);
	}
	
}
