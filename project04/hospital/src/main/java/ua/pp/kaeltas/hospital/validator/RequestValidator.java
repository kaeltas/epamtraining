package ua.pp.kaeltas.hospital.validator;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Provides validation of given string parameter by given validator and saves
 * found errors in request scope. 
 * 
 * @author Matsokha Andrey
 */
public class RequestValidator {
	private static final String VALIDATOR_ERROR_ATTRIBUTE = "validatorErrors";

	/**
	 * Request for holding found validation errors
	 */
	private HttpServletRequest req;

	private boolean hasErrors = false;
	
	public RequestValidator(HttpServletRequest req) {
		this.req = req;
	}

	/**
	 * Validate given string parameter by given validator and save
	 * found errors in request scope. 
	 * @param value parameter to check validation
	 * @param validator validator used for checking
	 */
	@SuppressWarnings("unchecked")
	public void validate(String value, Validator validator) {
		validator.validate(value);
		if (validator.hasErrors()) {
			hasErrors = validator.hasErrors();
			List<String> errors = (List<String>)req.getAttribute(VALIDATOR_ERROR_ATTRIBUTE);
			if (errors == null) {
				errors = validator.getErrors();
			} else {
				errors.addAll(validator.getErrors());
			}
			req.setAttribute(VALIDATOR_ERROR_ATTRIBUTE, errors);
		}
	}
	
	/**
	 * Return true if at least one validation rule of validator fails; 
	 * false - otherwise.
	 * @return
	 */
	public boolean hasErrors() {
		return hasErrors;
	}
	
}
