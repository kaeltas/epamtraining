package ua.pp.kaeltas.hospital.validator;

import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.hospital.validator.validaterule.ValidateRule;

/**
 * Validator for string params.
 *  
 * @author Matsokha Andrey
 */
public class Validator {

	private static final String ERRORS_PREFIX_SEPARATOR = ".";

	/**
	 * List of validating rules
	 */
	private List<ValidateRule> validateRules = new LinkedList<>();
	
	/**
	 * List of found errors, while validating
	 */
	private List<String> errors = new LinkedList<>();
	
	/**
	 * Prefix for each error. Used for localization of same errors ids 
	 * in different validators.
	 */
	private String errorsPrefix;
	
	public Validator(String errorsPrefix) {
		this.errorsPrefix = errorsPrefix + ERRORS_PREFIX_SEPARATOR;
	}

	/**
	 * Validate given string, using each validate rule from list of rules.
	 * If validation fails - add error message to list of errors.
	 * @param value
	 */
	public void validate(String value) {
		for (ValidateRule validateRule : validateRules) {
			if (!validateRule.valid(value)) {
				errors.add(errorsPrefix + validateRule.getErrorMessage());
			}
		}
	}
	
	/**
	 * Add new validation rule.
	 * @param validateRule
	 * @return
	 */
	public Validator add(ValidateRule validateRule) {
		validateRules.add(validateRule);
		return this;
	}
	
	/**
	 * Return true if validation by at least one rule fails; false - otherwise.
	 * @return
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}
	
	/**
	 * Get list of validation errors.
	 * @return {@code List<String>} list of errors
	 */
	public List<String> getErrors() {
		return errors;
	}
}
