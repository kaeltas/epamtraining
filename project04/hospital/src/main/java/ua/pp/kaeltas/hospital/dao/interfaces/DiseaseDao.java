package ua.pp.kaeltas.hospital.dao.interfaces;

import java.util.List;

import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.Patient;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * DAO for accessing Disease entity.
 *  
 * @author Matsokha Andrey
 */
public interface DiseaseDao extends BaseDao<Disease>{

	/**
	 * Save instance of Disease in DB and return {@code id} of stored instance.  
	 * @param disease
	 * @return
	 * @throws DaoException 
	 */
	int createAndGetId(Disease disease) throws DaoException;
	
	/**
	 * Find and return all Diseases related to given Patient.
	 * @param p
	 * @return {@code List<Disease>}
	 * @throws DaoException 
	 */
	List<Disease> findAllByPatient(Patient p) throws DaoException;
	
}
