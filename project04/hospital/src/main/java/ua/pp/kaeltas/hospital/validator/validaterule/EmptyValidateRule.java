package ua.pp.kaeltas.hospital.validator.validaterule;

/**
 * Validation rule for checking if string value is empty.
 *  
 * @author Matsokha Andrey
 */
public class EmptyValidateRule implements ValidateRule {

	/**
	 * Return true if value is null or empty;
	 * false - otherwise.
	 */
	@Override
	public boolean valid(String value) {
		
		if (value == null) {
			return false;
		}
		
		if (value.length() == 0) {
			return false;
		}
		
		return true;
	}

	@Override
	public String getErrorMessage() {
		return "validatorErrorEmpty";
	}

}
