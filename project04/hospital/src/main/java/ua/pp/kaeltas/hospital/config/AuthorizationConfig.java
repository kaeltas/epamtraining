package ua.pp.kaeltas.hospital.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.entities.Role;

/**
 * Class loads authorization rules from properties-file 
 * and provides access to them.
 *  
 * @author Matsokha Andrey
 */
public class AuthorizationConfig {

	// Private class for loading on demand singleton instance
	private static class AuthorizationConfigHolder {
		public static final AuthorizationConfig instance = new AuthorizationConfig();
	}
	
	private static final String AUTHORIZATION_PROPERTIES = 
			"ua/pp/kaeltas/hospital/config/authorizationRules.properties";

	private static final Logger LOGGER = Logger.getLogger(AuthorizationConfig.class);
	
	private Properties authorizationRules;

	/**
	 * Constructor, initializes Properties instance and loads properties file.
	 */
	private AuthorizationConfig() {
		authorizationRules = new Properties();
		
		InputStream stream = getClass().getClassLoader().getResourceAsStream(AUTHORIZATION_PROPERTIES);
		
		try {
			authorizationRules.load(stream);
		} catch (IOException e) {
			LOGGER.error("Can't load Authorization Config", e);
			throw new RuntimeException(e);
		}
		
	}
	
	/**
	 * Return instance of singleton config class
	 * @return
	 */
	public static AuthorizationConfig getInstance() {
		return AuthorizationConfigHolder.instance;
	}
	
	/**
	 * Return array {@code String} roles, who could access given 
	 * {@code String} request URI.
	 * @param reqURI
	 * @return
	 */
	public String[] getRoles(String reqURI) {
		String rolesForURI = authorizationRules.getProperty(reqURI);

		//if URI found - return list of roles
		if (rolesForURI != null) {
			return rolesForURI.split(",");
		}
		
		if (reqURI.endsWith("/*")) {
			reqURI = reqURI.substring(0, reqURI.length()-2);
		}

		int lastIndexOfSlash = reqURI.lastIndexOf("/");
		//if no URI, including wildcarded subURIs not found - forbid it
		if (lastIndexOfSlash == -1) {
			return new String[0]; //return empty roles
		}
		
		String wildcardURI = reqURI.substring(0, lastIndexOfSlash) + "/*";

		return getRoles(wildcardURI);
	}
	
	/**
	 * Check if given role has access to given request URI
	 * @param reqURI request URI to check
	 * @param role role to check
	 * @return true, if {@code role} has access to {@code reqURI};
	 * 			false - otherwise.
	 */
	public boolean isForbidden(String reqURI, Role role) {

		String[] arrRolesForURI = getRoles(reqURI);
		for (String thisRole : arrRolesForURI) {
			if (thisRole.equals(role.getName())) {
				return false;
			}
		}
		
		return true;
	}
	
	
	
		
		
	
	
	
}
