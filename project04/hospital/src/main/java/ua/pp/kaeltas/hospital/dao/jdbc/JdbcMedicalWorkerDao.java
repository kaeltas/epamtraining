package ua.pp.kaeltas.hospital.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.hospital.dao.interfaces.MedicalWorkerDao;
import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.entities.MedicalWorkerProfession;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.exception.DaoExceptionNoSuchEntity;

/**
 * JDBC implementation of DAO.
 * 
 * @author Matsokha Andrey
 */
public class JdbcMedicalWorkerDao implements MedicalWorkerDao {
	private static final String SELECT_ALL = 
			"SELECT * "
			+ "FROM MedicalWorker mw ";
	private static final String SELECT_BY_USER_ID =
			"SELECT * "
			+ "FROM MedicalWorker mw "
			+ "WHERE mw.User_id = ?";
	private static final String SELECT_BY_ID = 
			"SELECT * "
			+ "FROM MedicalWorker mw "
			+ "WHERE mw.id = ? ";

	@Override
	public void create(MedicalWorker medicalWorker) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MedicalWorker find(int id) throws DaoException {

		try ( Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn
					.prepareStatement(SELECT_BY_ID);
			
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			MedicalWorker mw = null;
			if (rs.first()) {
				mw = new MedicalWorker();
				mw.setId(rs.getInt("mw.id"));
				mw.setHireDate(rs.getDate("mw.hire_date"));
				mw.setOfficeNumber(rs.getString("mw.office_number"));
				
				User user = new User();
				user.setId(rs.getInt("mw.User_id"));
				mw.setUser(user);
				
				MedicalWorkerProfession mwp = new MedicalWorkerProfession();
				mwp.setId(rs.getInt("mw.MedicalWorkerProfession_id"));
				mw.setMedicalWorkerProfession(mwp);
			}
			
			rs.close();
			ps.close();
			
			if (mw == null) {
				throw new DaoExceptionNoSuchEntity("MedicalWorker NOT found by id=" + id);
			}
			
			return mw;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public List<MedicalWorker> findAll() throws DaoException {
		
		try(Connection conn = new JdbcConnection().getConnection()) {
			List<MedicalWorker> medicalWorkers = new LinkedList<>();
			
			Statement statement = conn.createStatement();
			ResultSet rs = statement.executeQuery(SELECT_ALL);
			while (rs.next()) {
				MedicalWorker mw = new MedicalWorker();
				
				User user = new User();
				user.setId(rs.getInt("mw.User_id"));
				mw.setUser(user);
				
				MedicalWorkerProfession mwp = new MedicalWorkerProfession();
				mwp.setId(rs.getInt("mw.MedicalWorkerProfession_id"));
				mw.setMedicalWorkerProfession(mwp);
				
				mw.setId(rs.getInt("mw.id"));
				mw.setOfficeNumber(rs.getString("mw.office_number"));
				
				medicalWorkers.add(mw);
			}
			
			rs.close();
			statement.close();
			
			return medicalWorkers;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

	@Override
	public void update(MedicalWorker medicalWorker) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete(int id) {
		throw new UnsupportedOperationException();
	}

	@Override
	public MedicalWorker findByUser(User user) throws DaoException {
		
		try(Connection conn = new JdbcConnection().getConnection()) {
			PreparedStatement ps = conn.prepareStatement(SELECT_BY_USER_ID);
			ps.setInt(1, user.getId());
			
			ResultSet rs = ps.executeQuery();
			
			MedicalWorker mw = null;
			if (rs.first()) {
				mw = new MedicalWorker();
				mw.setId(rs.getInt("mw.id"));
				mw.setUser(user);
				mw.setHireDate(rs.getDate("mw.hire_date"));
				mw.setOfficeNumber(rs.getString("mw.office_number"));
				
				MedicalWorkerProfession mwp = new MedicalWorkerProfession();
				mwp.setId(rs.getInt("mw.MedicalWorkerProfession_id"));
				mw.setMedicalWorkerProfession(mwp);
			}

			rs.close();
			ps.close();
			
			if (mw == null) {
				throw new DaoExceptionNoSuchEntity(
						"MedicalWorker NOT found by user.id=" + user.getId());
			}
			
			return mw;
			
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		//return null;
	}

}
