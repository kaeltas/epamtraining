package ua.pp.kaeltas.hospital.validator.validaterule;

/**
 * Validation rule for checking if string value length is in given constraints. 
 *  
 * @author Matsokha Andrey
 */
public class SizeValidateRule implements ValidateRule {

	/**
	 * Miminum length of checked value
	 */
	private int minSize;
	
	/**
	 * Maxinum length of checked value
	 */
	private int maxSize;
	
	public SizeValidateRule(int minSize, int maxSize) {
		this.minSize = minSize;
		this.maxSize = maxSize;
	}

	/**
	 * Return true if length of value is in given constraints; 
	 * false - otherwise.
	 */
	@Override
	public boolean valid(String value) {
		
		if (value == null) {
			return false;
		}
		
		if (value.length() < minSize || value.length() > maxSize) {
			return false;
		}
		
		return true;
	}

	@Override
	public String getErrorMessage() {
		return "validatorErrorSize";
	}

}
