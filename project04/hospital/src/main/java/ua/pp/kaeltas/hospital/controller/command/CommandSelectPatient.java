package ua.pp.kaeltas.hospital.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.exception.DaoException;
import ua.pp.kaeltas.hospital.service.PatientService;

/**
 * Command represents select patient functionality. 
 * 
 * @author Matsokha Andrey
 */
public class CommandSelectPatient implements Command {
	
	//returned view constants  
	private static final String VIEW_SUCCESS = "/WEB-INF/jsp/selectPatient.jsp";
	private static final String VIEW_ERROR = "/WEB-INF/jsp/error.jsp";

	//used attributes
	private static final String PATIENTS_ATTRIBUTE = "patients";
	private static final String ERROR_MSG_DAO = "daoError";
	private static final String ERROR_ATTRIBUTE = "error";

	private static final Logger LOGGER = Logger.getLogger(CommandSelectPatient.class);
	
	private PatientService patientService = 
			new PatientService();
	
	/**
	 * Show all patients. 
	 */
	@Override
	public String execute(HttpServletRequest req, HttpServletResponse resp) {
		
		try {
			req.setAttribute(PATIENTS_ATTRIBUTE, 
					patientService.getAllPatients());
		} catch (DaoException e) {
			req.setAttribute(ERROR_ATTRIBUTE, ERROR_MSG_DAO);
			LOGGER.error(e);
			return VIEW_ERROR;
		}
		
		return VIEW_SUCCESS;
		
	}

}
