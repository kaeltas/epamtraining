package ua.pp.kaeltas.hospital.dao.jdbc;

import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentDao;
import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentExecutionHistoryDao;
import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentTypeDao;
import ua.pp.kaeltas.hospital.dao.interfaces.DaoFactory;
import ua.pp.kaeltas.hospital.dao.interfaces.DiseaseDao;
import ua.pp.kaeltas.hospital.dao.interfaces.MedicalWorkerDao;
import ua.pp.kaeltas.hospital.dao.interfaces.MedicalWorkerProfessionDao;
import ua.pp.kaeltas.hospital.dao.interfaces.PatientDao;
import ua.pp.kaeltas.hospital.dao.interfaces.ReceptionDao;
import ua.pp.kaeltas.hospital.dao.interfaces.RoleDao;
import ua.pp.kaeltas.hospital.dao.interfaces.UserDao;

/**
 * JDBC implementation of abstract DAO factory.
 * 
 * @author Matsokha Andrey
 */
public class JdbcDaoFactory extends DaoFactory {

	@Override
	public UserDao createUserDao() {
		return new JdbcUserDao();
	}

	@Override
	public RoleDao createRoleDao() {
		throw new UnsupportedOperationException();
	}

	@Override
	public MedicalWorkerDao createMedicalWorkerDao() {
		return new JdbcMedicalWorkerDao();
	}

	@Override
	public MedicalWorkerProfessionDao createMedicalWorkerProfessionDao() {
		return new JdbcMedicalWorkerProfessionDao();
	}

	@Override
	public AppointmentDao createAppointmentDao() {
		return new JdbcAppointmentDao();
	}

	@Override
	public AppointmentTypeDao createAppointmentTypeDao() {
		return new JdbcAppointmentTypeDao();
	}

	@Override
	public AppointmentExecutionHistoryDao createAppointmentExecutionHistoryDao() {
		return new JdbcAppointmentExecutionHistoryDao();
	}

	@Override
	public ReceptionDao createReceptionDao() {
		return new JdbcReceptionDao();
	}

	@Override
	public PatientDao createPatientDao() {
		return new JdbcPatientDao();
	}

	@Override
	public DiseaseDao createDiseaseDao() {
		return new JdbcDiseaseDao();
	}

}
