package ua.pp.kaeltas.hospital.service;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import ua.pp.kaeltas.hospital.config.AppointmentRolesConfig;
import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentDao;
import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentExecutionHistoryDao;
import ua.pp.kaeltas.hospital.dao.interfaces.AppointmentTypeDao;
import ua.pp.kaeltas.hospital.dao.interfaces.DaoFactory;
import ua.pp.kaeltas.hospital.dao.interfaces.DiseaseDao;
import ua.pp.kaeltas.hospital.dao.interfaces.ReceptionDao;
import ua.pp.kaeltas.hospital.entities.Appointment;
import ua.pp.kaeltas.hospital.entities.AppointmentExecutionHistory;
import ua.pp.kaeltas.hospital.entities.AppointmentType;
import ua.pp.kaeltas.hospital.entities.Disease;
import ua.pp.kaeltas.hospital.entities.MedicalWorker;
import ua.pp.kaeltas.hospital.entities.Reception;
import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * Service class for operations with Appointments
 * 
 * @author Matsokha Andrey
 */
public class AppointmentService {

	private static final Logger LOGGER = Logger.getLogger(AppointmentService.class);

	private AppointmentTypeDao appointmentTypeDao = DaoFactory.getInstance()
			.createAppointmentTypeDao();
	
	private AppointmentDao appointmentDao = DaoFactory.getInstance()
			.createAppointmentDao();

	private AppointmentExecutionHistoryDao appointmentExecutionHistoryDao = 
			DaoFactory.getInstance().createAppointmentExecutionHistoryDao();
	
	private ReceptionDao receptionDao = DaoFactory.getInstance()
			.createReceptionDao();

	private DiseaseDao diseaseDao = DaoFactory.getInstance()
			.createDiseaseDao();
	
	private MedicalWorkerService medicalWorkerService = 
			new MedicalWorkerService();
	
	/**
	 * Return all appointment types
	 * @return {@code List<AppointmentType>}
	 * @throws DaoException 
	 */
	public List<AppointmentType> getAllAppointmentTypes() throws DaoException {
		return appointmentTypeDao.findAll();
	}

	/**
	 * Save given appointment in DB
	 * @param appointment
	 * @throws DaoException 
	 */
	public void saveAppointment(Appointment appointment) throws DaoException {
		appointmentDao.create(appointment);
	}

	/**
	 * Load all appointments for given reception. And load dependent entities
	 *  for each appointment. 
	 * @param receptionId id of reception, for which all appointments 
	 * will be loaded
	 * @return {@code List<Appointment>} list of appointments for given reception
	 * @throws DaoException 
	 */
	public List<Appointment> getAppointmentsByReception(Integer receptionId) throws DaoException {
		
		Reception reception = receptionDao.find(receptionId);
		List<Appointment> appointments = appointmentDao
				.findAllByReception(reception);
		
		for (Appointment appointment : appointments) {
			AppointmentType at = appointmentTypeDao.find(
					appointment.getAppointmentType().getId());
			appointment.setAppointmentType(at);
			
			AppointmentExecutionHistory aeh = appointmentExecutionHistoryDao
					.findByAppointment(appointment);
			if (aeh != null) {
				MedicalWorker mw = medicalWorkerService.getMedicalWorker(
						aeh.getMedicalWorker().getId());
				aeh.setMedicalWorker(mw);
			}
			appointment.setAppointmentExecutionHistory(aeh);
		}
		
		return appointments;
	}

	/**
	 * Load appointment by id. And load its dependent entities.
	 * @param appointmentId id of appointment to load
	 * @return {@code Appointment} appointment entity for given id
	 * @throws DaoException 
	 */
	public Appointment getAppointment(int appointmentId) throws DaoException {
		Appointment appointment = appointmentDao.find(appointmentId);
		
		AppointmentType at = appointmentTypeDao.find(
				appointment.getAppointmentType().getId());
		appointment.setAppointmentType(at);
		
		Reception reception = receptionDao.find(
				appointment.getReception().getId());
		appointment.setReception(reception);
		
		Disease disease = diseaseDao.find(reception.getDisease().getId());
		reception.setDisease(disease);
		
		return appointment;
	}

	/**
	 * Save information about appointment execution in DB 
	 * @param appExecHist
	 * @throws DaoException 
	 */
	public void doAppointment(AppointmentExecutionHistory appExecHist) throws DaoException {
		appointmentExecutionHistoryDao.create(appExecHist);
	}
	
	/**
	 * Check if given user is allowed to execute given appointment.
	 * @param appointment appointment to check
	 * @param user user to check
	 * @return true, if user is allowed to execute appointment; 
	 * false - otherwise.
	 */
	public boolean isAppointmentAllowedToUser(Appointment appointment, User user) {

		String[] allowedRoles = getAppointmentAllowedRoles(appointment);
		
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("UserId=" + user.getId() + " role: " + user.getRole().getName().toString());
			LOGGER.info("AppointmentId=" + appointment.getId() + " allowed roles: " + Arrays.toString(allowedRoles));
		}
		
		for (String role : allowedRoles) {
			if (role.equalsIgnoreCase(user.getRole().getName().toString())) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Return array of allowed roles for given appointment.
	 * @param appointment
	 * @return {@code String[]} allowed roles
	 */
	public String[] getAppointmentAllowedRoles(Appointment appointment) {
		
		return AppointmentRolesConfig.getInstance()
			.getRoles(appointment.getAppointmentType().getType());
		
	}

	/**
	 * Check if current appointment is allowed to execute, i.e. if disease 
	 * related to given appointment is not closed.
	 * @param appointment
	 * @return
	 */
	public boolean isAppointmentOpened(Appointment appointment) {
		return appointment.getReception().getDisease().getClosed() == null;
	}

	
	
}
