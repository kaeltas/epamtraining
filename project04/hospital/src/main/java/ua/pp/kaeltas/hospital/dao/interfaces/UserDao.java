package ua.pp.kaeltas.hospital.dao.interfaces;

import ua.pp.kaeltas.hospital.entities.User;
import ua.pp.kaeltas.hospital.exception.DaoException;

/**
 * DAO for accessing User entity.
 *  
 * @author Matsokha Andrey
 */
public interface UserDao extends BaseDao<User> {
	
	/**
	 * Find and return User entity by email.
	 * @param email
	 * @return
	 * @throws DaoException 
	 */
	User findByEmail(String email) throws DaoException;

}
