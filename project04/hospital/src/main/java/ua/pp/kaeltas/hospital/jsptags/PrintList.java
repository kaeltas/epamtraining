package ua.pp.kaeltas.hospital.jsptags;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 * JSP tag. 
 * Print all elements of given list, localized by given resource bundle and language,
 * each on a new line, e.g. separated by {@code <br>} tag.
 * 
 * @author Matsokha Andrey
 */
public class PrintList extends SimpleTagSupport {

	private static final String NEW_LINE = "<br>";

	private static final String LANGUAGE_SEPARATOR = "_";

	/**
	 * List to print it's elements
	 */
	private List<?> list;
	
	/**
	 * Path to resource bundle
	 */
	private String basename;
	
	/**
	 * Language for localization. Standard language for Locale.  
	 */
	private String lang;
	
	/**
	 * Get resource bundle for given {@code basename} and {@code language},
	 * localize and print each element of {@code list} separated by NEW_LINE. 
	 */
	@Override
	public void doTag() throws JspException, IOException {

		if (list != null) {
			if (basename != null) {
				ResourceBundle resources;
				if (lang != null) {
					String language = lang.split(LANGUAGE_SEPARATOR)[0];
					resources = ResourceBundle.getBundle(basename, new Locale(language));
				} else {
					resources = ResourceBundle.getBundle(basename);
				}
				
				for (Object object : list) {
					String str = resources.getString(object.toString());
					getJspContext().getOut().print(str + NEW_LINE);
				}
			} else {
				for (Object object : list) {
					String str = object.toString();
					getJspContext().getOut().print(str + NEW_LINE);
				}
			}
		}
		
		
		
	}

	public void setList(List<?> list) {
		this.list = list;
	}

	public void setbasename(String basename) {
		this.basename = basename;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
	
	

}
