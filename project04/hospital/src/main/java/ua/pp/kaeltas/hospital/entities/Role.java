package ua.pp.kaeltas.hospital.entities;

/**
 * Role entity.
 * It holds information about one row from DB table Role.
 *  
 * @author Matsokha Andrey
 */
public class Role {

	private int id;
	private String name;
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
