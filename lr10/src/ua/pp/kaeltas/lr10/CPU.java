package ua.pp.kaeltas.lr10;


/**
 * CPU thread. Consumer.
 * Takes process from one of 2 queues and executes it.
 * 
 * @author Matsokha Andrey
 *
 */
public class CPU implements Runnable {

	private CPUQueue queue1;
	private CPUQueue queue2;
	
	private int maxQueue2Bound;
	
	/**
	 * Flag, if true than cpu should shutdown after executing all processes 
	 * from both queues
	 */
	private boolean shutdown;
	
	private Statistic stat = new Statistic();

	public CPU(CPUQueue queue1, CPUQueue queue2, int maxQueue2Bound) {
		this.queue1 = queue1;
		this.queue2 = queue2;
		this.maxQueue2Bound = maxQueue2Bound;
	}

	public Statistic getStatistic() {
		return stat;
	}
	
	@Override
	public void run() {
		
		while (true) {
			try {
				//take process from queue
				Process p = null;
				
				if (Thread.currentThread().isInterrupted() || shutdown) {
					if (queue1.isEmpty() && queue2.isEmpty()) {
						break;
					}
				}
				//if queue2 is greater than bound 
				//or if queue1 isEmpty - cares the case when cpu have executed all
				// processes from queue1 and than it should execute all processes 
				// in queue2
				if (queue2.size() > maxQueue2Bound || (queue1.isEmpty() && !queue2.isEmpty())) {
					p = queue2.pop();
					stat.incCountProcessedFromQueue2();;
					stat.maxQueue2Size(queue2.size());
					
					System.out.printf("TAKE Process from Queue 2 = %1$5d\n",
							p.getProcessTime());
				} else {
					p = queue1.pop();
					stat.incCountProcessedFromQueue1();;
					System.out.printf("TAKE Process from Queue 1 = %1$5d\n",
							p.getProcessTime());
				}
				stat.incTotalCountProcesses();
				
				System.out.printf("Queue1 size = %1$3d  Queue2 size = %2$3d \n",
						queue1.size(), queue2.size());
				
				p.doProcess();
			
			} catch (InterruptedException e) {
				shutdown = true;
			}			
		}
		
	}
	
	
	
	
}
