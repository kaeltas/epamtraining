package ua.pp.kaeltas.lr10;

import java.util.LinkedList;

/**
 * Thread-safe cpu queue
 *  
 * @author Matsokha Andrey
 */
public class CPUQueue {
	
	private LinkedList<Process> processes = new LinkedList<>();
	
	/**
	 * Maximum size of the Queue. If equals to 0, then queue is endless. 
	 */
	private int maxSize = 0;
	
	public CPUQueue(int maxSize) {
		this.maxSize = maxSize;
	}

	public synchronized void add(Process p) throws InterruptedException {
		
		while(maxSize > 0 && processes.size() > maxSize) {
			wait();
		}
		
		processes.add(p);
		notifyAll();
		
	}
	
	public synchronized Process pop() throws InterruptedException {
		
		while(processes.isEmpty()) {
			wait();
		}
		
		Process p = processes.removeFirst();
		notifyAll();
		
		return p;
		
	}
	
	public synchronized boolean isFull() {
		return maxSize > 0 && processes.size() >= maxSize;
	}
	
	public synchronized boolean isEmpty() {
		return processes.isEmpty();
	}
	
	public synchronized int size() {
		return processes.size();
	}
	
	
}
