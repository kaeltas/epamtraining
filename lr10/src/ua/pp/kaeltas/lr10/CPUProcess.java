package ua.pp.kaeltas.lr10;

import java.util.Random;

/**
 * CPUProcess thread. Producer.
 * Generates new process with random execution time in random intervals and
 * puts it into one of two queues.
 * 
 * @author Matsokha Andrey
 */
public class CPUProcess implements Runnable {

	private int minProcessGenerationDelay;
	private int maxProcessGenerationDelay;
	
	private int minProcessExecutionTime;
	private int maxProcessExecutionTime;
	
	private Random random = new Random();
	
	private int countProcessesToExecute; 
	
	private CPUQueue queue1;
	private CPUQueue queue2;

	public CPUProcess(CPUQueue queue1, CPUQueue queue2,
			int minProcessGenerationDelay,	int maxProcessGenerationDelay, 
			int minProcessExecutionTime, int maxProcessExecutionTime, 
			int countProcessesToExecute) {
		this.minProcessGenerationDelay = minProcessGenerationDelay;
		this.maxProcessGenerationDelay = maxProcessGenerationDelay;
		this.minProcessExecutionTime = minProcessExecutionTime;
		this.maxProcessExecutionTime = maxProcessExecutionTime;
		this.countProcessesToExecute = countProcessesToExecute;
		this.queue1 = queue1;
		this.queue2 = queue2;
	}




	@Override
	public void run() {
		while(countProcessesToExecute-- > 0) {
			int processGenerationDelay = createGenerationDelay();
			try {
				Thread.sleep(processGenerationDelay);
			
				int processExecutionDelay = createExecutionDelay();
				Process p = new Process(processExecutionDelay);
				
				//add to queue
				if (queue1.isFull()) {
					queue2.add(p);
					System.out.printf("	ADD Queue 2 process = %1$3d\n", 
							p.getProcessTime());
				} else {
					queue1.add(p);
					System.out.printf("	ADD Queue 1 process = %1$3d\n", 
							p.getProcessTime());
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}




	private int createGenerationDelay() {
		int processGenerationDelay = getRandomBounded( 
				minProcessGenerationDelay, maxProcessGenerationDelay);
		return processGenerationDelay;
	}




	private int getRandomBounded(int minBound, int maxBound) {
		int processGenerationDelay = random.nextInt(
				maxBound-minBound)
				+ minBound;
		return processGenerationDelay;
	}
	
	private int createExecutionDelay() {
		int processGenerationDelay = getRandomBounded(
				minProcessExecutionTime, maxProcessExecutionTime);
		return processGenerationDelay;
	}

}
