package ua.pp.kaeltas.lr10;

/**
 * Вариант 28
 * Программа моделирует обслуживание одного потока процессов 
 * одним центральным процессором компьютера с двумя очередями. 
 * Если размер первой очереди превышает заданный, процесс помещается 
 * во вторую очередь (буфер переполнения), размер которой не ограничен. 
 * Процессор берет на обработку процесс из второй очереди только   
 * в том случае, если размер этой очереди превысит некоторую заданную величину. 
 * Определить максимальный размер второй очереди и процент процессов, 
 * которые были выбраны на обработку из второй очереди.
 * 
 * @author Matsokha Andrey
 */
public class Runner {

	private static final int COUNT_PROCESSES_TO_EXECUTE = 200;
	private static final int MAX_PROCESS_EXECUTION_TIME = 40;
	private static final int MIN_PROCESS_EXECUTION_TIME = 20;
	private static final int MAX_PROCESS_GENERATION_DELAY = 20;
	private static final int MIN_PROCESS_GENERATION_DELAY = 10;
	private static final int MAX_QUEUE_2_BOUND = 25;
	private static final int QUEUE_INFINITE_SIZE = 0;
	private static final int MAX_QUEUE_1_SIZE = 50;

	public static void main(String[] args) throws InterruptedException {
		
		Runner r = new Runner();
		r.execute();
		
	}

	private void execute() throws InterruptedException {
		CPUQueue queue1 = new CPUQueue(MAX_QUEUE_1_SIZE);
		CPUQueue queue2 = new CPUQueue(QUEUE_INFINITE_SIZE);
		
		CPU cpu = new CPU(queue1, queue2, MAX_QUEUE_2_BOUND);
		CPU cpu2 = new CPU(queue1, queue2, MAX_QUEUE_2_BOUND);
		
		CPUProcess cpuProcess = new CPUProcess(queue1, queue2, 
				MIN_PROCESS_GENERATION_DELAY, MAX_PROCESS_GENERATION_DELAY, 
				MIN_PROCESS_EXECUTION_TIME, MAX_PROCESS_EXECUTION_TIME,
				COUNT_PROCESSES_TO_EXECUTE);
		
		Thread thCpu = new Thread(cpu);
		thCpu.start();
//		Thread thCpu2 = new Thread(cpu2);
//		thCpu2.start();
		Thread thCpuProcess = new Thread(cpuProcess);
		thCpuProcess.start();		
		
		thCpuProcess.join();
		thCpu.interrupt();
		thCpu.join();
//		thCpu2.interrupt();
//		thCpu2.join();
		
		
		Statistic statistic = cpu.getStatistic();
		System.out.println("CPU1 " + statistic);
//		Statistic statistic2 = cpu2.getStatistic();
//		System.out.println("CPU2 " + statistic2);
	}
	
}
