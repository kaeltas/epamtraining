package ua.pp.kaeltas.lr10;

public class Statistic {

	private int totalCountProcesses;
	private int maxQueue2Size;
	private int countProcessedFromQueue1;
	private int countProcessedFromQueue2;

	public int getTotalCountProcesses() {
		return totalCountProcesses;
	}

	public void incTotalCountProcesses() {
		totalCountProcesses++;
	}

	public int getMaxQueue2Size() {
		return maxQueue2Size;
	}

	public void maxQueue2Size(int maxQueue2Size) {
		if (maxQueue2Size > this.maxQueue2Size) {
			this.maxQueue2Size = maxQueue2Size;
		}
	}

	public int getCountProcessedFromQueue2() {
		return countProcessedFromQueue2;
	}

	public void incCountProcessedFromQueue2() {
		countProcessedFromQueue2++;
	}
	
	public int getCountProcessedFromQueue1() {
		return countProcessedFromQueue1;
	}
	
	public void incCountProcessedFromQueue1() {
		countProcessedFromQueue1++;
	}

	private float percentProcessedFromQueue2() {
		return (float)countProcessedFromQueue2/totalCountProcesses*100;
	}

	@Override
	public String toString() {
		return "Statistic [totalCountProcesses=" + totalCountProcesses
				+ ", maxQueue2Size=" + maxQueue2Size
				+ ", countProcessedFromQueue1=" + countProcessedFromQueue1
				+ ", countProcessedFromQueue2=" + countProcessedFromQueue2
				+ ", percentProcessedFromQueue2=" + percentProcessedFromQueue2()
				+ "]";
	}

	
	
}
