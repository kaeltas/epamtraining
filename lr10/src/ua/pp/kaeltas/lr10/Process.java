package ua.pp.kaeltas.lr10;

public class Process {
	
	private int processTime;

	public Process(int processTime) {
		this.processTime = processTime;
	}

	public int getProcessTime() {
		return processTime;
	}
	
	public void doProcess() throws InterruptedException {
		Thread.sleep(processTime);
	}
		
}
