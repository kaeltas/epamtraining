package labr03.v03;

/**
 * В двузначном целом числе поменяйте цифры местами, 
 * найдите его квадратный корень и ближайшее к корню целое.
 * 
 * @author Matsokha Andrey
 */
public class Labr03v03 {
    
    private final int number;
    private int swappedNumber;
    private double sqrt;
    private int nearestToSqrt;

    /**
     * Initialize 2-digit {@code number}.
     * 
     * @exception throw IllegalArgumentException 
     * if {@code number < 10 || number > 99} 
     * @param number 
     */
    public Labr03v03(int number) {
        if (number < 10 || number > 99) {
            throw new IllegalArgumentException("Allowed only numbers "
                    + "from 10 to 99");
        }
        this.number = number;
    }
    
    /**
     * Swap first and second digit in {@code number}.
     */
    public void swapNumber() {
        int firstDigit = number%10;
        int secondDigit = number/10;
        
        swappedNumber = firstDigit*10 + secondDigit;
    }
    
    /**
     * Calc sqrt of {@code swappedNumber} and nearest int to sqrt.
     */
    public void calcSqrtAndNearest() {
        if (swappedNumber == 0) {
            swapNumber();
        }
        sqrt = Math.sqrt(swappedNumber);
        nearestToSqrt = (int)Math.round(sqrt);
    }

    public int getNumber() {
        return number;
    }

    public double getSqrt() {
        return sqrt;
    }

    public int getNearestToSqrt() {
        return nearestToSqrt;
    }

    public int getSwappedNumber() {
        return swappedNumber;
    }

    @Override
    public String toString() {
        return "Labr03v03{" + "number=" + number + ", swappedNumber=" + swappedNumber + ", sqrt=" + sqrt + ", nearestToSqrt=" + nearestToSqrt + '}';
    }
    
}
