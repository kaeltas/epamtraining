package labr03.v03;

/**
 *
 * @author Matsokha Andrey
 */
public class Labr3v03Test {

    public static void main(String[] args) {
        
        Labr3v03Test lab = new Labr3v03Test();
        
        lab.swapAndCalc(52);
        lab.swapAndCalc(81);
        lab.swapAndCalc(22);
        lab.swapAndCalc(5);
        
    }
    
    public void swapAndCalc(int number) {
        try {
            Labr03v03 lab = new Labr03v03(number);
            lab.swapNumber();
            lab.calcSqrtAndNearest();
            System.out.println(lab);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
    
}
