package labr03.v20;

/**
 * В четырехзначном числе цифры циклически сдвинуть на 1 позицию вправо. 
 * Найти его натуральный логарифм.
 * @author Matsokha Andrey
 */
public class Labr03v20 {
    private final int number;
    private int shiftedNumber;
    private double lognShiftedNumber;

    /**
     * Initialize 4-digit {@code number}.
     * 
     * @exception throw IllegalArgumentException 
     * if {@code number < 1000 || number > 9999}
     * 
     * @param number 
     */
    public Labr03v20(int number) {
        if (number < 1000 || number > 9999) {
            throw new IllegalArgumentException("Allowed only numbers "
                    + "from 1000 to 9999");
        }
        this.number = number;
    }
    
    /**
     * Shift digits cyclically on 1 position to right.
     */
    public void shiftNumber() {
        int firstDigit = number%10;
        int last3Digits = number/10;
        shiftedNumber = firstDigit*1000 + last3Digits;
    }
    
    /**
     * Calc natural log of {@code shiftedNumber}.
     */
    public void calcLogn() {
        if (shiftedNumber == 0) {
            shiftNumber();
        }
        lognShiftedNumber = Math.log(shiftedNumber);
    }

    public int getNumber() {
        return number;
    }

    public int getShiftedNumber() {
        return shiftedNumber;
    }

    public double getLognShiftedNumber() {
        return lognShiftedNumber;
    }

    @Override
    public String toString() {
        return "Labr03v20{" + "number=" + number + ", shiftedNumber=" + shiftedNumber + ", lognShiftedNumber=" + lognShiftedNumber + '}';
    }
    
}
