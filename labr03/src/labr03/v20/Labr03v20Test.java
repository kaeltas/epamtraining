package labr03.v20;

/**
 *
 * @author Matsokha Andrey
 */
public class Labr03v20Test {
    
    public static void main(String[] args) {
        Labr03v20Test labTest = new Labr03v20Test();
        labTest.shiftAndCalcLog(500);
        labTest.shiftAndCalcLog(1234);
        labTest.shiftAndCalcLog(100500);
        labTest.shiftAndCalcLog(5678);
    }
    
    public void shiftAndCalcLog(int number) {
        try {
            Labr03v20 lab = new Labr03v20(number);
            lab.shiftNumber();
            lab.calcLogn();
            System.out.println(lab);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
    
}
