package labr03.v05;

/**
 * Пусть даны целые длины сторон треугольника. 
 * Найдите его площадь и полупериметр.
 * 
 * @author Matsokha Andrey
 */
public class Labr03v05 {
    
    //sides of Triangle
    private final int a;
    private final int b;
    private final int c;
    private double halfPerimeter;
    private double square;

    /**
     * Initialize triangle by 3 sides.
     * 
     * @exception throw IllegalArgumentException if can't build triangle
     * with given sides
     * 
     * @param a
     * @param b
     * @param c 
     */
    public Labr03v05(int a, int b, int c) {
        if (a < 0 || b < 0 || c < 0 
                || (a + b <= c) || (a + c <= b) || (b + c <= a) ) {
            throw new IllegalArgumentException("Wrong triangle!");
        }
        
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    /**
     * Calc square of triangle using length of 3 sides.
     * Geron formula: S = sqrt(p*(p-a)*(p-b)*(p-c)), where p is halfPerimeter.
     */
    public void calcSquare() {
        if (halfPerimeter == 0) {
            calcHalpPerimeter();
        }
        square = Math.sqrt(halfPerimeter*(halfPerimeter-a)
                *(halfPerimeter-b)*(halfPerimeter-c));
    }
    
    /**
     * Calc halfperimeter of triangle.
     */
    public void calcHalpPerimeter() {
        halfPerimeter = (double)(a + b + c) / 2;
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }

    public int getC() {
        return c;
    }

    public double getHalfPerimeter() {
        return halfPerimeter;
    }

    public double getSquare() {
        return square;
    }
    
    @Override
    public String toString() {
        return "Labr03v05{" + "a=" + a + ", b=" + b + ", c=" + c + ", halfPerimeter=" + halfPerimeter + ", square=" + square + '}';
    }
    
}
