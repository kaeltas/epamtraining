package labr03.v05;

/**
 *
 * @author Matsokha Andrey
 */
public class Labr03v05Test {
    
    public static void main(String[] args) {
        
        Labr03v05Test labTest = new Labr03v05Test();
        
        labTest.calcTriangle(2, 3, 4);
        labTest.calcTriangle(18, 30, 45);
        labTest.calcTriangle(1, 2, 3);
        
    }
    
    public void calcTriangle(int a, int b, int c) {
        try {
            Labr03v05 lab = new Labr03v05(a, b, c);
            lab.calcSquare();
            System.out.println(lab);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}
