package ua.pp.kaeltas.institute.institutes;

import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.institute.StudentsQueue;
import ua.pp.kaeltas.institute.student.Student;
import ua.pp.kaeltas.institute.student.StudentBiol;

public class InstituteBiology implements Runnable {

	private static final int NUMBER_OF_STUDENTS_TO_TAKE = 5;
	private List<Student> students = new LinkedList<>();
	private StudentsQueue studentsQueue;
	
	public InstituteBiology(StudentsQueue studentsQueue) {
		this.studentsQueue = studentsQueue;
	}
	
	@Override
	public void run() {
		
		while (true) {
			synchronized (studentsQueue) {

				try {
					while (studentsQueue.getInstituteSelector() != Institutes.BIOLOGY) {
						studentsQueue.wait();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				boolean shutdown = false;
				
				for (int i = 0; i < NUMBER_OF_STUDENTS_TO_TAKE; i++) {
					Student student = studentsQueue.peekStudent();
					System.out.println("BIOL peek " + student);
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (student == null) {
						shutdown = true;
						break;
					} else {
						if (student.getClass() == StudentBiol.class) {
							studentsQueue.popStudent();
							students.add(student);
						} else {
							break;
						}
					}
				}
				studentsQueue.nextInstitute();
				studentsQueue.notifyAll();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (shutdown) {
					return;
				}

			}
		}
		
	}
	
	public List<Student> getStudents() {
		return students;
	}

}
