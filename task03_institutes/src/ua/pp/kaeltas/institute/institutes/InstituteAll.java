package ua.pp.kaeltas.institute.institutes;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import ua.pp.kaeltas.institute.StudentsQueue;
import ua.pp.kaeltas.institute.student.Student;

public class InstituteAll implements Runnable {

	private static final int NUMBER_OF_STUDENTS_TO_TAKE = 5;
	private List<Student> students = new LinkedList<>();
	private StudentsQueue studentsQueue;
	
	public InstituteAll(StudentsQueue studentsQueue) {
		this.studentsQueue = studentsQueue;
	}
	
	@Override
	public void run() {
		
		while (true) {
			synchronized (studentsQueue) {

				try {
					while (studentsQueue.getInstituteSelector() != Institutes.ALL) {
						studentsQueue.wait();
					}
					//System.out.println("ALL");
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				boolean shutdown = false;
				Random rand = new Random();
				for (int i = 0; i < rand.nextInt(NUMBER_OF_STUDENTS_TO_TAKE); i++) {
					Student student = studentsQueue.peekStudent();
					System.out.println("ALL peek " + student);
					try {
						Thread.sleep(20);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (student != null) {
						studentsQueue.popStudent();
						students.add(student);
					} else {
						shutdown = true;
					}
				}
				studentsQueue.nextInstitute();
				studentsQueue.notifyAll();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (shutdown) {
					return;
				}
			}
		}
		
	}
	
	public List<Student> getStudents() {
		return students;
	}

}
