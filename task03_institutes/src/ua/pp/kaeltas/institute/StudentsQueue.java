package ua.pp.kaeltas.institute;

import java.util.LinkedList;
import java.util.List;

import ua.pp.kaeltas.institute.institutes.Institutes;
import ua.pp.kaeltas.institute.student.Student;
import ua.pp.kaeltas.institute.student.StudentsHolder;

public class StudentsQueue implements Runnable {

	private static final int QUEUE_MAX_SIZE = 50;
	private static final int QUEUE_MIN_SIZE = 25;
	private Institutes instituteSelector = Institutes.MATH;
	private List<Student> studentsList = new LinkedList<>();
	private Object getMoreStudents = new Object();
	
	private StudentsHolder studentsHolder;
	
	public StudentsQueue(StudentsHolder studentsHolder) {
		this.studentsHolder = studentsHolder;
	}

	@Override
	public void run() {
		
		while (true) {
			//System.out.println("queue");
			synchronized (getMoreStudents) {
				try {
					getMoreStudents.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			synchronized (this) {
				if (studentsList.size() < QUEUE_MIN_SIZE) {
					while (studentsList.size() < QUEUE_MAX_SIZE) {
						if (studentsHolder.isEmpty()) {
							this.notifyAll();
							return;
						}
						Student randomStudent = studentsHolder.getRandomStudent();
						studentsList.add(randomStudent);
					}
				}
				this.notifyAll();
			}
			
		}
	}
	
	public synchronized Student popStudent() {

		if (studentsList.size() > 0) {
			return studentsList.remove(0);
		} else {
			return null;
		}
	}
	
	public synchronized Student peekStudent() {
		//System.out.println("QUEUE peek");
		
		if (studentsList.size() < QUEUE_MIN_SIZE) {
			if (!studentsHolder.isEmpty()) {
				synchronized (getMoreStudents) {
					getMoreStudents.notify();
				}
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("	QUEUE size " + studentsList.size());
		
		if (studentsList.size() > 0) {
			return studentsList.get(0);
		} else {
			return null;
		}
	}

	public synchronized Institutes getInstituteSelector() {
		return instituteSelector;
	}

	public synchronized void nextInstitute() {
		switch (instituteSelector) {
			case MATH: {
				instituteSelector = Institutes.ALL;
				break;
			}
			case ALL: {
				instituteSelector = Institutes.BIOLOGY;
				break;
			}
			case BIOLOGY: {
				instituteSelector = Institutes.MATH;
				break;
			}
		}
		
	}

	
	
}
