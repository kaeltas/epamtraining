package ua.pp.kaeltas.institute;

import ua.pp.kaeltas.institute.institutes.InstituteAll;
import ua.pp.kaeltas.institute.institutes.InstituteBiology;
import ua.pp.kaeltas.institute.institutes.InstituteMath;
import ua.pp.kaeltas.institute.student.StudentsHolder;

public class Runner {

	
	public static void main(String[] args) throws InterruptedException {
		
		StudentsHolder studentsHolder = new StudentsHolder();
		studentsHolder.fillStudents();
		
//		for (int i = 0; i < 10; i++) {
//			System.out.println(studentsHolder.getRandomStudent());
//		}
		
		StudentsQueue studentsQueue = new StudentsQueue(studentsHolder);
		InstituteMath instituteMath = new InstituteMath(studentsQueue);
		InstituteBiology instituteBiol = new InstituteBiology(studentsQueue);
		InstituteAll instituteAll = new InstituteAll(studentsQueue);
		
		Thread th1 = new Thread(studentsQueue);
		Thread th2 = new Thread(instituteMath);
		Thread th3 = new Thread(instituteBiol);
		Thread th4 = new Thread(instituteAll);
		
		th1.start();
		Thread.sleep(200);
		th2.start();
		th3.start();
		th4.start();
		
		
		try {
			th1.join();
			th2.join();
			th3.join();
			th4.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("----MATH " + instituteMath.getStudents().size() + " -----");
		System.out.println(instituteMath.getStudents());
		System.out.println("----BIOL " + instituteBiol.getStudents().size() + " -----");
		System.out.println(instituteBiol.getStudents());
		System.out.println("----ALL " + instituteAll.getStudents().size() + " ------");
		System.out.println(instituteAll.getStudents());
		
		int totalStudents = instituteMath.getStudents().size()
				+ instituteBiol.getStudents().size()
				+ instituteAll.getStudents().size();
		System.out.println("Total = " + totalStudents);
		
	}
	
}
