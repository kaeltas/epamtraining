package ua.pp.kaeltas.institute.student;

public class StudentMath extends Student {
	public final int mathGrade;

	public StudentMath(int id, int mathGrade) {
		super(id);
		this.mathGrade = mathGrade;
	}

	@Override
	public String toString() {
		return "StudentMath [id=" + super.id +" mathGrade=" + mathGrade + "]";
	}
	
}
