package ua.pp.kaeltas.institute.student;

public class StudentBiol extends Student {
	public final int biolGrade;

	public StudentBiol(int id, int biolGrade) {
		super(id);
		this.biolGrade = biolGrade;
	}

	@Override
	public String toString() {
		return "StudentBiol [id=" + super.id +" biolGrade=" + biolGrade + "]";
	}
	
}
