package ua.pp.kaeltas.institute.student;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class StudentsHolder {
	
	private List<Student> studentsMath = new LinkedList<>(); 
	private List<Student> studentsBiol = new LinkedList<>(); 

	public void fillStudents() {
		StudentsFiller sf = new StudentsFiller();
		sf.fillBiol(studentsBiol, 200);
		sf.fillMath(studentsMath, 250);
	}
	
	public Student getRandomStudent() {
		Random random = new Random();
		if (random.nextInt(100) % 2 == 0) {
			if (studentsBiol.size() > 0) {
				return studentsBiol.remove(0);
			} else if (studentsMath.size() > 0) {
				return studentsMath.remove(0);
			}
		} else {
			if (studentsMath.size() > 0) {
				return studentsMath.remove(0);
			} else if (studentsBiol.size() > 0) {
				return studentsBiol.remove(0);
			}
		}
		
		return null; 
	}
	
	public boolean isEmpty() {
		return studentsBiol.isEmpty() && studentsMath.isEmpty();
	}
}
