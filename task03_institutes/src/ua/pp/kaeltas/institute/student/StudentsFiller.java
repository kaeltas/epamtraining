package ua.pp.kaeltas.institute.student;

import java.util.List;
import java.util.Random;

public class StudentsFiller {
	
	private int idCounter = 0;
	
	public void fillMath(List<Student> students, int count) {
		Random random = new Random();
		for (int i = 0; i < count; i++) {
			StudentMath sm = new StudentMath(idCounter++, random.nextInt(12));
			students.add(sm);
		}
	}
	
	public void fillBiol(List<Student> students, int count) {
		Random random = new Random();
		for (int i = 0; i < count; i++) {
			StudentBiol sb = new StudentBiol(idCounter++, random.nextInt(12));
			students.add(sb);
		}
	}

}
