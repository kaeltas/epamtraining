package binarysearch;

import java.util.Arrays;

/**
 *
 * @author Matsokha Andrey
 */
public class BinarySearch {

    // TESTs
    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 6, 7, 9, 10, 15, 20};
        int idx = BinarySearch.binarySearch(20, arr);
        System.out.println("Return index: " + idx + "  Expected: " + 8);
        idx = BinarySearch.binarySearch(3, arr);
        System.out.println("Return index: " + idx + "  Expected: " + 1);
        idx = BinarySearch.binarySearch(1, arr);
        System.out.println("Return index: " + idx + "  Expected: " + 0);
        idx = BinarySearch.binarySearch(2, arr);
        System.out.println("Return index: " + idx + "  Expected: " + -2);
        idx = BinarySearch.binarySearch(25, arr);
        System.out.println("Return index: " + idx + "  Expected: " + -10);
        
        
//        int[] arr = {1, 3, 4, 6, 7, 9, 10, 15, 20};
//        int idx = BinarySearch.binarySearchIter(20, arr);
//        System.out.println("Return index: " + idx + "  Expected: " + 8);
//        idx = BinarySearch.binarySearchIter(3, arr);
//        System.out.println("Return index: " + idx + "  Expected: " + 1);
//        idx = BinarySearch.binarySearchIter(1, arr);
//        System.out.println("Return index: " + idx + "  Expected: " + 0);
//        idx = BinarySearch.binarySearchIter(2, arr);
//        System.out.println("Return index: " + idx + "  Expected: " + -2);
//        idx = BinarySearch.binarySearchIter(25, arr);
//        System.out.println("Return index: " + idx + "  Expected: " + -10);
        
    }
    
    public static int binarySearch(int elem, int[] arr) {
        return binarySearch(elem, arr, 0, arr.length-1);
    }
    
    public static int binarySearch(int elem, int[] arr, int lo, int hi) {
        if (lo > hi) return -(lo+1);
        
        int mid = lo + (hi-lo) /2;
        int result;
        if (elem > arr[mid]) {
            result = binarySearch(elem, arr, mid+1, hi);
        } else if (elem < arr[mid]) {
            result = binarySearch(elem, arr, lo, mid-1);
        } else {
            result = mid;
        }

        return result;
    }
    
    public static int binarySearchIter(int elem, int[] arr) {
        return binarySearchIter(elem, arr, 0, arr.length-1);
    }
    
    public static int binarySearchIter(int elem, int[] arr, int lo, int hi) {
        int low = lo;
        int high = hi;
        
        int mid = -1;
        while(low <= high) {
            mid = low + (high-low)/2;
            
            if (arr[mid] == elem) break;
            if (elem < arr[mid]) {
                high = mid-1;
            }
            if (elem > arr[mid]) {
                low = mid+1;
            } 
        }
        
        if (low > high) {
            return -(low+1);
        }
        
        return mid;
    }
    
}
