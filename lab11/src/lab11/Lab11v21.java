package lab11;

/**
 * Дано слово. Если его длина нечетная, то удалить среднюю букву, 
 * в противном случае – две средних буквы.
 * 
 * @author Matsokha Andrey
 */
public class Lab11v21 {
   
    /**
     * Delete middle letter in {@code word}.
     * If {@code word.length()} is even - then delete two middle letters,
     * if {@code word.length()} is odd - delete one middle letter.
     * 
     * @param word
     * @return 
     */
    public String deleteMidLetter(String word) {
        if (word.length() == 0) return word;
        
        StringBuilder sb = new StringBuilder(word);
        
        if (word.length() % 2 == 0) {
            sb.delete(word.length()/2-1, word.length()/2+1);
        } else {
            sb.delete(word.length()/2, word.length()/2+1);
        }
        
        return sb.toString();
    }
    
    public static void main(String[] args) {
        
        Lab11v21 lab11v21 = new Lab11v21();
        
        String word = "Abba";
        System.out.println(word);
        System.out.println("Method: " + lab11v21.deleteMidLetter(word));
        System.out.println("Expected: " + "Aa");
        
        System.out.println("");
        word = "Arbwa";
        System.out.println(word);
        System.out.println("Method: " + lab11v21.deleteMidLetter(word));
        System.out.println("Expected: " + "Arwa");
        
        word = "";
        System.out.println(word);
        System.out.println("Method: " + lab11v21.deleteMidLetter(word));
        System.out.println("Expected: " + "");
        
        
    }
    
}
