package lab11;

/**
 * Дано слово. Переставить его последнюю букву на место первой.
 * 
 * @author Matsokha Andrey
 */
public class Lab11v28 {
    
    /**
     * Swap letters of {@code word} in positions {@code idxFrom} and 
     * {@code idxTo}.
     * 
     * @param word
     * @param idxFrom
     * @param idxTo
     * @return 
     */
    public String swapLetters(String word, int idxFrom, int idxTo) {
        if (idxFrom < 0 || idxFrom > word.length()-1
                || idxTo < 0 || idxTo > word.length()-1) {
            throw new IllegalArgumentException("Wrong parameters");
        }
        
        StringBuilder sb = new StringBuilder(word);
        
        char chFrom = word.charAt(idxFrom);
        char chTo = word.charAt(idxTo);
        
        sb.setCharAt(idxFrom, chTo);
        sb.setCharAt(idxTo, chFrom);
        
        return sb.toString();
    }
    
    
    /**
     * Swap first and last letters in {@code word}.
     * 
     * @param word
     * @return 
     */
    public String swapFirstAndLastLetters(String word) {
        return swapLetters(word, 0, word.length()-1);
    }
    
    public static void main(String[] args) {
        Lab11v28 lab11v28 = new Lab11v28();

        System.out.println("TEST SWAP FIRST AND LAST LETTERS");
        String word = "parameter";
        System.out.println(word);
        System.out.println("Method: " + lab11v28.swapFirstAndLastLetters(word));
        System.out.println("Expected: " + "rarametep");
        System.out.println("");
        
        word = "z";
        System.out.println(word);
        System.out.println("Method: " + lab11v28.swapFirstAndLastLetters(word));
        System.out.println("Expected: " + "z");
        System.out.println("");
        
        
        System.out.println("TEST SWAP idxFrom AND idxTo LETTERS");
        word = "parameter";
        System.out.println(word + ", " + 2 + ", " + 4);
        System.out.println("Method: " + lab11v28.swapLetters(word, 2, 4));
        System.out.println("Expected: " + "pamareter");
        System.out.println("");
        
        word = "q";
        System.out.println(word + ", " + 0 + ", " + 0);
        System.out.println("Method: " + lab11v28.swapLetters(word, 0, 0));
        System.out.println("Expected: " + "q");
        System.out.println("");
        
        word = "wl";
        System.out.println(word + ", " + 2 + ", " + 4);
        try {
            System.out.println("Method: " + lab11v28.swapLetters(word, 2, 4));
        } catch (IllegalArgumentException e) {
            System.out.println("Method: " + "exception");
        }
        System.out.println("Expected: " + "exception");
        System.out.println("");
        
    }
    
}
