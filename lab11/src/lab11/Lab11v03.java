package lab11;

/**
 *
 * @author Matsokha Andrey
 */
public class Lab11v03 {
    
    /**
     * Prints to System.out all characters equal to {@code letter} in 
     * {@code sentence}.
     * 
     * @param sentence
     * @param letter 
     */
    public void printLettersOfSentence(String sentence, char letter) {
        int idxFrom = 0;
        while ((idxFrom = sentence.indexOf(letter, idxFrom+1)) != -1) {
            System.out.print(letter);
        }
        System.out.println("");
    }
    
    public static void main(String[] args) {
        
        Lab11v03 lab11v03 = new Lab11v03();
        System.out.print("Method: ");
        lab11v03.printLettersOfSentence("Привет! Вы выиграли приз!", 'и');
        System.out.println("Expected: " + "ииии");

        System.out.println("");
        System.out.print("Method: ");
        lab11v03.printLettersOfSentence("", 'и');
        System.out.println("Expected: " + "");
        
        System.out.println("");
        System.out.print("Method: ");
        lab11v03.printLettersOfSentence("Ура!", 'и');
        System.out.println("Expected: " + "");
    }
    
}
