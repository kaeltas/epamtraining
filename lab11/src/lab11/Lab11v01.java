package lab11;

/**
 *
 * Дано слово. Добавить к нему в начале и в конце столько звездочек,
 * сколько букв в этом слове.
 * 
 * @author Matsokha Andrey
 */
public class Lab11v01 {
    
    /**
     * Add asterisks to begin and to end of the word, 
     * count of asterisks equals to {@code word.length()}.
     * 
     * @param word
     * @return 
     */
    public String addAsterisks(String word) {
        StringBuilder sb = new StringBuilder();
        
        for (int i = 0; i < word.length(); i++) {
            sb.append("**");
        }
        sb.insert(word.length(), word);
        
        return sb.toString();
    }
    
    public static void main(String[] args) {
        
        Lab11v01 lab11v01 = new Lab11v01();
        
        System.out.println(lab11v01.addAsterisks("test"));
        System.out.println("Expected: " + "****test****");
        
        System.out.println(lab11v01.addAsterisks(""));
        System.out.println("Expected: " + "");
        
        System.out.println(lab11v01.addAsterisks("a"));
        System.out.println("Expected: " + "*a*");
        
    }
    
    
}
