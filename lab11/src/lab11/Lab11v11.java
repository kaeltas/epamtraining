package lab11;

/**
 * Дано слово. Проверить, является ли оно «перевертышем».
 * 
 * @author Matsokha Andrey
 */
public class Lab11v11 {
    
    /**
     * Check if {@code word} is a pallindrome.
     * 
     * @param word
     * @return 
     */
    public boolean isPallindrome(String word) {
       for (int i = 0; i < word.length()/2; i++) {
           if (word.charAt(i) != word.charAt(word.length()-i-1)) {
               return false;
           }
       }
       return true;
    } 
   
    public static void main(String[] args) {
        
        Lab11v11 lab11v11 = new Lab11v11();
        
        String word = "ABBA";
        System.out.println("Word: " +word);
        System.out.println("Method: " + lab11v11.isPallindrome(word));
        System.out.println("Expected: " + true);
        
        System.out.println("");
        word = "ABA";
        System.out.println("Word: " +word);
        System.out.println("Method: " + lab11v11.isPallindrome(word));
        System.out.println("Expected: " + true);
        
        System.out.println("");
        word = "BBA";
        System.out.println("Word: " +word);
        System.out.println("Method: " + lab11v11.isPallindrome(word));
        System.out.println("Expected: " + false);
    }
       
    
}
