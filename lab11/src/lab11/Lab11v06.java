package lab11;

/**
 * Дано предложение. Определить долю (в %) букв «а» в нем.
 * 
 * @author Matsokha Andrey
 */
public class Lab11v06 {

    
    /**
     * Return {@code letter} occurrence percent in {@code sentence}.
     * 
     * @param sentence
     * @return 
     */
    public double calcLetterOccurrencePercent(String sentence, char letter) {
        int letterCount = 0;
        int totalCount = 0;
        for (char ch : sentence.toCharArray()) {
            if (Character.isLetterOrDigit(ch)) {
                totalCount++;
                if (ch == letter) {
                    letterCount++;
                }
            }
        }
        
        double percent = (double)letterCount/totalCount*100;
        return percent;
    }
        
    
    public static void main(String[] args) {
        Lab11v06 lab11v06 = new Lab11v06();
        
        double percent = lab11v06.calcLetterOccurrencePercent("Ура", 'а');
        System.out.println("Method: " + percent);
        System.out.println("Expected: " + "33.(3)");
        
        percent = lab11v06.calcLetterOccurrencePercent("Ураа!!", 'а');
        System.out.println("Method: " + percent);
        System.out.println("Expected: " + "50.0");
        
        percent = lab11v06.calcLetterOccurrencePercent("Ур", 'а');
        System.out.println("Method: " + percent);
        System.out.println("Expected: " + "0.0");
        
        
        
    }
    
}
