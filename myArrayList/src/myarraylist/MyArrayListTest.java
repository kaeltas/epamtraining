/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myarraylist;

/**
 *
 * @author Matsokha Andrey
 */
public class MyArrayListTest {
    
    public static void main(String[] args) {
        
        MyArrayList<Integer> list = new MyArrayList<>(2);
        
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);
        list.add(1);
        list.add(2);
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);
        
        list.add(3);
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);
        
        list.add(4);
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);
        
        list.add(5);
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);
        
        list.add(6);
        list.add(7);
        list.set(6, 99);
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);
        
        list.remove(6);
        list.remove(0);
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);
        
        list.remove(2);
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);

        
        
        list.clear();
        System.out.println("Capacity = " + list.getCapacity());
        System.out.println("Size = " + list.size());
        printList(list);
        
        
    }

    private static void printList(MyArrayList<Integer> list) {
        System.out.print("[");
        for (Integer elem : list) {
            System.out.print(elem + " ");
        }
        System.out.println("]");
        System.out.println("");
    }
    
}
