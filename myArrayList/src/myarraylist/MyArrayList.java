package myarraylist;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Array implementation of List.
 * 
 * @author Matsokha Andrey
 */
public class MyArrayList<E> implements Iterable<E> {
    private static final int INITIAL_CAPACITY = 16;
    private E[] elements;
    private int size;
    private final float mulfactor = 1.5f;

    public MyArrayList(int capacity) {
        if (capacity <= 0) {
            capacity = INITIAL_CAPACITY;
        }
        elements = (E[]) new Object[capacity];
        size = 0;
    }

    public MyArrayList() {
        this(INITIAL_CAPACITY);
    }
    
    /**
     * Add element {@code value} to list.
     * If there is no free space in list - it would be automatically expanded.
     * 
     * @param value 
     */
    public void add(E value) {
        if (size >= elements.length) { //resize
            int newCapacity = (int)(elements.length*mulfactor);
            elements = Arrays.copyOf(elements, newCapacity);

        }
        elements[size++] = value;
    }
    
    /**
     * Return element in position {@code index}.
     * If {@code index > size()} - IndexOutOfBoundsException 
     * will be thrown.
     * 
     * @param index
     * @return 
     */
    public E get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        return (E)elements[index];
    }
    
    /**
     * Set elements in position {@code index} to {@code value}.
     * If {@code index > size()} - IndexOutOfBoundsException 
     * will be thrown.
     * 
     * @param index
     * @param value 
     */
    public void set(int index, E value) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        elements[index] = value;
    }

    /**
     * Return current capacity of internal array.
     * 
     * @return 
     */
    public int getCapacity() {
        return elements.length;
    }
    
    /**
     * Return current count of elements in list.
     * 
     * @return 
     */
    public int size() {
        return size;
    }
    
    /**
     * Remove all elements in list.
     * Recreate list with initial capacity.
     * 
     */
    public void clear() {
        elements = (E[]) new Object[INITIAL_CAPACITY];
        size = 0;
    }
    
    /**
     * Remove element at position {@code index} and return it.
     * Also - automatically shrink internal array.
     * If {@code index > size()} - IndexOutOfBoundsException 
     * will be thrown.
     * 
     * @param index
     * @return removed element
     */
    public E remove(int index) {
        if (index >= size || index < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        E result = elements[index];
        //E[] tmp = (E[]) new Object[elements.length];
        //System.arraycopy(elements, 0, tmp, 0, index);
        System.arraycopy(elements, index+1, elements, index, size-index-1);
        //elements = tmp;
        //size--;
        elements[size--] = null;
        
        shrink();
        
        return result;
    }
    
    /**
     * Shrink internal array.
     * If {@code size < capacity()/4} - then resize to {@code capacity()/2}.
     */
    public void shrink() {
        if (size < elements.length/4) {
            elements = Arrays.copyOf(elements, elements.length/2);
        }
    }
    
    /**
     * Iterator over all elements in list in straight order.
     * 
     * @return 
     */
    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            int index = 0;

            @Override
            public boolean hasNext() {
                return index < size;
            }

            @Override
            public E next() {
                return (E)elements[index++];
            }
        };
    }
    
    
    
}
