package ua.pp.kaeltas.congratulation;

public class HappyNewYear {
  
  private final int year;
  
  public HappyNewYear(int year) {
    this.year = year;
  }
  
  /**
   * Prints to standart output NewYear congratulations.
   */
  public void congratulate() {
    System.out.println("Hurray!!! Happy New " + year + " Year!!!");
  }
  
} 
