package ua.pp.kaeltas.congratulation;

public class HappyNewYearTest {

  public static void main(String[] args) {

    /* Get congratulation <year> from args */
    int congratulationYear = 0;
    try {
      congratulationYear = Integer.valueOf(args[0]);
    } catch (IndexOutOfBoundsException e) {
      System.out.println("Error: provide <year> as argument.");
      System.out.println("Example: java HappyNewYearTest 2015");
      System.exit(2);
    } catch (NumberFormatException e) {
      System.out.println("Error: incorrect year. Allowed only numbers.");
      System.exit(2);
    }
    
    HappyNewYear happyNewYear = new HappyNewYear(congratulationYear);
    happyNewYear.congratulate();
    
  }
  
}