package lr05.v10;

import com.sun.xml.internal.bind.v2.schemagen.xmlschema.Annotated;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Создать класс машина, имеющий марку, число цилиндров, мощность. 
 * Создать производный класс - грузовик, имеющий грузоподъемность.
 * 
 * Разработать аннотацию, отметить, ей метод(ы) в классе, 
 * С помощью рефлексии обойти методы класса и вызвать отмеченные аннотацией 
 * методы с помощью invoke().
 * 
 * С помощью рефлексии вывести имя класса, а также 
 * Список интерфейсов реализованных классом
 * Список полей с типами и аннтотациями
 * 
 * @author Matsokha Andrey
 */
public class Runner {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Runner runner = new Runner();
        runner.test();
        
    }
    
    public void test() {
        Car car = new Car(110, "BMV", 8);
        Car truck = new Truck(3000, 120, "Mercedes", 8);
        
        System.out.println("INVOKE METHODS");
        invokeMethods(car, Runme.class);
        invokeMethods(truck, Runme.class);
        
        car.stop();
        truck.stop();
        
        System.out.println("\nPRINT INTERFACES");
        printClassImplementations(car);
        printClassImplementations(truck);
        
        System.out.println("\nPRINT FIELDS");
        printFields(car);
        printFields(truck);
    }
    
    public void invokeMethods(Car car, Class<? extends Annotation> annotation) {
        Class clazz = car.getClass();
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(annotation)) {
                try {
                    method.invoke(car);
                } catch (IllegalAccessException 
                        | InvocationTargetException 
                        | IllegalArgumentException ex) {
                    System.out.println(ex);
                }
            }
        }
    }
    
    public void printClassImplementations(Car car) {
        Class clazz = car.getClass();
        System.out.println("Class name: " + clazz.getSimpleName());
        System.out.print("Implements: ");
        Class[] interfaces = clazz.getInterfaces();
        for (Class anInterface : interfaces) {
            System.out.print(anInterface.getSimpleName() + " ");
        }
        System.out.println("");
    }
    
    public void printFields(Car car) {
        Class clazz = car.getClass();
        System.out.println("Class name: " + clazz.getSimpleName());
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            
            System.out.println(fieldModifiersToString(field) 
                    + field.getType().getSimpleName() 
                    + " " + field.getName() + ";");
        }
    }
    
    public String fieldModifiersToString(Field field) {
        int mod = field.getModifiers();
        StringBuilder sb = new StringBuilder();
        if (Modifier.isPrivate(mod)) {
            sb.append("private ");
        }
        if (Modifier.isPublic(mod)) {
            sb.append("public ");
        }
        if (Modifier.isProtected(mod)) {
            sb.append("protected ");
        }
        if (Modifier.isStatic(mod)) {
            sb.append("static ");
        }
        if (Modifier.isFinal(mod)) {
            sb.append("final ");
        }
        return sb.toString();
    }
}
