package lr05.v10;

/**
 *
 * @author Matsokha Andrey
 */
public class Truck extends Car implements Loadable, Cloneable {
    private int carryingKg;
    private int currentLoadKg = 0;

    public Truck(int carryingKg, int horsePower, 
            String brand, int cylinderCount) {
        super(horsePower, brand, cylinderCount);
        this.carryingKg = carryingKg;
    }
    
    @Runme
    @Override
    public void move() {
        System.out.println("Truck is moving");
    }
    
    @Override
    public void stop() {
        System.out.println("Truck is stopped");
    }

    @Override
    public String toString() {
        return "Truck{" + "carryingKg=" + carryingKg + '}';
    }

    @Override
    public void load(int weight) {
        int newLoad = currentLoadKg + weight;
        if (weight < 0 || newLoad < 0 || newLoad > carryingKg) {
            throw new IllegalArgumentException();
        }
        currentLoadKg = newLoad;
    }
    
    @Override
    public Truck clone() throws CloneNotSupportedException {
        return (Truck)super.clone();
    }
    
}
