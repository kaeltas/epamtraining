package lr05.v10;

/**
 *
 * @author Matsokha Andrey
 */
public class Car implements Cloneable {
    protected int horsePower;
    protected String brand;
    protected int cylinderCount;

    public Car(int horsePower, String brand, int cylinderCount) {
        this.horsePower = horsePower;
        this.brand = brand;
        this.cylinderCount = cylinderCount;
    }
    
    /**
     * Move car
     */
    @Runme
    public void move() {
        System.out.println("Car is moving");
    }
    
    /**
     * Stop car
     */
    public void stop() {
        System.out.println("Car is stopped");
    }

    @Override
    public String toString() {
        return "Car{" + "horsePower=" + horsePower + ", brand=" + brand + ", cylinderCount=" + cylinderCount + '}';
    }
    
    @Override
    public Car clone() throws CloneNotSupportedException {
        return (Car)super.clone();
    }
    
}
