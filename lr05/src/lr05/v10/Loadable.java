package lr05.v10;

/**
 *
 * @author Matsokha Andrey
 */
public interface Loadable {
    public void load(int weight);
}
