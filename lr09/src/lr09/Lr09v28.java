package lr09;

import java.io.File;

/**
 *
 * Вариант 5-28
 * Создайте программу вывода суммарной длины выделенных файлов 
 * в текущем каталоге. Компоненты графического окна: 
 * надпись (JLabel) "Вывод длины файлов в каталоге" в области North, 
 * в области Center размещен объект класса JFileChooser, в области South 
 * размещены кнопка (JButton) "Вывод", надпись  (JLabel) "Длина файлов (кБ):" 
 * и текстовое поле (JTextField) для вывода суммарной длины выделенных файлов. 
 * При нажатии кнопки "Вывод" в текстовом поле выводится суммарная длина 
 * выделенных в объекте класса JFileChooser файлов.
 * 
 * @author Matsokha Andrey
 */
public class Lr09v28 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Lr09v28 lr09v28 = new Lr09v28();
        lr09v28.test();
    }
    
    private void test() {
        FileSize fileSize = new FileSize();
        fileSize.add(new File("./src/lr09/FileSize.java"));
        fileSize.add(new File("./src/lr09/Lr09v28.java"));
        
        System.out.println("Sum files size: " + fileSize.getSumFilesSize() + " bytes");
    }
    
}
