package lr09;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matsokha Andrey
 */
public class FileSize {
    private List<File> files = new LinkedList<>();
    
    public void add(File file) {
        files.add(file);
    }
    
    public long getSumFilesSize() {
        long sumFilesSize = 0;
        
        for (File file : files) {
            if (file.exists() && file.isFile()) {
                sumFilesSize += file.length();
            }
        }
        
        return sumFilesSize;
    }
    
}
