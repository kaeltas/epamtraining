package ua.pp.kaeltas.parseprogtutorial;

import ua.pp.kaeltas.parseprogtutorial.textelements.ITextElement;
import ua.pp.kaeltas.parseprogtutorial.textelements.Paragraph;
import ua.pp.kaeltas.parseprogtutorial.textelements.Sentence;
import ua.pp.kaeltas.parseprogtutorial.textelements.Text;

/**
 * Var12.
 * Из текста удалить все слова заданной длинны, 
 * начинающиеся на согласную букву. 
 * 
 * Class deletes from text all words of defined length and 
 * started from consonant. 
 * 
 * @author Matsokha Andrey
 *
 */
public class TextProcessorDeleteWords implements ITextProcessor {
	private int wordLength;
	
	public TextProcessorDeleteWords(int wordLength) {
		this.wordLength = wordLength;
	}

	/**
	 * Delete from {@code text} all words which length is equal to 
	 * {@code wordLength} and which are started from consonant. 
	 */
	@Override
	public void process(Text text) {
		
		for (ITextElement textElement : text.getParagraphs()) {
			//process only text paragraphs (skip source code blocks)
			if (textElement.getClass() == Paragraph.class) {
				Paragraph paragraph = (Paragraph)textElement;

				//process each sentence
				for (Sentence sentence : paragraph.getSentences()) {
					sentence.deleteWords(new DeleteWordFilter(wordLength));
				}
			}
		}
		
	}

}
