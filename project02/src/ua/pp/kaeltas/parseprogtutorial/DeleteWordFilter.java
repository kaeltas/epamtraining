package ua.pp.kaeltas.parseprogtutorial;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import ua.pp.kaeltas.parseprogtutorial.textelements.Word;
import ua.pp.kaeltas.parseprogtutorial.textelements.WordFilter;

/**
 * Filter class that accept words which are not delimiters, 
 * which length is equal to defined,
 * and which are stared from consonant. 
 * 
 * @author Matsokha Andrey
 *
 */
public class DeleteWordFilter implements WordFilter {
	private int wordLength; //given word length
	private Set<Character> vowelSet = new HashSet<>(); //set of english and russian vowels 
	
	public DeleteWordFilter(int wordLength) {
		this.wordLength = wordLength;
		vowelSet.addAll(Arrays.asList(new Character[] {
				'a', 'e', 'u', 'i', 'o', //english 
				'у', 'е', 'а', 'о', 'э', 'я', 'и', 'ю', 'ы', 'ё'  //russian
		}));
	}

	/**
	 * Return true if {@code word} appplies to all filter rules: 
	 *  1) word is not a delimiter 
	 *  2) word length is equal to defined
	 *  3) word is stared from consonant
	 *  
	 *  Return false otherwise. 
	 */
	@Override
	public boolean accept(Word word) {
		if (!word.isDelimiter() 
				&& word.getWord().length() == wordLength 
				&& !vowelSet.contains(word.getWord().toLowerCase().charAt(0)) ) {
			return true;
		}
		return false;
	}

}
