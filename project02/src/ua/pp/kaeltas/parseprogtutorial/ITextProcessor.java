package ua.pp.kaeltas.parseprogtutorial;

import ua.pp.kaeltas.parseprogtutorial.textelements.Text;

/**
 * Interface for processing parsed text - instance of class {@code Text}. 
 * 
 * @author Matsokha Andrey
 *
 */
public interface ITextProcessor {
	
	/**
	 * Do some operations on parsed text.
	 * 
	 * @param text
	 */
	void process(Text text);

}
