package ua.pp.kaeltas.parseprogtutorial;

import java.io.File;
import java.io.IOException;

import ua.pp.kaeltas.parseprogtutorial.textelements.Text;

/**
 * Demonstration class.
 * Reads text from file, parses it and do some processing.
 * 
 * @author Matsokha Andrey
 *
 */
public class Runner {

	public static void main(String[] args) {
		new Runner().parseFileAndProcessText("./text/progtutorial.txt");
	}
	
	/**
	 * Demonstration. 
	 * Parse text from file {@code filename} and delete from it
	 * all words which length is equal to 7, 8 or 9 characters and 
	 * which are started from consonant.
	 */
	private void parseFileAndProcessText(String filename) {
		TextParser textParser = new TextParser();
		
		try {
			textParser.parse(new File(filename));
			Text text = textParser.getText();
			System.out.println(text.buildText());
			
			new TextProcessorDeleteWords(7).process(text);
			new TextProcessorDeleteWords(8).process(text);
			new TextProcessorDeleteWords(9).process(text);
			System.out.println("-----------");
			System.out.println(text.buildText());
			
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
}
