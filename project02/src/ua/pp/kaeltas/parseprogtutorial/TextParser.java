package ua.pp.kaeltas.parseprogtutorial;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import ua.pp.kaeltas.parseprogtutorial.textelements.ITextElement;
import ua.pp.kaeltas.parseprogtutorial.textelements.Paragraph;
import ua.pp.kaeltas.parseprogtutorial.textelements.ParagraphSourceCode;
import ua.pp.kaeltas.parseprogtutorial.textelements.Text;

/**
 * Class parses file line by line and fills Text object with parsed results.
 * 
 * @author Matsokha Andrey
 *
 */
public class TextParser {
	
	//States of parsing state machine.
	enum ParseState {
		PARSE_TEXT, PARSE_SOURCECODE;
	}

	private Text text; //parsed result text
	
	private ITextElement currentParsingElement; //current parsing paragraph by state machine
	private ParseState currentParseState = ParseState.PARSE_TEXT; //current state of state machine
	
	public TextParser() {
		text = new Text();
	}

	public void parse(File file) throws IOException {
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = bufferedReader.readLine()) != null) {
				parse(line);
			}
		}
	}
	

	/**
	 * Parse one line(paragraph) and add it to Text object.
	 * Works as State Machine: do separate parsing of text paragraphs and 
	 * source code blocks.
	 * 
	 * @param line - paragraph of text or one line of source code block
	 */
	public void parse(String line) {
		
		switch (currentParseState) {
			case PARSE_TEXT: {
				if ("<sourcecode>".equalsIgnoreCase(line)) {
					currentParsingElement = new ParagraphSourceCode();
					currentParseState = ParseState.PARSE_SOURCECODE;
					break;
				}
				currentParsingElement = new Paragraph();
				currentParsingElement.parse(line);
				text.addParagraph(currentParsingElement);
				break;
			}
			case PARSE_SOURCECODE: {
				if ("</sourcecode>".equalsIgnoreCase(line)) { //end sourceCode block
					text.addParagraph(currentParsingElement);
					currentParseState = ParseState.PARSE_TEXT;
				} else {
					currentParsingElement.parse(line);
				}
				break;
			}
		}
	}

	public Text getText() {
		return text;
	}
	
}
