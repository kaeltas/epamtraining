package ua.pp.kaeltas.parseprogtutorial.textelements;

/**
 * Filter that is applied to instances of class {@code Word}.
 * 
 * @author Matsokha Andrey
 *
 */
public interface WordFilter {
	/**
	 * Method which decides to accept or not given word.
	 * 
	 * @param word
	 * @return true if filter applies to given word, false - otherwise.
	 */
	public boolean accept(Word word);
}
