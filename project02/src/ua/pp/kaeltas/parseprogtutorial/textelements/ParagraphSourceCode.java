package ua.pp.kaeltas.parseprogtutorial.textelements;

import java.util.LinkedList;
import java.util.List;


/**
 * Class represents source code block in text.
 * It consist of list of String lines and each line of source code are not 
 * parsed into words.
 * 
 * @author Matsokha Andrey
 *
 */
public class ParagraphSourceCode implements ITextElement {
	private List<String> lines = new LinkedList<>();

	/**
	 * Add line of source code to source code block without modifications. 
	 */
	@Override
	public void parse(String line) {
		lines.add(line);
	}
	
	/**
	 * Return string representation of source code block with addition of 
	 * line numbers.
	 */
	@Override
	public String buildStringRepresentation() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("<sourcecode>\n");
		int linesCounter = 1;
		for (String line : lines) {
			sb.append(linesCounter++ + ". " + line + "\n");
		}
		sb.append("</sourcecode>");
		
		return sb.toString();
	}
	
}
