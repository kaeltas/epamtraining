package ua.pp.kaeltas.parseprogtutorial.textelements;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Represents Paragraph abstraction of text.
 * It consists of list of sentences.
 * 
 * @author Matsokha Andrey
 *
 */
public class Paragraph implements ITextElement {
	protected List<Sentence> sentences = new LinkedList<>(); //list of sentences aka paragraph

	/**
	 * Build paragraph from String {@code line}.
	 * Delimiters of sentences are: .!?
	 */
	@Override
	public void parse(String line) {
		
		Pattern pattern = Pattern.compile("(.+?[.!?])");
		Matcher matcher = pattern.matcher(line);
		while (matcher.find()) {
			String sentenceStr = matcher.group();
			Sentence sentence = new Sentence();
			sentence.parse(sentenceStr);
			sentences.add(sentence);
		}
		
	}

	/**
	 * Return string representation of paragraph.
	 */
	@Override
	public String buildStringRepresentation() {
		StringBuilder sb = new StringBuilder();
		
		for (Sentence sentence : sentences) {
			sb.append(sentence.buildSentence() + " ");
		}
		if (sb.length() > 0) {
			sb.deleteCharAt(sb.length()-1);
		}
		
		return sb.toString();
	}
	
	/**
	 * Get list of sentences.
	 * 
	 * @return
	 */
	public List<Sentence> getSentences() {
		return sentences;
	}

	
}
