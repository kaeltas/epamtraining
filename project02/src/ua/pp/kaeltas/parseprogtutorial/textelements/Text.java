package ua.pp.kaeltas.parseprogtutorial.textelements;

import java.util.LinkedList;
import java.util.List;

/**
 * Class represents text. This is a root element which holds a list of 
 * paragraphs.
 * 
 * @author Matsokha Andrey
 *
 */
public class Text {
	private List<ITextElement> paragraphs = new LinkedList<>(); //list of paragraphs aka text

	/**
	 * Add paragraph to text.
	 * 
	 * @param paragraph
	 */
	public void addParagraph(ITextElement paragraph) {
		paragraphs.add(paragraph);
	}
	
	/**
	 * Return string representation of full text, including all paragraphs 
	 * and source code blocks.
	 * 
	 * @return
	 */
	public String buildText() {
		StringBuilder sb = new StringBuilder();
		for (ITextElement paragraph : paragraphs) {
			sb.append(paragraph.buildStringRepresentation() + "\n");
		}
		return sb.toString();
	}
	
	public List<ITextElement> getParagraphs() {
		return paragraphs;
	}
	
	
}
