package ua.pp.kaeltas.parseprogtutorial.textelements;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class represents sentence abstraction in text.
 * It consists of list of words.
 * It is built from string representation of sentence using 
 * method {@code parse(String line)}. 
 * 
 * @author Matsokha Andrey
 *
 */
public class Sentence {
	private List<Word> words = new LinkedList<>(); //list of words aka sentence

	/**
	 * Parse {@code line} into words and wordBreakers by delimiters: {@code .,!?:;()}.
	 * Saves parsed elements into instance of this class. 
	 * 
	 * @param line
	 */
	public void parse(String line) {
		Pattern pattern = Pattern.compile("([)(])?([\\wА-Яа-я-]+)([?.!:;,)( ]*)");
		Matcher matcher = pattern.matcher(line);
		while (matcher.find()) {
			if (matcher.group(1) != null) {
				words.add(
						WordsCache.getInstance().getWord( 
								WordFactory.createWordDelimiter(matcher.group(1)) 
								));
			}
			if (matcher.group(2) != null) {
				words.add(
						WordsCache.getInstance().getWord( 
								WordFactory.createWord(matcher.group(2)) 
								));
			}
			if (matcher.group(3) != null) {
				words.add(
						WordsCache.getInstance().getWord( 
								WordFactory.createWordDelimiter(matcher.group(3)) 
								));
			}
		}
	}
	
	/**
	 * Build and return string representation of sentence. 
	 * 
	 * @return
	 */
	public String buildSentence() {
		StringBuilder sb = new StringBuilder();
		for (Word word : words) {
			sb.append(word.getWord());
		}
		return sb.toString();
	}


	/**
	 * Delete all words from sentence which apply to filter.
	 * 
	 * @param filter
	 */
	public void deleteWords(WordFilter filter) {
		for(Iterator<Word> iter = words.iterator(); iter.hasNext();) {
			Word word = iter.next();
			if (filter.accept(word)) {
				iter.remove();
			}
		}
	}

}
