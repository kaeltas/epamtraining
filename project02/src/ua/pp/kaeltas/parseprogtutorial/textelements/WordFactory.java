package ua.pp.kaeltas.parseprogtutorial.textelements;

/**
 * Factory for creating words and word delimiters.
 * 
 * @author Matsokha Andrey
 *
 */
public class WordFactory {

	/**
	 * Create and return instance of Word. 
	 * 
	 * @param word
	 * @return
	 */
	public static Word createWord(String word) {
		return new Word(word);
	}
	
	/**
	 * Create and return instance of Word which behaves as word delimiter. 
	 * 
	 * @param word
	 * @return
	 */
	public static Word createWordDelimiter(String wordDelimiter) {
		return new Word(wordDelimiter, true);
	}
}
