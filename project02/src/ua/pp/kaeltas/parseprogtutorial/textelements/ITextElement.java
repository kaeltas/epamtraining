package ua.pp.kaeltas.parseprogtutorial.textelements;

/**
 * Interface of for aggregating text elements, like paragraph, sentence.
 * It allows to get string representation and also build such element 
 * by parsing string line. 
 * 
 * @author Matsokha Andrey
 *
 */
public interface ITextElement {
	/**
	 * Return String representation of this text element.
	 * @return
	 */
	String buildStringRepresentation();
	
	/**
	 * Build this text element from String {@code line}. 
	 * @param line
	 */
	void parse(String line);

}
