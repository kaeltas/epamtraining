package ua.pp.kaeltas.parseprogtutorial.textelements;

import java.util.HashMap;
import java.util.Map;


/**
 * Words cache class represents flyweight pattern.
 * It holds a map from String representation of word to instance of class
 * Word.
 * 
 * @author Matsokha Andrey
 */
public class WordsCache {
	private static WordsCache instance; //singleton instance
	
	private WordsCache() {
	}
	
	/**
	 * Return singleton instance of WordsCache class.
	 * @return
	 */
	public static WordsCache getInstance() {
		if (instance == null) {
			instance = new WordsCache();
		}
		return instance;
	}
	
	
	private Map<String, Word> wordsCache = new HashMap<>();

	/**
	 * Return cached Word object from cache according to given {@code word}.
	 * If there is no such word in cache - add it.
	 * 
	 * @param word
	 * @return
	 */
	public Word getWord(Word word) {
		if (!wordsCache.containsKey(word.getWord())) {
			wordsCache.put(word.getWord(), word);
		} 
		return wordsCache.get(word.getWord());
	}

}
