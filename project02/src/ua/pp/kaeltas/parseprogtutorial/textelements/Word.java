package ua.pp.kaeltas.parseprogtutorial.textelements;

/**
 * Class represents a word abstraction.
 * It could behave as word, which consist of letters and digits,
 * also it could behave as word delimiter, which holds elements: .,!?():; 
 * 
 * @author Matsokha Andrey
 *
 */
public class Word {
	private String word;
	private boolean isDelimiter = false;

	/**
	 * Create usual word. Flag isDelimiter is set to false. 
	 * 
	 * @param word
	 */
	public Word(String word) {
		this.word = word;
	}
	
	/**
	 * Create word and set its flag isDelimiter.
	 * 
	 * @param word
	 * @param isDelimiter
	 */
	public Word(String word, boolean isDelimiter) {
		this.word = word;
		this.isDelimiter = isDelimiter;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	
	public boolean isDelimiter() {
		return isDelimiter;
	}
	
}
