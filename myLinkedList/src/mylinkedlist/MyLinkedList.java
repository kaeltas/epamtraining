/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mylinkedlist;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author kaeltas
 */
public class MyLinkedList implements Iterable<Integer> {

    class Node {
        private int value;
        private Node next;
        private Node prev;

        public Node(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        public Node getPrev() {
            return prev;
        }

        public void setPrev(Node prev) {
            this.prev = prev;
        }
    }
    
    private Node first;
    private Node last;
    private int size;
    
    /**
     * Add int value {@code elem} to head of LinkedList.
     * @param elem 
     */
    public void addFirst(int elem) {
        Node newNode = new Node(elem);
        if (first == null) {
            first = last = newNode;
        } else {
            first.setPrev(newNode);
            newNode.setNext(first);
            first = newNode;
        }
        size++;
    }
    
    /**
     * Add int value {@code elem} to tail of LinkedList.
     * @param elem 
     */
    public void addLast(int elem) {
        Node newNode = new Node(elem);
        if (last == null) {
            first = last = newNode;
        } else {
            last.setNext(newNode);
            newNode.setPrev(last);
            last = newNode;
        }
        size++;
    }
    
    /**
     * Remove first element in LinkedList and return value of it element.
     * @return 
     */
    public int removeFirst() {
        if (first == null) {
            throw new IllegalStateException("List is empty");
        }
        int value = first.getValue();
        if (first == last) {
            first = last = null;
        } else {
            first = first.getNext();
            first.setPrev(null);
        }
        size--;
        
        return value;
    }
    
    /**
     * Remove last element in LinkedList and return value of it element.
     * @return 
     */
    public int removeLast() {
        if (last == null) {
            throw new IllegalStateException("List is empty");
        }
        int value = last.getValue();
        if (first == last) {
            first = last = null;
        } else {
            last = last.getPrev();
            last.setNext(null);
        }
        size--;
        
        return value;
    }
    
    /**
     * Clear LinkedList - remove all elements.
     */
    public void clear() {
        first = last = null;
        size = 0;
    }
    
    /**
     * Remove first node which value equals to {@code value}.
     * @param value
     * @return true if node was deleted, false - otherwise
     */
    public boolean remove(int value) {
        Node nodeTmp = first;
        while (nodeTmp != null) {
            if (nodeTmp.value == value) {
                Node prev = nodeTmp.getPrev();
                if (prev == null) {
                    first = nodeTmp.getNext();
                    if (first != null) {
                        first.setPrev(null);
                    }
                } else {
                    prev.setNext(nodeTmp.getNext());
                }
                
                Node next = nodeTmp.getNext();
                if (next == null) {
                    last = nodeTmp.getPrev();
                    if (last != null) {
                        last.setNext(null);
                    }
                } else {
                    next.setPrev(nodeTmp.getPrev());
                }
                
                size--;
                
                return true;
            }
            nodeTmp = nodeTmp.getNext();
        }
        
        return false;
    }
    
    /**
     * Remove all nodes which value equals to {@code value}.
     * @param value
     * @return true if at least one node was deleted, false - otherwise
     */
    public boolean removeAll(int value) {
        boolean result = false;
        while (true) {
            if (remove(value)) {
                result = true;
            } else {
                break;
            }
        }
        
        return result;
    }
    
    class MyLinkedListIterator implements Iterator<Integer> {
        private Node nodeCursor = first;
        
        @Override
        public boolean hasNext() {
            return nodeCursor != null;
        }

        @Override
        public Integer next() {
            if (nodeCursor == null) {
                throw new NoSuchElementException("List is empty");
            }
            int val = nodeCursor.getValue();
            nodeCursor = nodeCursor.getNext();
            return val;
        }
        
    }

    @Override
    public Iterator<Integer> iterator() {
        return new MyLinkedListIterator();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        for (Iterator<Integer> iter = iterator(); iter.hasNext(); ) {
            int val = iter.next();
            sb.append(val);
            if (iter.hasNext()) {
                sb.append(", ");
            }
        }
        
        return "MyLinkedList{ {" + sb + "}, size=" + size + '}';
    }
    
}
