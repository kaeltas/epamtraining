package mylinkedlist;

/**
 *
 * @author Matsokha Andrey
 */
public class TestMyLinkedList {
    
    public static void main(String[] args) {
        TestMyLinkedList testMyLinkedList = new TestMyLinkedList();
        testMyLinkedList.test();
    }
    
    public void test() {
        MyLinkedList myLinkedList = new MyLinkedList();
        myLinkedList.addFirst(3);
        myLinkedList.addFirst(5);
        System.out.println(myLinkedList);
        
        myLinkedList.addLast(9);
        myLinkedList.addLast(0);
        System.out.println(myLinkedList);
        
        myLinkedList.removeFirst();
        System.out.println(myLinkedList);
        myLinkedList.removeLast();
        System.out.println(myLinkedList);

        myLinkedList.clear();
        System.out.println(myLinkedList);
        
        myLinkedList.addLast(9);
        myLinkedList.addLast(0);
        myLinkedList.addLast(9);
        System.out.println(myLinkedList);
        
        System.out.println(myLinkedList.remove(9));
        System.out.println(myLinkedList.remove(9));
        System.out.println(myLinkedList.remove(9));
        System.out.println(myLinkedList);
        
        myLinkedList.addLast(9);
        myLinkedList.addLast(0);
        myLinkedList.addLast(9);
        System.out.print("Iterator: ");
        for (Integer val : myLinkedList) {
            System.out.print(val + ", ");
        }
        System.out.println("");
        
        System.out.println(myLinkedList);
        System.out.println(myLinkedList.removeAll(0));
        System.out.println(myLinkedList.removeAll(0));
        myLinkedList.remove(9);
        myLinkedList.remove(9);
        System.out.println(myLinkedList);
        
        
    }
    
}
