package labr06.v01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Matsokha Andrey
 */
public class HyperboleInputStdin implements HyperboleInput {

    /**
     * Read and return {@code double} value from {@code System.in}.
     * Read request will repeat until correct double value is received.
     * 
     * @return
     * @throws IOException 
     */
    @Override
    public double readDouble() throws IOException {
        BufferedReader br = 
                new BufferedReader(new InputStreamReader(System.in));
        
            while(true) {
                String value = br.readLine();
                try {
                    return Double.valueOf(value);
                } catch (NumberFormatException e) {
                    System.out.println("Wrong double value! Try again.");
                }
            }
    }
    
}
