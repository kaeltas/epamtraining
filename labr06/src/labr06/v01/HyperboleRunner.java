package labr06.v01;

/**
 *
 * @author Matsokha Andrey
 */
public class HyperboleRunner {
    
    public static void main(String[] args) {
        HyperboleRunner hyperboleRunner = new HyperboleRunner();
        hyperboleRunner.runHyperboleTest();
    }
    
    public void runHyperboleTest() {
        HyperboleController hyperboleController = new HyperboleController(
                new HyperboleViewStdout()); 
        
        hyperboleController.readAndSetParamA();
        hyperboleController.readAndSetParamB();
        
        hyperboleController.printRootX();
        hyperboleController.printRootY();
    }
    
}
