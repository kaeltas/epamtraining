package labr06.v01;

import java.io.IOException;

/**
 * Controller for Hyperbole class.
 * 
 * @author Matsokha Andrey
 */
public class HyperboleController {
    
    private final HyperboleView hyperboleView;
    private final Hyperbole hyperbole;

    /**
     * Create HyperboleController
     * 
     * @param hyperboleView - implementation of HyperboleView interface
     * @see HyperboleView
     * @see HyperboleInput
     */
    public HyperboleController(HyperboleView hyperboleView) {
        this.hyperboleView = hyperboleView;
        hyperbole = new Hyperbole();
    }
    
    /**
     * Read param and set {@code a} value of hyperbole.
     * 
     */
    public void readAndSetParamA() {
        try {
            double a = hyperboleView.read("a");
            hyperbole.setA(a);
        } catch (IllegalArgumentException e) {
            hyperboleView.printError(e.getMessage());
        } catch (IOException e) {
            hyperboleView.printSystemError("Can't read value.");
        }
    }
    
    /**
     * Read param and set {@code b} value of hyperbole.
     */
    public void readAndSetParamB() {
        try {
            double b = hyperboleView.read("b");
            hyperbole.setB(b);
        } catch (IllegalArgumentException e) {
            hyperboleView.printError(e.getMessage());
        } catch (IOException e) {
            hyperboleView.printSystemError("Can't read value.");
        }
    }
    
    /**
     * Read {@code y}, calc x-roots and print them to {@code hyperboleView}.
     * 
     */
    public void printRootX() {
        try {
            double[] res = calcX();
            hyperboleView.printX(hyperbole, res[0], res[1]);
        } catch (IOException e) {
            hyperboleView.printSystemError("Can't read value.");
        }
    }
    
    /**
     * Read {@code x}, calc y-roots and print them to {@code hyperboleView}.
     * 
     */
    public void printRootY() {
        try {
            double[] res = calcY();
            hyperboleView.printY(hyperbole, res[0], res[1]);
        } catch (IOException e) {
            hyperboleView.printSystemError("Can't read value.");
        }
    }

    /**
     * Read {@code x} and calc y-roots.
     * 
     * @return double[] where [0] = x, [1] = y-root
     * @throws IOException 
     */
    private double[] calcY() throws IOException {
        double x = hyperboleView.read("x");
        return new double[] {x, hyperbole.calcY(x)};
    }
    
    /**
     * Read {@code y} and calc x-roots.
     * 
     * @return double[] where [0] = y, [1] = x-root
     * @throws IOException 
     */
    private double[] calcX() throws IOException {
        double y = hyperboleView.read("y");
        return new double[] {y, hyperbole.calcX(y)};
    }
    
}
