package labr06.v01;

import java.io.IOException;

/**
 *
 * @author Matsokha Andrey
 */
interface HyperboleInput {
    
    public double readDouble() throws IOException;
    
}
