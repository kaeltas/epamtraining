package labr06.v01;

import java.io.IOException;

/**
 *
 * @author Matsokha Andrey
 */
interface HyperboleView {
    public void printX(Hyperbole hyperbole, double param, double root);
    public void printY(Hyperbole hyperbole, double param, double root);
    public void printError(String message);
    public void printSystemError(String message);
    public double read(String readParamName) throws IOException;
}
