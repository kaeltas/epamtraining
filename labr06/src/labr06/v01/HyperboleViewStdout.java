package labr06.v01;

import java.io.IOException;

/**
 * Print hyperbole roots to System.out 
 * and read necessary values from System.in
 * 
 * @author Matsokha Andrey
 */
public class HyperboleViewStdout implements HyperboleView {
    private HyperboleInput hyperboleInput = new HyperboleInputStdin();
    
    @Override
    public void printX(Hyperbole hyperbole, double param, double root) {
        System.out.println("Roots of hyperbole, in point y = " + param + " :");
        System.out.println("x^2/" + hyperbole.getA() 
                + " - " + "y^2/" + hyperbole.getB() 
                + " = 1;");
        System.out.println("x1 = + " + root);
        System.out.println("x2 = - " + root);
    }

    @Override
    public void printY(Hyperbole hyperbole, double param, double root) {
        System.out.println("Roots of hyperbole, in point x = " + param + " :");
        System.out.println("x^2/" + hyperbole.getA() 
                + " - " + "y^2/" + hyperbole.getB() 
                + " = 1;");
        System.out.println("y1 = + " + root);
        System.out.println("y2 = - " + root);
    }

    @Override
    public void printError(String message) {
        System.out.println(message);
    }
    
    @Override
    public void printSystemError(String message) {
        System.out.println("System error! " + message);
    }
    
    @Override
    public double read(String readParamName) throws IOException {
        System.out.println("Please enter value [" + readParamName + "]");
        return hyperboleInput.readDouble();
    }
}
