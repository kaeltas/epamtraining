package labr06.v01;

/**
 *
 * - написать класс согдасно индивидуальному варианту, 
 * оформить согласно JCC,  написать JavaDOC;
 * - реализовать хотя бы 2 конструктора и клонирование;
 * - переопределить методы equals, hashCode, toString;
 * 
 * Построить программу для работы с классом для хранения данных 
 * о кривой второго порядка - гиперболе. 
 * Программа должна обеспечивать: 
 * расчет у по х и наоборот, ввод значений, вывод значений.
 * 
 * @author Matsokha Andrey
 */
public class Hyperbole implements Cloneable {
    private double a;
    private double b;

    /**
     * Create Hyperbole with a = 1 and b = 1.
     * Canonical equation: {@code x^2/a^2 - y^2/b^2 = 1}
     * 
     */
    public Hyperbole() {
        this(1, 1);
    }
    
    /**
     * Create hyperbole with params {@code a, b}.
     * Canonical equation: {@code x^2/a^2 - y^2/b^2 = 1}
     * 
     * @param a
     * @param b 
     */
    public Hyperbole(double a, double b) {
        if (a <= 0 || b <= 0) {
            throw new IllegalArgumentException("Wrong hyperbole!"
                    + " Params can't be less or equal 0.");
        }
        this.a = a;
        this.b = b;
    }

    /**
     * Calc Y value of hyperbole by given param X.
     * Equation has 2 roots:  
     * {@code y = + sqrt(b^2 * (x^2/a^2 - 1) );}
     * {@code y = - sqrt(b^2 * (x^2/a^2 - 1) );}
     * 
     * @param x
     * @return {@code double} positive root of equation.
     */
    public double calcY(final double x) {
        return Math.sqrt(b*b * (x*x / a*a - 1));
    }
    
    /**
     * Calc X value of hyperbole by given param Y.
     * Equation has 2 roots:  
     * {@code x = + sqrt(a^2 * (y^2/b^2 + 1) );}
     * {@code x = - sqrt(a^2 * (y^2/b^2 + 1) );}
     * 
     * @param y
     * @return {@code double} positive root of equation.
     */
    public double calcX(final double y) {
        return Math.sqrt(a*a * (y*y / b*b + 1));
    }
    
    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    /**
     * Set {@code a} param of hyperbole.
     * Throws IllegalArgumentException if {@code a} is not acceptable.
     * 
     * @param b 
     */
    public void setA(double a) {
        if (a <= 0) {
            throw new IllegalArgumentException("Wrong hyperbole!"
                    + " Param can't be less or equal 0.");
        }
        this.a = a;
    }

    /**
     * Set {@code b} param of hyperbole.
     * Throws IllegalArgumentException if {@code b} is not acceptable.
     * 
     * @param b 
     */
    public void setB(double b) {
        if (b <= 0) {
            throw new IllegalArgumentException("Wrong hyperbole!"
                    + " Param can't be less or equal 0.");
        }
        this.b = b;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj == this) {
            return true;
        }
        
        if (obj.getClass() != getClass()) {
            return false;
        }
        
        Hyperbole that = (Hyperbole) obj;
        
        if (Double.doubleToLongBits(a) != Double.doubleToLongBits(that.a)) {
            return false;
        }
        
        if (Double.doubleToLongBits(b) != Double.doubleToLongBits(that.b)) {
            return false;
        }
        
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.a) ^ (Double.doubleToLongBits(this.a) >>> 32));
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.b) ^ (Double.doubleToLongBits(this.b) >>> 32));
        return hash;
    }

    @Override
    public String toString() {
        return "Hyperbole{" + "a=" + a + ", b=" + b + '}';
    }
    
    @Override
    public Hyperbole clone() {
        try {
            return (Hyperbole)super.clone();
        } catch (CloneNotSupportedException ex) {
            throw new AssertionError(ex.getMessage());
        }
    }
    
}
